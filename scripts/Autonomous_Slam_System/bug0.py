#!/usr/bin/env python

import rospy  # rospy library
from geometry_msgs.msg import Point
import math


class bug0:
    def __init__(self):
        #distance precision
        self.dist_precision = 0.3
        #yaw precision
        self.yaw_precision = 5 * math.pi / 180
        #safe wall following distance
        self.d = 0.8
        # initialise dictionary to contain min distances to objects around robot
        self.regions = {
            'front': self.d,
            'fright': self.d,
            'right': self.d,
            'left': self.d,
            'fleft': self.d,
        }
        # initialise state of bug behaviour
        self.State = 0
        # State_dict = {
        # 0: 'Go to point',
        # 1: 'Wall following'
        # }
        # initialise state of go to point behaviour
        self.State_gtp = 0
        # State_dict_gtp = {
        # 0: 'Fix yaw',
        # 1: 'Proceed to goal',
        # 2: 'Stop'
        # }
        # initialise state of wall following behaviour
        self.State_wf = 0
        # State_dict_wf = {
        # 0: 'Find wall',
        # 1: 'Turn left',
        # 2: 'Follow wall'
        # }
        # initialise variable to store distance to goal each time an obstacle is encountered
        self.goal_dist_hit = None
        # initialise variables for tracking bug0 states (to enable debugging)
        self.current_state = 0  # tracks current state of bug0 without affecting code execution
        self.first_run = True  # tracks whether or not its the first run of go to point behaviour
        self.robot_stopped = False  # tracks whether goal point has been reached
        #set speed commands to return
        self.linear_v = 0
        self.angular_v = 0


    def navigateViaBug(self, Yaw, Position, desired_position, dists, number_angle_segments):

        self.obtain_regions(dists, number_angle_segments)

        if self.State == 0:
            # initiate go to point behaviour
           self.go_to_point(Yaw, Position, desired_position)

        elif self.State == 1:
            # initiate wall following behaviour
           self.wall_following(Yaw, Position, desired_position)

        return self.linear_v, self.angular_v


##################################################### CALLBACKS ########################################################

    def obtain_regions(self, dists, number_angle_segments):
        # returns flat distance to nearest obstacle in each region around robot (in regions) each time lidar message is received

        #360/10 = 36 degrees per region
        region_size = int(round(number_angle_segments*0.1) - 1)
        left_region_start = int(round(number_angle_segments*0.25))
        left_region_end = (left_region_start + region_size)
        fleft_region_start = left_region_end + 1
        fleft_region_end = fleft_region_start + region_size
        front_region_start = fleft_region_end + 1
        front_region_end = front_region_start + region_size
        fright_region_start = front_region_end + 1
        fright_region_end = fright_region_start + region_size
        right_region_start = fright_region_end + 1
        right_region_end = right_region_start + region_size

        # define regions of robot using 2D map via a dictionary using 45 degree segments
        self.regions = {
            'front': min(dists[front_region_start: front_region_end]),
            'fright': min(dists[fright_region_start: fright_region_end]),
            'right': min(dists[right_region_start: right_region_end]),
            'left': min(dists[left_region_start: left_region_end]),
            'fleft': min(dists[fleft_region_start: fleft_region_end]),
        }


########################################################################################################################

################################################### GENERAL METHODS ####################################################

    def normalize_angle(self, angle):
        # normalises an angle to between -pi and pi so that the shortest turn can be taken to reach desired heading
        normalized_angle = angle
        if math.fabs(angle) > math.pi:
            normalized_angle = angle - (2 * math.pi * angle) / math.fabs(angle)

        return normalized_angle


    def stopRobot(self):
        # stops the robot
        self.linear_v = 0
        self.angular_v = 0


    def change_state(self, state):
        # changes states within bug behaviour
        self.State = state

########################################################################################################################

########################################## METHODS FOR GO TO POINT BEHAVIOUR ###########################################

    def change_state_gtp(self, state):
        # changes states within go to point behaviour
        self.State_gtp = state


    def fix_yaw(self, Yaw, Position, desired_position):
        # points the robot to face towards the goal point

        # find desired yaw and current yaw error
        desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
        err_yaw = desired_yaw - Yaw
        err_yaw = self.normalize_angle(err_yaw)
        absolute_error_yaw = abs(err_yaw)

        # publish angular velocity to correct heading if error too big
        if math.fabs(err_yaw) > self.yaw_precision:
            if(err_yaw > 0):
                self.angular_v = self.mapValue(absolute_error_yaw, 0, 2* math.pi, 0.3, 1.5)
            else:
                #print("negative yaw angle offset")
                self.angular_v = self.mapValue(absolute_error_yaw, 0, 2* math.pi, -0.3, -1.5)
                #print("therefore angular v is :" + str(self.angular_v))
            self.linear_v = 0

        # change state to go straight towards goal if/when yaw is okay
        if math.fabs(err_yaw) <= self.yaw_precision:
            self.change_state_gtp(1)

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue


    def go_straight(self, Yaw, Position, desired_position):
        # moves the robot directly towards the goal point

        # find desired yaw and current yaw error
        desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
        err_yaw = desired_yaw - Yaw

        # find position error (distance to goal)
        pos_error = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

        # if robot needs to travel towards goal then publish a forward velocity
        if pos_error > self.dist_precision:
            self.linear_v = 0.6
            self.angular_v = 0

        # change state to stop once goal is reached
        if pos_error <= self.dist_precision:
            self.change_state_gtp(2)

        # change state to fix heading if needed
        if math.fabs(err_yaw) > self.yaw_precision:
            self.change_state_gtp(0)


    def go_to_point(self, Yaw, Position, desired_position):
        # implements go to point behaviour by switching between fixing heading and proceeding straight towards goal point

        #signal each time go to point bug0 state is triggered
        if self.first_run == True:
            self.first_run = False
            #print('Going to point')
            self.current_state = 0

        if self.first_run == False and self.current_state == 1:
            self.current_state = 0
            #print('Going to point')

        #implement go to point behaviour
        if self.State_gtp == 0:
            self.fix_yaw(Yaw, Position, desired_position)
            # print('Fixing yaw')
        elif self.State_gtp == 1:
            self.go_straight(Yaw, Position, desired_position)
            # print('Proceeding to goal')
        elif self.State_gtp == 2:
            # print('Goal reached!')
            self.stopRobot()
            if self.robot_stopped == False:
                self.robot_stopped = True
                #print('Robot Stopped')
        else:
            print('Go to point - Unknown State!')
            rospy.loginfo(State_gtp)

        # find desired yaw and current yaw error
        desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
        err_yaw = desired_yaw - Yaw
        # find yaw error between -pi and pi
        err_yaw_norm = self.normalize_angle(err_yaw)

        #if pointing towards goal and an obstacle is encountered, switch to wall following behaviour
        #print(regions['front'])
        if self.regions['front'] < self.d and err_yaw_norm <= self.yaw_precision:
            self.change_state(1)
        if self.regions['fleft'] < self.d and err_yaw_norm <= self.yaw_precision:
            self.change_state(1)
        if self.regions['fright'] < self.d and err_yaw_norm <= self.yaw_precision:
            self.change_state(1)

########################################################################################################################

####################################### METHODS FOR WALL FOLLOWING BEHAVIOUR ###########################################

    def change_state_wf(self, state):
        # changes states within wall following behaviour
        self.State_wf = state


    def decide_wf_state(self):
        # locates obstacles relative to robot by finding which regions are currently occupied at less than safe distance
        # then dictates which state wall follower should be in to follow obstacle wall

        # d is safe wall following distance defined globally

        #now basically: 1 = should progress, 0 = should turn
        #progression towards or away from obstacle determined by distance to wall
        #if regions right > d*1.05 then turn towards wall
        #if regions right < d*0.95 then turn away from wall
        #if regions right < d*1.05 and > d*0.95 move straight
        #Hence, overall: 0 = move towards wall, 1 = turn left, 2 = move away from wall, 3 = move straight along wall

        regions = self.regions
        d = self.d

        state_description = ''
        if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
            state_description = 'Case 1 - No Obstacle Ahead'
            self.change_state_wf(0)  # could try state 2 (follow wall) here
        elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
            state_description = 'Case 2 - Obstacle Front Right'
            self.change_state_wf(1) #was 2
        elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
            state_description = 'Case 3 - Obstacle Front Left'
            self.change_state_wf(0)
        elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
            state_description = 'Case 4 - Obstacle Front'
            self.change_state_wf(1)
        elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
            state_description = 'Case 5 - Obstacle Front and Front Right'
            self.change_state_wf(1)
        elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
            state_description = 'Case 6 - Obstacle Front and Front Left'
            self.change_state_wf(1)
        elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
            state_description = 'Case 7 - Obstacle Front Right and Front Left'
            self.change_state_wf(0)
        elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
            state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
            self.change_state_wf(1)
        else:
            state_description = 'Unknown Obstacle Case'
            #print(state_description)
            #rospy.loginfo(regions)

        if self.State_wf == 0:
            if regions['right'] < d*0.975:
                self.change_state_wf(2)
                state_description = 'Too close to wall - must move away'

            elif regions['right'] >= d*0.975 and regions['right'] <= d*1.025:
                self.change_state_wf(3)
                state_description = 'Good distance to wall - move straight'

            else:
                state_description = 'Too far from wall - move towards it'

        #now need a controller: when wall too far away (regions right > d) turn right in proportion to distance away from d,
        #when too close (regions right < d) turn left in proportion to distance away from d

        #print(state_description)


    def find_wall(self):
        # moves robot in right arc to find obstacle wall
        self.linear_v = 0.35
        distRight = self.regions['right']
        #self.angular_v = self.mapValue(distRight, 0, self.d, -0.2, -0.5)
        self.angular_v = -0.35


    def move_away(self):
        # moves robot in right arc to find obstacle wall
        self.linear_v = 0.35
        distRight = self.regions['right']
        #self.angular_v = self.mapValue(distRight, 0, self.d, 0.2, 0.5)
        self.angular_v = 0.35


    def turn_left(self):
        # turns robot left to keep obstacle wall on right side
        self.angular_v = 0.7
        self.linear_v = 0


    def follow_wall(self):
        # moves robot straight forwards to follow obstacle wall
        self.linear_v = 0.35
        self.angular_v = 0


    def wall_following(self, Yaw, Position, desired_position):
        # implements wall following behaviour by switching states accordingly between find wall, turn left and follow wall (move straight)

        #update wall follower state according to current environment
        self.decide_wf_state()

        #signal each time wall following bug0 state is triggered
        if self.current_state == 0:
            self.current_state = 1
            #print('Following obstacle wall')

            # find distance to goal when wall following entered
            self.goal_dist_hit = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5
            #print('Goal distance when obstacle encountered: ' + str(self.goal_dist_hit))

        #implement wall following behaviour
        if self.State_wf == 0:
            self.find_wall()
        elif self.State_wf == 1:
            self.turn_left()
        elif self.State_wf == 2:
            self.move_away()
        elif self.State_wf == 3:
            self.follow_wall()
        else:
            pass
            #print('Wall following - Unknown State!')
            #rospy.loginfo(State_wf)

        # find position error (distance to goal)
        pos_error = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

        #stop if goal reached
        if pos_error <= self.dist_precision:
            self.stopRobot()
            if self.robot_stopped == False:
                self.robot_stopped = True
                #print('Robot Stopped')

        # find distance to goal during wall following
        goal_dist = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

        #if goal current goal distance is less than at last hit point by at least 2% then check to exit wall following
        # (now as long as robot is closer to goal by at least length of robot it can check to exit wall following)
        if goal_dist < (self.goal_dist_hit - 0.6):
            # find desired yaw and current yaw error
            desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
            err_yaw = desired_yaw - Yaw
            #find current normalised yaw error
            err_yaw_norm = self.normalize_angle(err_yaw)

            # if err_yaw_norm > 0 must turn left (CCW), if err_yaw_norm < 0 must turn right (CW) to face goal

            free_distance = self.d*1.25

            # go to point if no obstacle ahead, goal is on left of robot, and robot must turn less than 45 degrees to face goal
            if self.regions['front'] > free_distance and (err_yaw_norm <= 18 * (math.pi / 180) and err_yaw_norm > -18 * (math.pi / 180)):
                self.change_state(0)
                #print('Space forward')
                #print('Yaw error: ' + str(err_yaw_norm))
                #print('Goal distance when leaving obstacle: ' + str(goal_dist))
                #print('Closer to goal by ' + str(self.goal_dist_hit - goal_dist))
            #go to point if no obstacle to left side, goal is on left of robot, and robot must turn by between 45 and 90 degrees to face goal
            if self.regions['left'] > free_distance and (err_yaw_norm > 54 * (math.pi / 180) and err_yaw_norm <= 90 * (math.pi / 180)):
                self.change_state(0)
                # print('Space left')
                # print('Yaw error: ' + str(err_yaw_norm))
                # print('Goal distance when leaving obstacle: ' + str(goal_dist))
                # print('Closer to goal by ' + str(self.goal_dist_hit - goal_dist))
            # go to point if no obstacle to right side, goal is on right of robot, and robot must turn by between 45 and 90 degrees to face goal
            if self.regions['right'] > free_distance and (err_yaw_norm <= -54 * (math.pi / 180) and err_yaw_norm > -90 * (math.pi / 180)):
                self.change_state(0)
                # print('Space right')
                # print('Yaw error: ' + str(err_yaw_norm))
                # print('Goal distance when leaving obstacle: ' + str(goal_dist))
                # print('Closer to goal by ' + str(self.goal_dist_hit - goal_dist))
            if self.regions['fleft'] > free_distance and (err_yaw_norm > 18 * (math.pi / 180) and err_yaw_norm <= 54 * (math.pi / 180)):
                self.change_state(0)
                # print('Space front left')
                # print('Yaw error: ' + str(err_yaw_norm))
                # print('Goal distance when leaving obstacle: ' + str(goal_dist))
                # print('Closer to goal by ' + str(self.goal_dist_hit - goal_dist))
            if self.regions['fright'] > free_distance and (err_yaw_norm <= -18 * (math.pi / 180) and err_yaw_norm > -54 * (math.pi / 180)):
                self.change_state(0)
                # print('Space front right')
                # print('Yaw error: ' + str(err_yaw_norm))
                # print('Goal distance when leaving obstacle: ' + str(goal_dist))
                # print('Closer to goal by ' + str(self.goal_dist_hit - goal_dist))
########################################################################################################################
