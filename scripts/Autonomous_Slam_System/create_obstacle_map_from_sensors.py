#! /usr/bin/env python

import rospy
from time import sleep
import math
import matplotlib.pyplot as plt
import numpy as np
from geometry_msgs.msg import Point
import random

# This class is designed to accept a 3D point cloud and flatten the point cloud with respect
# to the robot, any obstacles that are within the robots height are tracked and an array is
# returned with the closest obstacle at each angle segment in 360 degrees around the robot
#
# Developed by David Russell December 2020
# Edited by David Russell Decemeber 2020

###################################### END ############################################
class flattenLidar2DMap():
    def __init__(self, numberOfAngleSegments, heightOfLidar, heightOfRobot, lidarOffset):
        self.numberOfAngleSegments = numberOfAngleSegments
        self.heightOfLidar = heightOfLidar
        self.heightOfRobot = heightOfRobot
        self.lidarOffSet = lidarOffset
        self.nearestObstaclesFlattened = [100 for x in range(self.numberOfAngleSegments)]
        self.angleSegUpdated = [False for x in range(self.numberOfAngleSegments)]
        self.lowerBoundLIDARVisionAngle = -150
        self.upperBoundLIDARVisionAngle = 150
        self.lowerBoundRad = self.mapValue(self.lowerBoundLIDARVisionAngle, -180, 180, -math.pi, math.pi)
        self.upperBoundRad = self.mapValue(self.upperBoundLIDARVisionAngle, -180, 180, -math.pi, math.pi)

        self.occlusionZone1 = np.array([Point(-0.2, -0.4, -0.12), 0.7, 0.9, 0.2])


        self.xAxis = [0] * numberOfAngleSegments
        for angleSeg in range(self.numberOfAngleSegments):
            self.xAxis[angleSeg] = int(self.mapValue(angleSeg, 0, self.numberOfAngleSegments, self.lowerBoundLIDARVisionAngle, self.upperBoundLIDARVisionAngle))


    # This function is the main function for this class, it takes the lidar point cloud and processes it
    # to return a flattened array of distances at each angle segment around the robot to the closest obstacle
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Feb 2021

    ###################################### END ############################################
    def return2DObstacleMap(self, lidarScan):
        # Initialise empty obstacle flattened array
        nearestObstaclePoint2DFlattened = [100 for x in range(self.numberOfAngleSegments)]
        self.angleSegUpdated = [False for x in range(self.numberOfAngleSegments)]
        # loop through all points in the point cloud
        for point in lidarScan:
            # if the point is something the robot could crash into
            point = self.changeFrameFromLidarToRobotCentre(point, self.lidarOffSet)
            angleOfPoint = self.calculateAngle(point[0], point[1])

            if self.checkIfPointIsValidObstacle(point, angleOfPoint, self.heightOfRobot, self.heightOfLidar, self.occlusionZone1, self.lowerBoundRad, self.upperBoundRad) == True:
                # calculate the angle segment index this point would lie in
                angleIndex = self.calculateAngleSegmentIndex(angleOfPoint, self.numberOfAngleSegments, self.lowerBoundRad, self.upperBoundRad)

                # calculate the horizontal distance to the point and then update angle segment if it is lower than the current value
                # at that angle segment
                horizontalDistanceToPoint = self.calculatePythagorasDistance(point[0], point[1])
                #print("dist: " + str(horizontalDistanceToPoint))
                nearestObstaclePoint2DFlattened[angleIndex] = self.returnLowest(nearestObstaclePoint2DFlattened[angleIndex], horizontalDistanceToPoint)

        maxValidSensorReading = self.findMaxValidSensorReading(nearestObstaclePoint2DFlattened)

        # Eliminate any outliers in the flattened obstacle array
        nearestObstaclePoint2DFlattened = self.eliminateOutliers(nearestObstaclePoint2DFlattened, maxValidSensorReading)
        # Change the array fromat so that 0 is in fron of the robot rather than to the right and its range is -180 to 180 degrees
        #nearestObstaclePoint2DFlattened = self.changeArrayAngleFormat(nearestObstaclePoint2DFlattened, self.numberOfAngleSegments)
        self.nearestObstaclesFlattened = nearestObstaclePoint2DFlattened

        return nearestObstaclePoint2DFlattened

    # All three point valdiity checks in one nice working block for acces from the main fucntion block.
    # It checks if the point is a valid obstacle that isnt contrianed on the robot.
    #
    # Developed by David Russell Feb 2021
    # Edited by David Russell Feb 2021

    ###################################### END ############################################
    def checkIfPointIsValidObstacle(self, point, angleOfPoint, heightOfRobot, heightOfLidar, occlusionsZone1, lowerBoundAngle, upperBoundAngle):
        validPoint = True
        #Check if the robot can even crash into the point, the floor isnt an obstacle, neither is anything above the robot
        if self.checkIfPointWithinObstacleHeight(point[2], heightOfRobot, heightOfLidar) == False:
            validPoint = False

        #Check if the point is one of the points detected on the robot
        if validPoint == True:
            if self.checkIfPointInOcclusionZone(point, self.occlusionZone1) == True:
                validPoint = False

        if validPoint == True:
            if self.checkIfPointWithinAngleRange(angleOfPoint, lowerBoundAngle, upperBoundAngle) == False:
                validPoint = False


        return validPoint

    # Simple check to see if the point is within the working angle range of the lidar due to the
    # obstruction block at the back of the robot
    #
    # Developed by David Russell Feb 2021
    # Edited by David Russell Feb 2021

    ###################################### END ############################################
    def checkIfPointWithinAngleRange(self, angle, lowerBoundAngle, upperBoundAngle):
        pointInAngleRange = False
        if angle > lowerBoundAngle:
            if angle < upperBoundAngle:
                pointInAngleRange = True

        return pointInAngleRange


    # Finds the max valid sensor reading throughout the obstacle array, only want to use
    # values that were updated and not default
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def findMaxValidSensorReading(self, obstacleData):
        maxVal = 0
        for angleSegment in range(len(obstacleData)):
            if self.angleSegUpdated[angleSegment] == True:
                if(obstacleData[angleSegment] > maxVal):
                    maxVal = obstacleData[angleSegment]
        return maxVal


    # We need to change the array format so instead of 0 to 360 degrees data format, it is instead -180 to 180
    # and 0 is directly in front of the robot, the reason behind this is we need all the different frames to have
    # the same perspective so the whole system math works.
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def changeArrayAngleFormat(self, zeroTo2PiArray, numberOfAngleSegments):
        minusPiToPiArray = [0] * numberOfAngleSegments
        tempArray = [0] * numberOfAngleSegments
        # reverse array
        for angleSeg in range(numberOfAngleSegments):
            tempArray[angleSeg] = zeroTo2PiArray[numberOfAngleSegments - angleSeg - 1]

        for angleSeg in range(numberOfAngleSegments):
            index = (angleSeg + (numberOfAngleSegments / 2)) % numberOfAngleSegments
            minusPiToPiArray[angleSeg] = tempArray[index]
        # to switch to correct angle array format, we need to flip direction and shift it by pi radians

        return minusPiToPiArray

    # All the points come from the LIDAR which is not in the centre of the robot, we need to adjust all
    # points so they are relative to the robot centre.
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def changeFrameFromLidarToRobotCentre(self, point, lidarOffset):
        newPoint = [0, 0, 0]
        newPoint[0] = point[0] + lidarOffset[0]
        newPoint[1] = point[1] + lidarOffset[1]
        newPoint[2] = point[2]
        #print("point was " + str(point))
        #print("new point is " + str(newPoint))
        return newPoint

    # Returns the angle from the robot to a certain point, needs to be in the range - pi to
    # pi with -pi at the back of the robot
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculateAngle(self, x, y):
        angle = math.atan2(y, x) + math.pi
        angle = -1 * angle
        if angle < -math.pi:
            angle = (2*math.pi) + angle
        return angle

    # Returns the correct angle segment from around the robot depnding on the point passed in.
    # If point is 1,1 that is at 45 degrees to the robot, if there are 360 segments, it would be
    # segment 45
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Feb 2021

    ###################################### END ############################################
    def calculateAngleSegmentIndex(self, angle, numberOfSegments , lowerBoundAngle, upperBoundAngle):
        #angleSegment = (angle / (2 * math.pi)) * numberOfSegments
        angleSegment = self.mapValue(angle, lowerBoundAngle, upperBoundAngle, 0, numberOfSegments)
        angleSegment = int(angleSegment)
        #print("angle: " + str(angle))
        #print("angle seg: " + str(angleSegment))
        self.angleSegUpdated[angleSegment] = True
        return angleSegment

    # Calculates the pythagorean distance to any point from the robot
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculatePythagorasDistance(self, x, y):
        distance = math.sqrt(math.pow(x,2) + math.pow(y,2))
        return distance

    # returns true or false depending on whether a certain point could be an obstacle that the
    # robot could crash with. Any points above the robots height or that are below the wheels.
    # A lot of points will be the lasers hitting the floor which are not obstacles!
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def checkIfPointWithinObstacleHeight(self, heightOfObstacle, heightOfRobot, heightOfLidar):
        pointWithinRobotHeight = False
        heightOfObstacle = heightOfObstacle + heightOfLidar

        if((heightOfObstacle < heightOfRobot) and heightOfObstacle > 0.01):
            pointWithinRobotHeight = True


        return pointWithinRobotHeight

    # Checks if a given point is in the occlusion zone foir the robot, that is an angle segment that
    # will have sensor infomation, however the lidar picks up the robot itself for some angle beams
    # which are useless measuremenbts and want to be occluded.
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def checkIfPointInOcclusionZone(self, point, occlusionZone1):
        pointInOcclusionZone = False
        #Check if in occlusion zone 1
        if point[0] > occlusionZone1[0].x and point[0] < occlusionZone1[0].x + occlusionZone1[1]:
            if point[1] > occlusionZone1[0].y and point[1] < occlusionZone1[0].y + occlusionZone1[2]:
                if point[2] > occlusionZone1[0].z and point[2] < occlusionZone1[0].z + occlusionZone1[3]:
                    pointInOcclusionZone = True


        return pointInOcclusionZone

    # Returns the lower of two distances, primarily used to make sure the closes obstacle
    # in an angle segment is registered
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def returnLowest(self, distance1, distance2):
        if(distance1 < distance2):
            return distance1
        else:
            return distance2

    # The lidar data is not perfect so sometimes certain angle segments dont get an update value,
    # even though likely it wouldnt go from an obstacle in one segment and then max range in the next
    # this replaces any un updated values with the value in the previosu angle segment
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Feb 2021

    ###################################### END ############################################
    def eliminateOutliers(self, obstacleAngleArray, maxVal):
        for angleSeg in range(len(obstacleAngleArray)):
            if(obstacleAngleArray[angleSeg] > maxVal and self.angleSegUpdated[angleSeg] == True):
                maxVal = obstacleAngleArray[angleSeg]

        for angleSeg in range(len(obstacleAngleArray) - 2):
            if(self.angleSegUpdated[angleSeg] == True and self.angleSegUpdated[angleSeg + 1] == False and self.angleSegUpdated[angleSeg + 2] == True):
                obstacleAngleArray[angleSeg + 1] = obstacleAngleArray[angleSeg]

        return obstacleAngleArray


    # maps a value from one range to another, useful for mapping from degrees to radians and
    # back again
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue
