#!/usr/bin/env python
import numpy as np
import math
import rospy  # rospy library
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import String
from Queue import Queue
from nav_msgs.msg import OccupancyGrid
import matplotlib.pyplot as plt
import matplotlib as mpl

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap = []
Position = Point()
map_open = []
map_closed = []
frontier_open = []
frontier_closed = []
reachable_frontier_midpoints = []


environmentMapped = False
searchForFrontiers = False
mapDataReceived = False
publishedPoint = Point(0, 0, 0)
rotationActive = False

angleRotationPublisher = rospy.Publisher('desired_angle_rotation', numpy_msg(Floats), queue_size=1)
frontierPointPublisher = rospy.Publisher('nextFrontierPoint', Point, queue_size=1)

def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position, searchForFrontiers

    if searchForFrontiers == False:
        # get position of robot from odometry message
        Position = msg.pose.pose.position


def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap, searchForFrontiers

    if searchForFrontiers == False:
        # get size of map occupancy grid in rows and columns
        map_columns = msg.info.width
        map_rows = msg.info.height

        # get map resolution
        map_res = msg.info.resolution

        # get coordinates of gmap cell[0,0]
        map_origin = msg.info.origin.position

        # get occupancy grid map
        gmap = msg.data

        gmap = np.array(gmap)

        gmap = gmap.reshape(map_rows, map_columns)

def clbk_frontierPointStatus(data):
    #write code here!!
    global searchForFrontiers
    global angleRotationPublisher
    global rotationActive
    global environmentMapped
    data = str(data)
    if(data.find("new_point_requested") != -1):
        if(searchForFrontiers == False and rotationActive ==  False):
            rotationActive = True
            anglePubValue = np.array([2 * math.pi], dtype = np.float32)
            angleRotationPublisher.publish(anglePubValue)

    elif(data.find("point_non_reachable") != -1):
        #If there are still points to attempt to publish
        try:
            reachable_frontier_midpoints.pop(0)
        except:
            print("attemtped to pop frontier point list when none left, stopped crash")
        if(len(reachable_frontier_midpoints) != 0):
            publishedPoint = Point(0, 0, 0)
            publishedPoint.x = reachable_frontier_midpoints[0][0]
            publishedPoint.y = reachable_frontier_midpoints[0][1]
            frontierPointPublisher.publish(publishedPoint)
            print("new point published " + str(publishedPoint))
        else:
            print("no more possible points, GPP has rejected all of them, therefore finished?")
            environmentMapped = True



def clbk_rotComplete(data):
    #write code here
    global searchForFrontiers
    global rotationActive
    if(searchForFrontiers == False):
        print("rotation complete received, searching for frontiers")
        searchForFrontiers = True
        rotationActive = False

def is_valid_cell(cell):
    global map_rows, map_columns

    #returns whether or not cell indices are valid

    r = cell[0]
    c = cell[1]

    if r < 0 or c < 0:
        return False
    elif r > map_rows or c > map_columns:
        return False
    else:
        return True

def is_frontier_point(cell):
    global gmap
    #returns whether cell is a frontier point or not

    # default is false
    is_frontier_point = False

    #get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for if boundary between open and unknown is crossed
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[r][c] == -1 and gmap[adj_r][adj_c] == 0:
            #if cell is unknown and an adjacent cell is open space then it is a frontier point
            is_frontier_point = True

    #return whether cell is a frontier point
    return is_frontier_point

def is_map_open(cell):
    global map_open

    #returns whether cell is map-open or not

    if map_open[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_map_closed(cell):
    global map_closed

    # returns whether cell is map-closed or not

    if map_closed[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_frontier_open(cell):
    global frontier_open

    # returns whether cell is frontier-open or not

    if frontier_open[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_frontier_closed(cell):
    global frontier_closed

    # returns whether cell is frontier-closed or not

    if frontier_closed[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def mark(cell,marker):
    global map_open, map_closed, frontier_open, frontier_closed

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    #mark cell in appropriate array according to marker
    if marker == "map_open":
        map_open[r][c] = True
    elif marker == "map_closed":
        map_closed[r][c] = True
    elif marker == "frontier_open":
        frontier_open[r][c] = True
    elif marker == "frontier_closed":
        frontier_closed[r][c] = True
    else:
        print("Error in mark function marker input!")


def queue_f_adjacent(cell, queue):
    #adds adjacent points to frontier queue if they're not already in it or otherwise explored

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for frontier points
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        w = [adj_r,adj_c]

        if is_valid_cell(w) == False:
            continue

        if is_frontier_open(w) == False and is_frontier_closed(w) == False and is_map_closed(w) == False:
            queue.put(w)
            mark(w,"frontier_open")

def queue_m_adjacent(cell, queue):
    #adds adjacent points to frontier queue if they're not already in it or otherwise explored

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for frontier points
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        v = [adj_r,adj_c]

        if is_valid_cell(v) == False:
            continue

        if is_map_open(v) == False and is_map_closed(v) == False and has_open_space_neighbour(v) == True:
            queue.put(v)
            mark(v,"map_open")

def has_open_space_neighbour(cell):
    global gmap
    #returns whether cell has open space neighbours or not

    #default is false
    has_open_neighbour = False

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for open cells
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[adj_r][adj_c] == 0:
            has_open_neighbour = True

    return has_open_neighbour

def find_robot_cell():
    global Position, map_origin, map_res

    #finds index of gmap cell in centre of robot

    start_r = int(round((Position.y - map_origin.y) / map_res - 1))
    start_c = int(round((Position.x - map_origin.x)/map_res - 1))

    return [start_r, start_c]

def indices_to_coordinates(cell_list):
    global map_res, map_origin
    #converts cell indices to x,y coordinates for a list of cells
    #returns list of coordinates

    coordinate_list = [] #initialise coordinate list

    for cell in cell_list:
        r = cell[0]
        c = cell[1]
        y = (r + 1)*map_res + map_origin.y
        x = (c + 1)*map_res + map_origin.x
        coordinate_list.append([x,y])

    return coordinate_list

def frontier_midpoint(coordinate_list):
    #returns midpoint from list of coordinates

    #get list of x and y coordinates respectively
    x_list = [0]*len(coordinate_list)
    y_list = [0]*len(coordinate_list)
    for i in range(len(coordinate_list)):
        point = coordinate_list[i]
        x_list[i] = point[0]
        y_list[i] = point[1]

    #find mid x coordinate and mid y coordinate
    mid_x = (max(x_list) + min(x_list)) / 2
    mid_y = (max(y_list) + min(y_list)) / 2

    return [mid_x, mid_y]

def order_by_closest(coordinate_list):
    global Position
    #order list of coordinates by ascending euclidean distance from robot position

    #find euclidean distance from each point to robot
    dists = [0]*len(coordinate_list)
    for i in range(len(coordinate_list)):
        point = coordinate_list[i]
        x = point[0]
        y = point[1]
        dists[i] = ((x - Position.x)**2 + (y - Position.y)**2)**0.5

    #sort coordinate list in order corresponding to ascending order of dists
    index_order = np.argsort(dists) #gets list of indices in sorted (ascending) order (from dists)

    #create ordered list
    ordered_list = [0]*len(coordinate_list)
    for i in range(len(coordinate_list)):
        ordered_list[i] = coordinate_list[index_order[i]] #reorders coordinate list accordingly

    return ordered_list

def find_nearest_reachable_cell(coordinates):
    global gmap, map_origin, map_columns, map_rows, map_res

    checked = [[False for i in range(map_columns)] for i in range(map_rows)]

    #convert coordinates to row and column indices
    x = coordinates[0]
    y = coordinates[1]
    row = int(round((y - map_origin.y)/map_res - 1))
    col = int(round((x - map_origin.x)/map_res - 1))

    #set start point for search
    start_p = [row, col]
    queue_e = Queue()  # create explore queue
    queue_e.put(start_p)  # enqueue start point to explore queue

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    #initialise reachable cell
    reachable_cell = []

    #beign search
    while queue_e.empty() == False:
        p = queue_e.get() #get next point in explore queue

        #get row and column indices from p
        r = p[0]
        c = p[1]

        #if cell is open space then see if it is surrounded by open space neighbours
        if gmap[r][c] == 0:
            # count open space neighbours
            number_open_neighbours = count_open_space_neighbours(p)

            # if cell is surrounded by open space neighbours then return cell
            if number_open_neighbours == 8:
                reachable_cell = p
                break


        #mark p as checked
        checked[r][c] = True

        # cycle through p's adjacent cells, and add them to explore queue as long as they're valid and unchecked
        for i in range(len(drow)):
            adj_r = r + drow[i]
            adj_c = c + dcol[i]
            adj_cell = [adj_r, adj_c]


            if is_valid_cell(adj_cell) == True and checked[adj_r][adj_c] == False:
                queue_e.put(adj_cell)

    return reachable_cell

def count_open_space_neighbours(cell):
    global gmap
    #returns number of open space neighbours a cell has

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for open cells
    open_space_neighbours = 0
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[adj_r][adj_c] == 0:
            open_space_neighbours += 1

    return open_space_neighbours


def find_open_space_frontier_neighbour(coordinates):
    global gmap, map_origin, map_res
    #returns open space neighbour of a cell, if it has none will return an empty list

    # convert coordinates to row and column indices
    x = coordinates[0]
    y = coordinates[1]
    r = int(round((y - map_origin.y) / map_res - 1))
    c = int(round((x - map_origin.x) / map_res - 1))

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for open cells
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[adj_r][adj_c] == 0:
            break

        else:
            adj_cell = []

    return adj_cell


def frontier_middle(cell_list):
    #returns middle cell from list of cells

    #get list of row and column indices respectively
    row_indices = [0]*len(cell_list)
    col_indices = [0]*len(cell_list)
    for i in range(len(cell_list)):
        row = cell_list[i][0]
        row_indices[i] = row
        col = cell_list[i][1]
        col_indices[i] = col

    #find column and row range
    range_rows = max(row_indices) - min(row_indices)
    range_cols = max(col_indices) - min(col_indices)

    #select whether to sort via rows or columns depending on ranges (use whichever has largest range)
    if range_rows >= range_cols:
        index_order = np.argsort(row_indices)
    else:
        index_order = np.argsort(col_indices)

    #produce ordered cell list by ordering cells in cell_list ascendingly by either row values or column values (decided above)
    ordered_list = [0] * len(cell_list)
    for i in range(len(cell_list)):
        ordered_list[i] = cell_list[index_order[i]]  # reorders cell list accordingly

    #find middle cell in ordered cell list
    middle_cell_index = int(round(len(ordered_list)/2))
    middle_cell = ordered_list[middle_cell_index]

    return middle_cell


def main():
    global map_rows, map_columns, map_open, map_closed, frontier_open, frontier_closed, gmap, map_res
    global environmentMapped, searchForFrontiers, rotationNeeded, mapDataReceived, publishedPoint
    global reachable_frontier_midpoints, frontierPointPublisher

    rospy.init_node('frontier_exploration')

    #subscribe to receive position of robot, and occupancy grid map data
    sub_odom = rospy.Subscriber('/odom', Odometry, clbk_odom)
    sub_map = rospy.Subscriber('/map', OccupancyGrid, clbk_map)

    rospy.Subscriber('frontierPointStatus', String, clbk_frontierPointStatus)

    rospy.Subscriber('rotation_complete', String, clbk_rotComplete)

    rate = rospy.Rate(0.4)
    while not rospy.is_shutdown():
        rate.sleep()

################################################### FIND FRONTIERS #####################################################
        # If the environment is not mapped, find new frontiers
        if(environmentMapped == False):
            if(searchForFrontiers == True):
                #initialise lists to track occupancy grid cell states
                print("started calculating frontiers")
                map_open = [[False for i in range(map_columns)] for i in range(map_rows)]
                map_closed = [[False for i in range(map_columns)] for i in range(map_rows)]
                frontier_open = [[False for i in range(map_columns)] for i in range(map_rows)]
                frontier_closed = [[False for i in range(map_columns)] for i in range(map_rows)]

                #initialise list to store found frontiers in
                frontiers = []

                # find frontier length/size to find
                min_frontier_size = int(round(0.6 / map_res))

                #define markers
                mo = "map_open"
                mc = "map_closed"
                fo = "frontier_open"
                fc = "frontier_closed"

                #initiate WFD process
                #find index of cell at centre of robot to begin search
                start_p = find_robot_cell()
                queue_m = Queue() #create explore queue
                queue_m.put(start_p) #enqueue start point to explore queue
                mark(start_p, mo) #mark start point as map-open

                #deal with each point in explore queue
                while queue_m.empty() == False:
                    p = queue_m.get() #get next point in explore queue

                    #skip to next point in explore queue if this point has already been explored
                    if is_map_closed(p) == True:
                        continue

                    #otherwise
                    #see if point is a frontier point
                    if is_frontier_point(p) == True:
                        #if point is a frontier point then:
                        queue_f = Queue() #create a frontier point queue
                        new_frontier = [] #create new frontier point list
                        queue_f.put(p) #add point to frontier point list
                        mark(p, fo) #mark point as frontier-open

                        #frontier extraction now begins

                        #deal with each point in frontier queue
                        while queue_f.empty() == False:
                            q = queue_f.get() #get next point in frontier queue

                            #skip to next point in frontier queue if point cannot be added to new frontier list because it's already explored
                            if is_map_closed(q) == True or is_frontier_closed(q) == True:
                                continue

                            #otherwise
                            #see if point is a frontier point
                            if is_frontier_point(q) == True:
                                #if point is a frontier point then:
                                new_frontier.append(q) #add point to frontier list

                                queue_f_adjacent(q, queue_f) #explore points adjacent to q and add unexplored ones to frontier queue

                            mark(q, fc) #mark point as frontier-closed

                        # save frontier point list to list of all frontiers
                        if len(new_frontier) >= min_frontier_size: #ignore frontiers containing less than minimum number of cells (removes empty elements from frontiers too)
                            frontiers.append(new_frontier) #add frontier point list to list of all frontiers

                        #frontier extraction ends

                    #exploration continues by adding unexplored cells to explore queue
                    queue_m_adjacent(p, queue_m) #explore points adjacent to p and add valid unexplored ones to explore queue
                    mark(p, mc) #mark p as map-closed


        ########################################################################################################################

        ########################################### ORDER FRONTIERS BY CLOSEST #################################################
                print("frontiers extracted")
                frontiers_middles = [] #initialise list to store frontier middles
                for frontier in frontiers:
                    #for each frontier in frontiers:

                    #get middle cell of frontier
                    middle = frontier_middle(frontier)

                    #add frontier middle to list of frontier middles
                    frontiers_middles.append(middle)

                # convert frontier middles to coordinates from cell indices
                frontiers_midpoints = indices_to_coordinates(frontiers_middles)
                #order list of frontier midpoints (coordinates) by euclidean distance to robot position
                closest_frontier_midpoints = order_by_closest(frontiers_midpoints)
                print("frontiers ordered")

        ########################################################################################################################

        ################################ FIND NEAREST REACHABLE POINTS FROM FRONTIER MIDPOINTS #################################

                print("number of frontiers: " + str(len(closest_frontier_midpoints)))
                reachable_frontier_midpoint_cells = [] #initialise list of reachable points

                # if there are no frontier midpoints then exploration is finished
                if len(closest_frontier_midpoints) == 0:
                    print('Exploration complete!')
                    environmentMapped = True

                #otherwise, convert frontier midpoints to coordinates reachable by global path planner
                else:
                    for point in closest_frontier_midpoints:
                        #return reachable cell
                        reachable_point = find_open_space_frontier_neighbour(point)
                        # filter out possible empty coordinates (shouldn't need anymore using newest method)
                        if len(reachable_point) == 2:
                            #add reachable cell to list
                            reachable_frontier_midpoint_cells.append(reachable_point)

                    # convert reachable cells to coordinates that can be sent to path planner
                    reachable_frontier_midpoints = indices_to_coordinates(reachable_frontier_midpoint_cells)
                    print("points ready to send")

        ################################################ PLOTTING TO TEST ######################################################

                #plot received gmap as a colour map

                #create merged list of all frontier points for plotting (can no longer tell where each frontier starts/ends
                #list created is: rows = 1, columns = number of frontier points/x,y coordinate pairs
                # all_frontier_points = []
                # for frontier in frontiers:
                #     all_frontier_points = all_frontier_points + frontier
                #
                # #convert list to numpy array for reordering
                # all_frontier_points = np.array(all_frontier_points)
                # #get total number of frontier points/x,y coordinate pairs
                # total_frontier_points = int(sum(len(row) for row in frontiers))
                # #reorder array/list so its: rows = number of frontier points/x,y coordinate pairs, columns = 2 (one for x, other for y)
                # all_frontier_points = all_frontier_points.reshape(total_frontier_points, 2)
                #
                # # create gmap with frontier points highlighted
                # gmap_highlighted = gmap
                #
                # #mark all frontier points in red
                # for point in all_frontier_points:
                #     r = point[0]
                #     c = point[1]
                #     gmap_highlighted[r][c] = 150
                #
                # #mark reachable frontier midpoints in green
                # for reachable_point in reachable_frontier_midpoint_cells:
                #     r = reachable_point[0]
                #     c = reachable_point[1]
                #     gmap_highlighted[r][c] = 250
                #
                #
                # # make colour map of fixed colours: dark grey, light grey, black, red
                # colour_map = mpl.colors.ListedColormap(['#808080', '#C5C9C7', 'black', 'red', 'green'])
                # # specify boundary values for each colour
                # bounds = [-1.5, -0.5, 0.5, 101, 200, 300]
                # # set how to scale values to be coloured
                # norm = mpl.colors.BoundaryNorm(bounds, colour_map.N)
                #
                # # create plot
                # plt.imshow(gmap_highlighted, interpolation='nearest', cmap=colour_map, norm=norm)
                # plt.gca().invert_yaxis()
                # plt.suptitle("Gmap (frontier points highlighted)")
                #
                # plt.show()

                searchForFrontiers = False
                mapDataReceived = False
                publishedPoint.x = reachable_frontier_midpoints[0][0]
                publishedPoint.y = reachable_frontier_midpoints[0][1]
                print("new frontier point published " + str(publishedPoint))
        else:
            print("area is fully mapped, SUCCESS!!!!")

        if(rotationActive == False):
            frontierPointPublisher.publish(publishedPoint)


########################################################################################################################

######################################## PUBLISH POINTS TO PATH PLANNER ################################################

########################################################################################################################

if __name__ == '__main__':
    main()
