#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg

from create_obstacle_map_from_sensors import flattenLidar2DMap
from calculate_robot_force_PF import obstclDetGaussPotentialFields
from bug0 import bug0

class localPathPlanning():
    def __init__(self):
        self.nextGoalPoint = Point(0,0,0)
        self.currentRawGlobalPath = np.array([])
        self.currentRefinedGlobalPath = np.array([])
        self.remainingGlobalPath = np.array([])

        self.navigating = False
        self.robotAlginedToGoal = True

        self.lidarOffset = [0, 0.136]
        self.numberAngleSegments = 150
        self.flattenNearestObstacles = flattenLidar2DMap(self.numberAngleSegments, 0.246, 0.36185, self.lidarOffset)
        self.distThreshold = 4
        self.ODGPF = obstclDetGaussPotentialFields(self.distThreshold, 0.2, 10, self.numberAngleSegments)
        self.bugAlgorithm = bug0()
        self.LidarDataPoints = 0
        self.newLIDARDataReceived = False

        self.robotPoint = Point(0, 0, 0)
        self.robotYaw = 0
        self.usingBackupLocalPlanner = False

        self.robotVel = 0
        self.robotw = 0
        self.robotHeading = 0

        self.lastTimeRobotPosStored = time.time()
        self.newPointReceivedTime = time.time()
        self.newPathReceived = False
        self.robotPosLog = []
        self.robotStuckStoredPoint = Point(0, 0, 0)
        self.robotMissedPointCounter = 0
        self.robotFailedThisNavWithPFCounter = 0


        self.ANGLE_DIFF_MIN_ALIGNED = 0.1
        self.ROTATION_DAMPING_FACTOR = 0.8
        self.ROTATION_MAX_ACCEL = 0.4
        self.MAX_ROBOT_ANGULAR_VEL = 2.0

        self.MIN_DIST_TO_GLOBAL_MIDPOINT = 1
        self.MIN_DIST_TO_GLOBAL_ENDPOINT = 0.4

        self.TIME_BETWEEN_STORING_ROBOT_POS = 2
        self.NUMBER_OF_STORED_ROBOT_POS = 10
        self.MINIMUM_REQURIED_DIST_DECREASE = 0.15
        self.POINTS_MISSED_LOCAL_MINIMA = 2
        self.MIN_UNSTUCK_DIST_GAIN = 0.7



        rospy.on_shutdown(self.shutDown)
        rospy.Subscriber('/UGV/laser/scan', PointCloud2, self.callback_pointcloud)
        rospy.Subscriber('globalPath', numpy_msg(Point), self.callback_globalPath)
        odom_sub = rospy.Subscriber('/odom', Odometry, self.callback_robotPose)
        self.goalStatusPublisher = rospy.Publisher('goalStatus', String, queue_size=1)
        self.desiredVelPublisher = rospy.Publisher('desired_vel', numpy_msg(Floats), queue_size=1)
        #self.pathPointReceivedPublisher = rospy.Publisher('pointReceived', String, queue_size=1)


    def shutDown(self):
        print("shut down called")
        desiredVel = np.array([0, 0, 0], dtype=np.float32)
        self.desiredVelPublisher.publish(desiredVel)

    def callback_robotPose(self, data):
        robotPose = data
        self.robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, self.robotYaw) = euler_from_quaternion (robotQuaternionList)


    def callback_globalPath(self, data):
        #print("goal point: " + str(data))
        newPathPoint = data
        currentTime = time.time()
        #print("new point received: " + str(data))
        if(len(self.currentRawGlobalPath) == 0):
            #print("new goal point received: " + str(newPathPoint))
            self.currentRawGlobalPath = np.append(self.currentRawGlobalPath, newPathPoint)
            self.remainingGlobalPath = self.currentRawGlobalPath
            self.newPointReceivedTime = currentTime
            self.newPathReceived = True
        else:
            if(newPathPoint != self.currentRawGlobalPath[len(self.currentRawGlobalPath) - 1]):
                #print("new goal point received: " + str(newPathPoint))
                self.currentRawGlobalPath = np.append(self.currentRawGlobalPath, newPathPoint)
                self.remainingGlobalPath = self.currentRawGlobalPath
                # Instead of minimum number of points just add a second delay maybe.


    def callback_pointcloud(self, data):
        self.LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        self.newLIDARDataReceived = True


    def handleLocalPathPlanning(self):
        currentTime = time.time()
        if(self.navigating == False and self.newPathReceived == True):
            if(currentTime - self.newPointReceivedTime > 2):
                self.newPathReceived = False
                self.navigating = True
                print("BEGIN NAVIGATION, final destination is: " + str(self.remainingGlobalPath[len(self.remainingGlobalPath)-1]))
                self.robotPosLog = []

        if self.navigating == True:
            self.localPathPlannerNavigation()
        else:
            if(self.newPathReceived == False):
                self.askForNewGlobalPath()

    def updateRobotPositionLog(self, robotPoint, time):
        # If enough time has passed to store a new position
        if(time - self.lastTimeRobotPosStored > self.TIME_BETWEEN_STORING_ROBOT_POS):
            self.robotPosLog.append(robotPoint)
            if(len(self.robotPosLog) > self.NUMBER_OF_STORED_ROBOT_POS):
                self.robotPosLog.pop(0)
                return True

        return False

    def checkIfRobotStuckPF(self, currentRobotPoint):
        if(len(self.robotPosLog) < self.NUMBER_OF_STORED_ROBOT_POS):
            return False
        else:
            # distsToGoal = []
            # # calculate distance to goal for each of the last positions
            # for i in range(len(self.robotPosLog) - 1):
            #     dist = self.distBetweenPoints(self.robotPosLog[i], self.remainingGlobalPath[len(self.remainingGlobalPath) - 1])
            #     distsToGoal.append(dist)
            #
            # sumDistDecrease = 0
            # for i in range(len(self.robotPosLog) - 2):
            #     distDecrease = distsToGoal[i] - distsToGoal[i + 1]
            #     sumDistDecrease = sumDistDecrease + distDecrease
            #
            # averageDistDecrease = sumDistDecrease / (len(self.robotPosLog) - 2)
            # print("average dist decrease is: " + str(averageDistDecrease))
            #
            # if(averageDistDecrease < self.MINIMUM_REQURIED_DIST_DECREASE):
            #     self.robotStuckStoredPoint = currentRobotPoint
            #     self.robotPosLog = []
            #     print("robot is stuck")
            #     return True
            # else:
            #     return False

            avgPoint = Point(0, 0, 0)
            for i in range(len(self.robotPosLog)):
                avgPoint.x = avgPoint.x + self.robotPosLog[i].x
                avgPoint.y = avgPoint.y + self.robotPosLog[i].y

            avgPoint.x = avgPoint.x / len(self.robotPosLog)
            avgPoint.y = avgPoint.y / len(self.robotPosLog)
            #print("average point is: " + str(avgPoint))

            distsToAvgPoint = []
            for i in range(len(self.robotPosLog)):
                dist = self.distBetweenPoints(self.robotPosLog[i], avgPoint)
                distsToAvgPoint.append(dist)

            sumDistDeviation = 0
            for i in range(len(self.robotPosLog)):
                sumDistDeviation = sumDistDeviation + distsToAvgPoint[i]

            avgDistDeviation = sumDistDeviation / len(self.robotPosLog)
            #print("avg dist deviation is: " + str(avgDistDeviation))

            if(avgDistDeviation < self.MINIMUM_REQURIED_DIST_DECREASE):
                self.robotStuckStoredPoint = currentRobotPoint
                self.robotPosLog = []
                print("robot is stuck")
                return True
            else:
                return False

    def resolveRobotStuck(self, robotStuckPoint, currentRobotPoint, currTime):
        robotUnstuck = False
        goalPoint = self.remainingGlobalPath[len(self.remainingGlobalPath)-1]
        distToGoalNow = self.distBetweenPoints(goalPoint, currentRobotPoint)
        distToGoalBefore = self.distBetweenPoints(goalPoint, robotStuckPoint)
        if(distToGoalBefore - distToGoalNow > self.MIN_UNSTUCK_DIST_GAIN):
            robotUnstuck = True

        if(currTime - self.bugStartTime > 25):
            robotUnstuck = True

        return robotUnstuck

    def localPathPlannerNavigation(self):
        timeNow = time.time()
        # print("navigating to point " + str(self.remainingGlobalPath[0]))
        # print("final goal point is " + str(self.remainingGlobalPath[len(self.remainingGlobalPath) - 1]))

        robotAtGoal = self.checkIfGoalReached(self.remainingGlobalPath[0], self.robotPoint, len(self.remainingGlobalPath))
        if(robotAtGoal == True):
            self.updatePathStatus()
        else:
            # Navigate via potential fields algorithm
            if(self.usingBackupLocalPlanner == False):
                self.handleNavigationLogicForPF(timeNow)

            # Navigate via bug algorithm
            else:
                self.handleNavigationLogicBug(timeNow)



    def handleNavigationLogicForPF(self, timeNow):
        self.navigateWithBugOrPotentialFields(self.usingBackupLocalPlanner)
        checkRequired = self.updateRobotPositionLog(self.robotPoint, timeNow)
        pointNonReachable = False
        if(checkRequired == True):
            pointNonReachable = self.checkIfRobotStuckPF(self.robotPoint)

        if(pointNonReachable == True):
            self.handlePointNonReachable()

    def handlePointNonReachable(self):
        #check if last point in global path?
        if(len(self.remainingGlobalPath) <= 1):
            # reset navigation variables
            # ask for new global path cos this is GNRON
            self.resetNavigationVariables()
            self.askForNewGlobalPath()
            print("GNRON reported")
        else:
            self.robotMissedPointCounter = self.robotMissedPointCounter + 1
            self.remainingGlobalPath = np.delete(self.remainingGlobalPath, 0, 0)
            if(self.robotMissedPointCounter >= self.POINTS_MISSED_LOCAL_MINIMA):
                print("switching to bug")
                self.switchToBugAlgorithm()
                # switch to bug algorithm

    def handleNavigationLogicBug(self, timeNow):
        self.navigateWithBugOrPotentialFields(self.usingBackupLocalPlanner)
        if(self.resolveRobotStuck(self.robotStuckStoredPoint, self.robotPoint, timeNow) == True):
            self.switchBackToPF()

    def switchBackToPF(self):
        self.usingBackupLocalPlanner = False
        print("switching back to PF")
        self.robotPosLog = []
        self.robotMissedPointCounter = 0


    def switchToBugAlgorithm(self):
        self.bugStartTime = time.time()
        self.usingBackupLocalPlanner = True

    def navigateWithBugOrPotentialFields(self, backupAlgorithmStatus):

        desiredVel = 0
        desiredHeading = 0
        if self.newLIDARDataReceived == True:
            self.newLIDARDataReceived = False
            flattenedObstacles = self.flattenNearestObstacles.return2DObstacleMap(self.LidarDataPoints)
            if backupAlgorithmStatus == False:
                #use main algorithm
                theta_goalHeadingNoRobotOrientation = self.calculateRobotToGoalHeading(self.remainingGlobalPath[0], self.robotPoint)

                theta_goalHeadingWithRobotOrientation = self.changeHeadingToRobotFrame(theta_goalHeadingNoRobotOrientation, self.robotYaw)
                self.robotHeading, fieldStrength = self.ODGPF.calcHeadingAngle(theta_goalHeadingWithRobotOrientation, flattenedObstacles)
                self.robotw = self.calcRobotAngularForGoalAlignment(self.robotw, self.robotYaw, theta_goalHeadingNoRobotOrientation)
                self.robotVel = self.mapValue(fieldStrength, 0, 20, 0.6, 0.2)
            else:
                # use bug algorithm backup
                self.robotVel, self.robotw = self.bugAlgorithm.navigateViaBug(self.robotYaw, self.robotPoint, self.remainingGlobalPath[0], flattenedObstacles, self.numberAngleSegments)
                self.robotHeading = 0

        desiredVel = np.array([self.robotVel, self.robotHeading, self.robotw], dtype=np.float32)
        self.desiredVelPublisher.publish(desiredVel)

    def calcRobotAngularForGoalAlignment(self, lastRobotw, robotOrientation, theta_robotToGoal):
        angularVel = 0

        angleDiff = robotOrientation - theta_robotToGoal

        # Keeps angle diff between -pi and pi
        if(angleDiff < -math.pi):
            angleDiff = angleDiff + (2*math.pi)
        if(angleDiff > math.pi):
            angleDiff = angleDiff - (2*math.pi)

        if abs(angleDiff) < self.ANGLE_DIFF_MIN_ALIGNED:
            angularVel = 0
        else:
            accel = self.mapValue(angleDiff, -math.pi, math.pi, self.ROTATION_MAX_ACCEL, -self.ROTATION_MAX_ACCEL)
            # Apply acceleration
            angularVel = lastRobotw + accel
            # Apply damping
            angularVel = self.ROTATION_DAMPING_FACTOR * angularVel

        return angularVel

    def updatePathStatus(self):
        self.remainingGlobalPath = np.delete(self.remainingGlobalPath, 0, 0)
        if(len(self.remainingGlobalPath != 0)):
            #print("new point reachd, next goal: " + str(self.remainingGlobalPath[0]))
            pass
        else:
            print("global path point achieved, requesting new global path point")
            self.resetNavigationVariables()
            self.askForNewGlobalPath()

    def resetNavigationVariables(self):
        self.usingBackupLocalPlanner = False
        self.currentRawGlobalPath = []
        self.robotAlginedToGoal = True
        self.navigating = False
        desiredVel = np.array([0, 0, 0], dtype=np.float32)
        self.desiredVelPublisher.publish(desiredVel)
        self.newPathReceived = False
        self.robotFailedThisNavWithPFCounter = 0
        self.remainingGlobalPath = np.array([])
        self.robotPosLog = []
        self.robotMissedPointCounter = 0

    def askForNewGlobalPath(self):
        self.goalStatusPublisher.publish("next")


    def calculateRobotToGoalHeading(self, goalPoint, robotPoint):
        changeInX = goalPoint.x - robotPoint.x
        changeInY = goalPoint.y - robotPoint.y
        theta_robotToGoal = math.atan2(changeInY, changeInX)            # returns a global headin betwen -Pi and Pi

        return theta_robotToGoal

    def changeHeadingToRobotFrame(self, theta_robotToGoal, robotYaw):
        theta_heading = robotYaw - theta_robotToGoal
        if(theta_heading < -math.pi):
            theta_heading = theta_heading + (2*math.pi)
        if(theta_heading > math.pi):
            theta_heading = theta_heading - (2*math.pi)
        return theta_heading

    def checkIfGoalReached(self, goalPoint, robotPoint, lengthOfRemainingPath):
        changeInX = goalPoint.x - robotPoint.x
        changeInY = goalPoint.y - robotPoint.y
        distance = self.distBetweenPoints(goalPoint, robotPoint)
        # If final point is the global end point, be stricter
        if(lengthOfRemainingPath == 1):
            if distance < self.MIN_DIST_TO_GLOBAL_ENDPOINT:
                return True
            else:
                return False
        else:
            if distance < self.MIN_DIST_TO_GLOBAL_MIDPOINT:
                return True
            else:
                return False


    def distBetweenPoints(self, point1, point2):
        changeInX = point2.x - point1.x
        changeInY = point2.y - point1.y
        distance = math.sqrt(math.pow(changeInX, 2) + math.pow(changeInY, 2))
        return distance

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter


        return newValue

    def plotLiveData(self):
        global plotDelay
        global ODGPF
        plotDelay = plotDelay + 1
        if plotDelay > 5:
            plotDelay = 0
            plt.figure(1, figsize = (8, 8))
            plt.cla()
            plt.bar(self.flattenNearestObstacles.xAxis, self.flattenNearestObstacles.nearestObstaclesFlattened, align='center', color='#89c3e5')
            plt.axhline(self.distThreshold, color='#e69ca4')
            plt.xlabel('Angle around robot (degrees)', fontsize=16)
            plt.ylabel('Distance to nearest object (m)', fontsize=16)
            plt.title('LIDAR Distance Histogram', fontsize=20)
            plt.xticks(fontsize = 12)
            plt.yticks(fontsize = 12)
            fig = plt.gcf()
            fig.canvas.draw()


            plt.figure(2, figsize = (8, 8))
            plt.cla()
            plt.plot(self.ODGPF.xAxis, self.ODGPF.repulsiveField, '#e82a3d', label = 'Repulsive')
            # old light red: #e69ca4
            plt.plot(self.ODGPF.xAxis, self.ODGPF.attractiveField, '#40d124', label = 'Attractive')
            #old light green #b3d6ac
            plt.plot(self.ODGPF.xAxis, self.ODGPF.totalField, '#259ee6', label = 'Total')
            #old light blue #89c3e5
            plt.xlabel('Angle around robot (degrees)', fontsize=16)
            plt.ylabel('Field strength', fontsize=16)
            plt.title('Gaussian Potential Fields', fontsize = 20)
            plt.xticks(fontsize = 12)
            plt.yticks(fontsize = 12)
            plt.legend(prop={'size': 12})
            # if(len(self.ODGPF.tempGaussRepFields) != 0):
            #     for i in range(self.ODGPF.tempNumberOfObstacles):
            #         try:
            #             #, label = 'gaussrep' + str(i)
            #             plt.plot(self.ODGPF.xAxis, self.ODGPF.tempGaussRepFields[i])
            #         except:
            #             print("random error plotting all repulsive fields")
            fig = plt.gcf()
            fig.canvas.draw()

            # plt.figure(2, figsize = (8, 8))
            # plt.cla()
            # plt.xlabel('Angle around robot / degrees', fontsize=16)
            # plt.ylabel('Field strength', fontsize=16)
            # plt.title('Individual repulsive fields', fontsize = 20)
            # if(len(self.ODGPF.tempGaussRepFields) != 0):
            #     for i in range(self.ODGPF.tempNumberOfObstacles):
            #         try:
            #             #, label = 'gaussrep' + str(i)
            #             plt.plot(self.ODGPF.xAxis, self.ODGPF.tempGaussRepFields[i])
            #         except:
            #             print("random error plotting all repulsive fields")
            #
            # fig = plt.gcf()
            # fig.canvas.draw()

            # plt.figure(3, figsize = (8, 8))
            # plt.cla()
            # plt.plot(self.ODGPF.xAxis, self.ODGPF.repulsiveField, '#e82a3d', label = 'Repulsive')
            # plt.xlabel('Angle around robot / degrees', fontsize=16)
            # plt.ylabel('Field strength', fontsize=16)
            # plt.title('Total repulsive field', fontsize = 20)
            #
            # fig = plt.gcf()
            # fig.canvas.draw()



plotDelay = 0

def main():
    rospy.init_node('local_path_planner', anonymous=True)
    localPathPlanner = localPathPlanning()
    rate = rospy.Rate(5)
    plt.ion()
    while not rospy.is_shutdown():
        localPathPlanner.handleLocalPathPlanning()
        #localPathPlanner.plotLiveData()
        rate.sleep()


if __name__ == '__main__':
    main()
