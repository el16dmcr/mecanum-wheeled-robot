#! /usr/bin/env python
import rospy, math
import numpy as np
from time import sleep
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from nav_msgs.msg import Odometry
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import String
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from sensor_msgs.msg import Range

class moveRobot():
    def __init__(self):
        self.msg = Twist()
        self.desiredRobotVelX = 0
        self.desiredRobotVelY = 0
        self.desiredRobotw = 0

        self.robotw = 0

        self.robotHeading = 0

        self.desiredRotationAngle = 0
        self.rotationOccuring = False

        self.robotCurrentYaw = 0
        self.robotLastYaw = 0
        self.newOdomDataReceived = False
        # Order for ultrasonic sensors will be front, right, back, left
        self.ultrasonicDistance = np.array([0.8, 0.8, 0.8, 0.8])

        # Constants used within this class to remove magic numbers
        self.ANGLE_DIFF_MIN_ALIGNED = 0.1
        self.ROTATION_DAMPING_FACTOR = 0.8
        self.ROTATION_MIN_ACCEL  = 0.1
        self.ROTATION_MAX_ACCEL = 0.4
        self.MAX_ROBOT_ANGULAR_VEL = 2.0
        self.MAX_UDU_SENSOR_RANGE = 0.8
        self.MIN_DIST_TO_OBSTACLES_DESIRED = 0.2

        rospy.Subscriber('desired_vel', numpy_msg(Floats), self.callback_desiredVelocity)
        rospy.Subscriber('desired_angle_rotation', numpy_msg(Floats), self.callback_rotateCommand)
        rospy.Subscriber('/odom', Odometry, self.callback_robotPose)
        rospy.Subscriber('/UGV/ultrasonic/frontScan', Range, self.callback_frontScan)
        rospy.Subscriber('/UGV/ultrasonic/rightScan', Range, self.callback_rightScan)
        rospy.Subscriber('/UGV/ultrasonic/backScan', Range, self.callback_backScan)
        rospy.Subscriber('/UGV/ultrasonic/leftScan', Range, self.callback_leftScan)
        self.movementPublisher = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.rotationCompletePublisher = rospy.Publisher('rotation_complete', String, queue_size=1)
        rospy.on_shutdown(self.shutDown)

    def callback_frontScan(self, data):
        self.ultrasonicDistance[0] = data.range

    def callback_rightScan(self, data):
        self.ultrasonicDistance[1] = data.range

    def callback_backScan(self, data):
        self.ultrasonicDistance[2] = data.range

    def callback_leftScan(self, data):
        self.ultrasonicDistance[3] = data.range

    def callback_robotPose(self, data):
        robotPose = data
        self.robotLastYaw = self.robotCurrentYaw
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, self.robotCurrentYaw) = euler_from_quaternion (robotQuaternionList)
        self.newOdomDataReceived = True

    def callback_rotateCommand(self, data):
        if(self.rotationOccuring == False):
            print("rotate command received")
            self.desiredRotationAngle = data.data[0]
            print("desired rot angle: " + str(data.data[0]))
            self.rotationOccuring = True

    def callback_desiredVelocity(self, data):
        self.desiredRobotVelX = data.data[0] * math.cos(data.data[1])
        self.desiredRobotVelY = -(data.data[0] * math.sin(data.data[1]))
        self.desiredRobotw = data.data[2]

    def handleRobotMovement(self):
        # If the robot has been sent a pure rotation command from frontier exploration
        if(self.rotationOccuring == True):
            # If new odom data received
            if(self.newOdomDataReceived == True):
                # Check to see if desired rotation angle has been achieved
                if(self.desiredRotationAngle < self.ANGLE_DIFF_MIN_ALIGNED):
                    self.rotationOccuring = False
                    self.rotationCompletePublisher.publish('rot_complete')
                    self.msg.linear.x = 0
                    self.msg.linear.y = 0
                    self.msg.angular.z = 0
                    self.robotw = 0
                else:
                    self.msg.linear.x = 0
                    self.msg.linear.y = 0
                    accel = self.mapValue(self.desiredRotationAngle, 0, 2 * math.pi, self.ROTATION_MIN_ACCEL, self.ROTATION_MAX_ACCEL)
                    # Apply acceleration
                    self.robotw = self.robotw + accel
                    # Apply damping
                    self.robotw = self.ROTATION_DAMPING_FACTOR * self.robotw
                    self.msg.angular.z = self.robotw
                    angleChangeLast = self.calculateNormalisedAngleDiff(self.robotLastYaw, self.robotCurrentYaw)

                    # Subtract the angle moved from the desired rotation angle
                    self.desiredRotationAngle = self.desiredRotationAngle - angleChangeLast
                    #print("desired rotation angle left is: " + str(self.desiredRotationAngle))

        else:
            # using subscriber data to the ultrasonics, validate desired velocities and limit them
            # to prevent crashing

            if(self.desiredRobotVelX >= 0):
                if(self.ultrasonicDistance[0] < self.MAX_UDU_SENSOR_RANGE):
                    reductionFactor = self.mapValue(self.ultrasonicDistance[0], self.MIN_DIST_TO_OBSTACLES_DESIRED, self.MAX_UDU_SENSOR_RANGE, 0, 1)
                    actualVelX = self.desiredRobotVelX * reductionFactor
                else:
                    actualVelX = self.desiredRobotVelX

            if(self.desiredRobotVelX < 0):
                if(self.ultrasonicDistance[2] < self.MAX_UDU_SENSOR_RANGE):
                    reductionFactor = self.mapValue(self.ultrasonicDistance[2], self.MIN_DIST_TO_OBSTACLES_DESIRED, self.MAX_UDU_SENSOR_RANGE, 0, 1)
                    actualVelX = self.desiredRobotVelX * reductionFactor
                else:
                    actualVelX = self.desiredRobotVelX

            if(self.desiredRobotVelY >= 0):
                if(self.ultrasonicDistance[3] < self.MAX_UDU_SENSOR_RANGE):
                    reductionFactor = self.mapValue(self.ultrasonicDistance[3], self.MIN_DIST_TO_OBSTACLES_DESIRED, self.MAX_UDU_SENSOR_RANGE, 0, 1)
                    actualVelY = self.desiredRobotVelY * reductionFactor
                else:
                    actualVelY = self.desiredRobotVelY

            if(self.desiredRobotVelY < 0):
                if(self.ultrasonicDistance[1] < self.MAX_UDU_SENSOR_RANGE):
                    reductionFactor = self.mapValue(self.ultrasonicDistance[1], self.MIN_DIST_TO_OBSTACLES_DESIRED, self.MAX_UDU_SENSOR_RANGE, 0, 1)
                    actualVelY = self.desiredRobotVelY * reductionFactor
                else:
                    actualVelY = self.desiredRobotVelY

            self.msg.linear.x = actualVelX
            self.msg.linear.y = actualVelY
            self.msg.angular.z = self.desiredRobotw

        self.movementPublisher.publish(self.msg)

    def calculateNormalisedAngleDiff(self, lastAngle, newAngle):


        angleDiff = abs(newAngle - lastAngle)
        if((2 * math.pi) - angleDiff < angleDiff):
            angleDiff = (2 * math.pi) - angleDiff

        if(angleDiff > 1):
            print("weird angle chnage, lastAngle was: " + str(lastAngle) + " new angle: " + str(newAngle))
        return angleDiff

    def shutDown(self):
        print("shut down called")
        self.stopRobot()

    def stopRobot(self):
        self.msg.angular.x = 0
        self.msg.angular.y = 0
        self.msg.angular.z = 0
        self.msg.linear.x = 0
        self.msg.linear.y = 0
        self.msg.linear.z = 0
        self.movementPublisher.publish(self.msg)

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue


    # def calcRobotVelocities(self, robotAccMagnitude, goalHeadingRobotFrame):
    #     robotXAccel = robotAccMagnitude * math.cos(goalHeadingRobotFrame)
    #     robotYAccel = -robotAccMagnitude * math.sin(goalHeadingRobotFrame)
    #     robotAngularAccel = 0
    #
    #     self.robotVelX = self.robotVelX + robotXAccel
    #     self.robotVelY = self.robotVelY + robotYAccel
    #     self.robotw = self.robotw + robotAngularAccel
    #
    #     self.normaliseVelocities(1)
    #
    #     if self.robotw > 1:
    #         self.robotw = 1
    #     elif self.robotw < -1:
    #         self.robotw = -1
    #
    #     self.msg.linear.x = self.robotVelX
    #     self.msg.linear.y = self.robotVelY
    #     self.msg.angular.z = self.robotw
    #
    #     self.movementPublisher.publish(self.msg)


    # def normaliseVelocities(self, maxVel):
    #
    #     currentTotalVel = math.sqrt(math.pow(self.robotVelX, 2) + math.pow(self.robotVelY, 2))
    #     normalisingFactor = currentTotalVel / maxVel
    #     if currentTotalVel > maxVel:
    #         self.robotVelX = self.robotVelX / normalisingFactor
    #         self.robotVelY = self.robotVelY / normalisingFactor

    # def alignRobotToPoint(self, theta_robotToGoal, robotOrientation):
    #     robotAligned = False
    #     self.msg.linear.x = 0
    #     self.msg.linear.y = 0
    #
    #     angleDiff = robotOrientation - theta_robotToGoal
    #
    #     # Keeps angle diff between -pi and pi
    #     if(angleDiff < -math.pi):
    #         angleDiff = angleDiff + (2*math.pi)
    #     if(angleDiff > math.pi):
    #         angleDiff = angleDiff - (2*math.pi)
    #
    #     if abs(angleDiff) < self.ANGLE_DIFF_MIN_ALIGNED:
    #         self.robotw = 0
    #         robotAligned = True
    #     else:
    #         accel = self.mapValue(angleDiff, -math.pi, math.pi, self.ROTATION_MAX_ACCEL, -self.ROTATION_MAX_ACCEL)
    #         # Apply acceleration
    #         self.robotw = self.robotw + accel
    #         # Apply damping
    #         self.robotw = self.ROTATION_DAMPING_FACTOR * self.robotw
    #
    #     if self.robotw > self.MAX_ROBOT_ANGULAR_VEL:
    #         self.robotw = self.MAX_ROBOT_ANGULAR_VEL
    #     elif self.robotw < -self.MAX_ROBOT_ANGULAR_VEL:
    #         self.robotw = -self.MAX_ROBOT_ANGULAR_VEL
    #
    #     self.msg.angular.z = self.robotw
    #     self.movementPublisher.publish(self.msg)
    #
    #     return robotAligned


def main():
    rospy.init_node('safe_robot_vel_node', anonymous=True)
    safeMoveRobot = moveRobot()
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        safeMoveRobot.handleRobotMovement()
        rate.sleep()

if __name__ == '__main__':
    main()
