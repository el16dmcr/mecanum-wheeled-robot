#! /usr/bin/env python

import rospy, math, random, tf
import numpy as np
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from nav_msgs.msg import OccupancyGrid

from local_path_planning.create_obstacle_map_from_sensors import flattenLidar2DMap
from pose_graph import poseGraph
from pose_graph_testing import poseGraphTesting

# Global variable declaration
robotPoint = Point(0, 0, 0)
robotYaw = 0
LidarDataPoints = 0
numberAngleSegments = 150
lidarOffset = [0, 0.136]

newLIDARDataReceived = False
newRobotPoseReceived = False

flattenedObstacles = []

map_msg = OccupancyGrid()
map_msg.header.frame_id = 'map'
resolution = 0.05
width = 1000
height = 1000

#robot_pose = tf.TransformListener()

# Instantiate any necessary classes
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
poseGraphSLAM = poseGraph(width, height, resolution, numberAngleSegments)
#poseGraphSLAMTesting = poseGraphTesting()



def callback_pointcloud(data):
    global LidarDataPoints
    global newLIDARDataReceived
    if(newLIDARDataReceived == False):
        LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        newLIDARDataReceived = True
        #print("new lidar data")

def callback_robotPose(data):
    global newRobotPoseReceived
    global robotPoint
    global robotYaw
    global newLIDARDataReceived

    if(newLIDARDataReceived == True and newRobotPoseReceived == False):
        robotPose = data
        robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)
        newRobotPoseReceived = True
        #print("new odom data")

def main():
    # Global variable instantiation
    global robotYaw
    global robotPoint
    global newLIDARDataReceived
    global newRobotPoseReceived
    global LidarDataPoints
    global flattenedObstacles

    rospy.on_shutdown(shutDown)
    rospy.init_node('POSE_GRAPH_SLAM', anonymous=True)
    print("SLAM script running")

    br = tf.TransformBroadcaster()

    map_msg.info.resolution = resolution
    map_msg.info.width = width
    map_msg.info.height = height
    map_msg.data = range(width*height)



    map_msg.info.origin.position.x = - width // 2 * resolution
    map_msg.info.origin.position.y = - height // 2 * resolution

    # Setup subscirbers, one for odometry topic and one for lidar sensor topic.
    rospy.Subscriber('/odom', Odometry, callback_robotPose)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, callback_pointcloud)

    #Create the map topic publisher
    mapPub = rospy.Publisher("/UGV/map", OccupancyGrid, queue_size = 10)

    rate = rospy.Rate(1) # 1 Hz
    while not rospy.is_shutdown():
        br.sendTransform((0.0, 0.0, 0.0),
                         (0.0, 0.0, 0.7071, 0.7071),
                         rospy.Time.now(),
                         "odom",
                         "map")

        if( newLIDARDataReceived == True and newRobotPoseReceived == True):
            newLIDARDataReceived = False
            newRobotPoseReceived = False
            flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(LidarDataPoints)

            ########################### ----------------------------------------------------------
            ### missing code needed here  - check new sensor data agaisnt past possible sensor locations
            ##########################--------------------------------------------------------------
            newConstraints = []
            newPose = np.array([[robotPoint.x, robotPoint.y, robotYaw]])
            newSensorData = np.array([flattenedObstacles])
            poseGraphSLAM.updatePoseGraph(newPose, newSensorData, newConstraints)

            grid = poseGraphSLAM.calculateOccupancyGrid()



            map_msg.header.stamp = rospy.Time.now()
            # build ros map message and publish
            for i in range(width*height):
        		map_msg.data[i] = grid.flat[i]
            mapPub.publish(map_msg)




        rate.sleep()


def shutDown():
    print("shutdown time!")

def convertCartesianWorld():
    cartesianPoint_x, cartesianPoint_y  = conversion.degPointToCartesianWrtOrigin(20,20,90,10,0) #(int, int, rad, int, rad)
    print(cartesianPoint_x,cartesianPoint_y)

def convertCartesianRobot():
    cartesianPoint_x, cartesianPoint_y  = conversion.degPointToCartesianWrtRobot(10,0) #(int, rad)
    print(cartesianPoint_x,cartesianPoint_y)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
