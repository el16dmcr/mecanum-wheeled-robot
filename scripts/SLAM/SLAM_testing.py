#! /usr/bin/env python

import rospy
import numpy
import math
import tf
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from tf.transformations import euler_from_quaternion, quaternion_from_euler
import matplotlib.pyplot as plt

from pointConversion import pointConversion
from local_path_planning.create_obstacle_map_from_sensors import flattenLidar2DMap

map_msg = OccupancyGrid()
map_msg.header.frame_id = 'map'
resolution = 0.05
width = 1000
height = 1000

robotX = 0
robotY = 0

robotRadius = 0.3

robotPoint = Point(0, 0, 0)
robotYaw = 0
LidarDataPoints = 0
numberAngleSegments = 300
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
pointConversion = pointConversion()
newLidarDataRecieved = False
newRobotPoseReceived = False
flattenedObstacles = []


def plotLiveData():

    plt.figure(1)
    plt.cla()
    plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5', linewidth= 0.1)
    plt.xlabel('Angle around robot / degrees', fontsize=14)
    plt.ylabel('Distance to nearest object / m', fontsize=14)
    plt.title('Flattened graph of nearest obstacles', fontsize=18)
    fig = plt.gcf()
    fig.canvas.draw()



def setFreeCells(grid, robotPosition, robotRadius):
    global resolution
    off_x = robotPosition.x // resolution + width  // 2
    off_y = robotPosition.y // resolution + height // 2

	# set the roi to 1: known free positions
    for i in range(-robotRadius, robotRadius):
        for j in range(-robotRadius, robotRadius):
            grid[int(i + off_x), int(j + off_y)] = 1


def setObstacleCells(grid, robotPosition, robotOrientation, flattenedObstacles):

    global pointConversion
    off_x = (robotPosition.x // resolution) + (width  // 2)
    off_y = (robotPosition.y // resolution) + (height // 2)
    for obstacleIndex in range(len(flattenedObstacles)): #-150 degree to 150 degree
        if flattenedObstacles[obstacleIndex] < 60:
            angle = mapValue(obstacleIndex, 0, len(flattenedObstacles), (-(5*math.pi)/6), ((5*math.pi)/6))
            #print("obstacle dist from robot: " + str(flattenedObstacles[obstacleIndex]))
            #print("angle is " + str(angle))
            obstacle_x_from_origin, obstacle_y_from_origin  = pointConversion.PointToCartesianWrtOrigin(robotPosition.x, robotPosition.y, robotOrientation, flattenedObstacles[obstacleIndex], angle)
            #print("obstacle x from origin: " + str(obstacle_x_from_origin) + " y: " + str(obstacle_y_from_origin))
            obstacle_grid_x = (obstacle_x_from_origin // resolution) + (width  // 2)
            obstacle_grid_y = (obstacle_y_from_origin // resolution) + (height // 2)
            #print("obstacle x from origin grid: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))
            obstacle = [obstacle_grid_x, obstacle_grid_y]
            #print("obstacel x: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))
            #print("robot x: " + str(off_x) + " y: " + str(off_y))

            grid[int(obstacle[0]), int(obstacle[1])] = int(100)
            if  grid[int(obstacle[0]+1), int(obstacle[1])]   < int(1):
        		grid[int(obstacle[0]+1), int(obstacle[1])]   = int(50)
            if  grid[int(obstacle[0]), 	 int(obstacle[1]+1)] < int(1):
        		grid[int(obstacle[0]),   int(obstacle[1]+1)] = int(50)
            if  grid[int(obstacle[0]-1), int(obstacle[1])]   < int(1):
        		grid[int(obstacle[0]-1), int(obstacle[1])]   = int(50)
            if  grid[int(obstacle[0]),   int(obstacle[1]-1)] < int(1):
        		grid[int(obstacle[0]),   int(obstacle[1]-1)] = int(50)


            change_x = (obstacle_grid_x-off_x)
            change_y = (obstacle_grid_y-off_y)
            grid_distance = int(math.floor(math.sqrt(math.pow(change_x, 2) + math.pow(change_y, 2))))
            #print("grid dist: " + str(grid_distance))
            for i in range(grid_distance):
                new_x = int(off_x + i*math.cos(angle - robotOrientation))
                new_y = int(off_y + i*math.sin(angle - robotOrientation))
                #print("new x: " + str(new_x) + " , new y: " + str(new_y))
                if  grid[new_x, new_y]   < int(1):
                    grid[new_x, new_y] = int(0)

def callback_pointcloud(data):
    global LidarDataPoints
    global newLidarDataRecieved
    LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
    newLidarDataRecieved = True

def callback_robotPose(data):
    robotPose = data
    global newRobotPoseReceived
    global robotPoint
    global robotYaw
    robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
    robotOrientation = robotPose.pose.pose
    robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
    (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)
    newRobotPoseReceived = True

def mapValue(value, lowerBefore, upperBefore, lowerAfter, upperAfter):
    newValue = 0
    newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
    newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

    return newValue

def shutDown():
    print("shutdown")

def main():
    global flattendObstacles
    global LidarDataPoints
    global robotPoint
    global newLidarDataRecieved
    global newRobotPoseReceived
    global robotYaw
    global robotPoint


    rospy.init_node('SLAM_test_planner', anonymous=True)
    rate = rospy.Rate(5)
    plt.ion()

    map_msg.info.resolution = resolution
    map_msg.info.width = width
    map_msg.info.height = height
    map_msg.data = range(width*height)

    grid = numpy.ndarray((width, height), buffer=numpy.zeros((width, height), dtype=numpy.int), dtype=numpy.int)
    grid.fill(int(-1))

    map_msg.info.origin.position.x = - width // 2 * resolution
    map_msg.info.origin.position.y = - height // 2 * resolution

    rospy.Subscriber('/odom', Odometry, callback_robotPose)
    rospy.on_shutdown(shutDown)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, callback_pointcloud)
    mapPub = rospy.Publisher("/UGV/map", OccupancyGrid, queue_size = 10)

    br = tf.TransformBroadcaster()


    while not rospy.is_shutdown():
        br.sendTransform((0.0, 0.0, 0.0),
                         (0.0, 0.0, 0.7071, 0.7071),
                         rospy.Time.now(),
                         "odom",
                         "map")

        if newLidarDataRecieved == True and newRobotPoseReceived == True:
            #plotLiveData()
            newLIDARDataReceived = False
            newRobotPoseReceived = False
            flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(LidarDataPoints)
            setFreeCells(grid, robotPoint, int(robotRadius//resolution))
            setObstacleCells(grid, robotPoint, robotYaw, flattenedObstacles)
            # try:
            #     flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(LidarDataPoints)
            #     setFreeCells(grid, robotPoint, int(robotRadius//resolution))
            #     setObstacleCells(grid, robotPoint, robotYaw, flattenedObstacles)
            # except:
            #     print("error")

        map_msg.header.stamp = rospy.Time.now()
        # build ros map message and publish
    	for i in range(width*height):
    		map_msg.data[i] = grid.flat[i]
    	mapPub.publish(map_msg)

        rate.sleep()


if __name__ == '__main__':
    main()
