#! /usr/bin/env python
import rospy
from math import *
import numpy as np
from nav_msgs.msg import Odometry
import tf

lastOdomPerfect = None
lastOdomNoise = [0.0, 0.0, 0.0]
pose = [0.0,0.0,0.0]
rotNoiseParam = 0.0
transNoiseParam = 0.0
base_frame = ""

# a callback for Gazebo odometry data
def callback(data):
    global lastOdomPerfect
    global lastOdomNoise
    global base_frame
    global pose

    if(lastOdomPerfect == None):
        lastOdomPerfect = data
        pose[0] = data.pose.pose.position.x
        pose[1] = data.pose.pose.position.y
        q = [ data.pose.pose.orientation.x,
                data.pose.pose.orientation.y,
                data.pose.pose.orientation.z,
                data.pose.pose.orientation.w ]
        (r, p, y) = tf.transformations.euler_from_quaternion(q)
        pose[2] = y
        lastOdomNoise[0] = pose[0]
        lastOdomNoise[1] = pose[1]
        lastOdomNoise[2] = pose[2]
    else:
        #calculate the perfect translation from the perfect odom last position and the perfect
        # odom this position
        perfect_dx = data.pose.pose.position.x - lastOdomPerfect.pose.pose.position.x
        perfect_dy = data.pose.pose.position.y - lastOdomPerfect.pose.pose.position.y
        #perfectTranslation = sqrt(dx*dx + dy*dy)

        # Get the current perfect rotation and the last position perfect rotation
        q = [ lastOdomPerfect.pose.pose.orientation.x,
                lastOdomPerfect.pose.pose.orientation.y,
                lastOdomPerfect.pose.pose.orientation.z,
                lastOdomPerfect.pose.pose.orientation.w ]
        (r,p, theta1) = tf.transformations.euler_from_quaternion(q)

        q = [ data.pose.pose.orientation.x,
                data.pose.pose.orientation.y,
                data.pose.pose.orientation.z,
                data.pose.pose.orientation.w ]
        (r,p, theta2) = tf.transformations.euler_from_quaternion(q)

        perfectRotation = theta1 - theta2
        if(perfectRotation > pi):
            perfectRotation = (2 * pi) - perfectRotation
        if(perfectRotation < -pi):
            perfectRotation = perfectRotation + (2 * pi)

        sd_rotation = rotNoiseParam * abs(perfectRotation)
        #sd_translation = transNoiseParam * abs(perfectTranslation)
        sd_transX = transNoiseParam * abs(perfect_dx)
        sd_transY = transNoiseParam * abs(perfect_dy)

        #noisyTranslation = perfectTranslation + np.random.normal(0,sd_translation*sd_translation)
        noisyTransX = perfect_dx + np.random.normal(0,sd_transX*sd_transX)
        noisyTransY = perfect_dy + np.random.normal(0,sd_transY*sd_transY)
        noisyRotation = perfectRotation + np.random.normal(0, sd_rotation*sd_rotation)

        pose[0] = lastOdomNoise[0] + noisyTransX
        pose[1] = lastOdomNoise[1] + noisyTransY
        pose[2] = lastOdomNoise[2] + noisyRotation
        lastOdomPerfect = data

        # publish the tf
        br = tf.TransformBroadcaster()
        br.sendTransform((pose[0] - data.pose.pose.position.x, pose[1] - data.pose.pose.position.y, 0),
            tf.transformations.quaternion_from_euler(0, 0, pose[2] - theta2),
            data.header.stamp,
            "odom",
            "odomNoise")




if __name__ == '__main__':
    rospy.init_node('noisy_odometry', anonymous=True)
    # rotNoiseParam 1 is degree/degree
    if rospy.has_param("~rotNoise"):
        rotNoiseParam = rospy.get_param("~rotNoise")
    else:
        rospy.logwarn("rotation noise is set to default")
        rotNoiseParam = 15.0*pi/180.0

    # alpha 2 is m/m
    if rospy.has_param("~transNoise"):
        transNoiseParam = rospy.get_param("~transNoise")
    else:
        transNoiseParam = 0.2
        rospy.logwarn("translation noise is set to default")

    # get odom topic
    if rospy.has_param("~old_odom_topic"):
        odom_topic = rospy.get_param("~old_odom_topic")
    else:
        odom_topic = "/odomNoise"

    # get base frame
    if rospy.has_param("~base_frame"):
        base_frame = rospy.get_param("~base_frame")
    else:
        base_frame = "/base_footprint"

    rospy.Subscriber("/odom", Odometry, callback)
    print("End of initialisation")
    rospy.spin()
