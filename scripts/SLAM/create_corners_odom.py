#! /usr/bin/env python
import rospy
from math import *
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
import tf

last_odom = None
pose = [0.0,0.0,0.0]
base_frame = ""

odomFLPub = rospy.Publisher("odomFL", Odometry, queue_size = 50)
odomFRPub = rospy.Publisher("odomFR", Odometry, queue_size = 50)
odomBLPub = rospy.Publisher("odomBL", Odometry, queue_size = 50)
odomBRPub = rospy.Publisher("odomBR", Odometry, queue_size = 50)

# a callback for Gazebo odometry data
def callback(data):
    global last_odom
    global base_frame
    global pose
    global odomFLPub, odomFRPub, odomBLPub, odomBRPub
    transformTL = [0.0, 0.0, 0.0]
    transformTR = [0.0, 0.0, 0.0]
    transformBL = [0.0, 0.0, 0.0]
    transformBR = [0.0, 0.0, 0.0]

    poseFL = [0.0, 0.0, 0.0]
    poseFR = [0.0, 0.0, 0.0]
    poseBL = [0.0, 0.0, 0.0]
    poseBR = [0.0, 0.0, 0.0]
    currentTime = rospy.Time.now()

    centreX = data.pose.pose.position.x
    centreY = data.pose.pose.position.y
    q = [ data.pose.pose.orientation.x,
            data.pose.pose.orientation.y,
            data.pose.pose.orientation.z,
            data.pose.pose.orientation.w ]
    (r, p, y) = tf.transformations.euler_from_quaternion(q)

    transformTL[0] = 0.272
    transformTL[1] = 0.3

    transformTR[0] = 0.272
    transformTR[1] = -0.3

    transformBL[0] = -0.272
    transformBL[1] = 0.3

    transformBR[0] = -0.272
    transformBR[1] = -0.3


    poseFL[0] = centreX + (transformTL[0] * cos(y)) - (sin(y) * transformTL[1])
    poseFL[1] = centreY + (transformTL[0] * sin(y)) + (cos(y) * transformTL[1])

    poseFR[0] = centreX + (transformTR[0] * cos(y)) - (sin(y) * transformTR[1])
    poseFR[1] = centreY + (transformTR[0] * sin(y)) + (cos(y) * transformTR[1])

    poseBL[0] = centreX + (transformBL[0] * cos(y)) - (sin(y) * transformBL[1])
    poseBL[1] = centreY + (transformBL[0] * sin(y)) + (cos(y) * transformBL[1])

    poseBR[0] = centreX + (transformBR[0] * cos(y)) - (sin(y) * transformBR[1])
    poseBR[1] = centreY + (transformBR[0] * sin(y)) + (cos(y) * transformBR[1])


    # publish the tf
    br = tf.TransformBroadcaster()
    br.sendTransform((0, 0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     data.header.stamp,
                     "odomFL",
                     "/odom")

    br.sendTransform((0, 0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     data.header.stamp,
                     "odomFR",
                     "/odom")

    br.sendTransform((0, 0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     data.header.stamp,
                     "odomBL",
                     "/odom")

    br.sendTransform((0, 0, 0),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     data.header.stamp,
                     "odomBR",
                     "/odom")

    odometries = [Odometry(), Odometry(), Odometry(), Odometry()]
    odometryNames = ["FL", "FR", "BL", "BR"]

    for i in range(len(odometries)):
        odometries[i].header.stamp = currentTime
        odometries[i].header.frame_id = "odom" + odometryNames[i]
        odometries[i].child_frame_id = "base_footprint"

    odomQuat = tf.transformations.quaternion_from_euler(0, 0, 0)
    odometries[0].pose.pose = Pose(Point(poseFL[0], poseFL[1], 0), data.pose.pose.orientation)
    odometries[1].pose.pose = Pose(Point(poseFR[0], poseFR[1], 0), data.pose.pose.orientation)
    odometries[2].pose.pose = Pose(Point(poseBL[0], poseBL[1], 0), data.pose.pose.orientation)
    odometries[3].pose.pose = Pose(Point(poseBR[0], poseBR[1], 0), data.pose.pose.orientation)


    # odometries[0].pose.pose = Pose(Point(0, 0, 0), Quaternion(odomQuat[0], odomQuat[1], odomQuat[2], odomQuat[3]))
    # odometries[1].pose.pose = Pose(Point(0, 0, 0), Quaternion(odomQuat[0], odomQuat[1], odomQuat[2], odomQuat[3]))
    # odometries[2].pose.pose = Pose(Point(0, 0, 0), Quaternion(odomQuat[0], odomQuat[1], odomQuat[2], odomQuat[3]))
    # odometries[3].pose.pose = Pose(Point(0, 0, 0), Quaternion(odomQuat[0], odomQuat[1], odomQuat[2], odomQuat[3]))

    odomFLPub.publish(odometries[0])
    odomFRPub.publish(odometries[1])
    odomBLPub.publish(odometries[2])
    odomBRPub.publish(odometries[3])







if __name__ == '__main__':
    rospy.init_node('odom_corners', anonymous=True)

    # get odom topic
    if rospy.has_param("~old_odom_topic"):
        odom_topic = rospy.get_param("~old_odom_topic")
    else:
        odom_topic = "/odom"
    # get base frame
    if rospy.has_param("~base_frame"):
        base_frame = rospy.get_param("~base_frame")
    else:
        base_frame = "base_footprint"

    rospy.Subscriber(odom_topic, Odometry, callback)

    rospy.spin()
