#! /usr/bin/env python

import rospy
import numpy
import math
import tf
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from matplotlib import pyplot as plt

from pointConversion import pointConversion
from local_path_planning.create_obstacle_map_from_sensors import flattenLidar2DMap

robotX = 0
robotY = 0

robotRadius = 0.3

robotPoint = Point(0, 0, 0)
robotYaw = 0
LidarDataPoints = 0
numberAngleSegments = 300
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
pointConversion = pointConversion()
newLidarDataReceived = False
newRobotPoseReceived = False
flattenedObstacles = []
angleList = []
xList = []
yList = []

def generateHistogram(flattenedLidar):
    global pointConversion
    global angleList
    global xList
    global yList
    angleList = []
    xList = []
    yList = []
    for obstacleIndex in range(len(flattenedLidar)-1): #-150 degree to 150 degree
        if flattenedLidar[obstacleIndex] < 60:
            angle1 = mapValue(obstacleIndex, 0, len(flattenedLidar), (-(5*math.pi)/6), ((5*math.pi)/6))
            angle2 = mapValue(obstacleIndex+1, 0, len(flattenedLidar), (-(5*math.pi)/6), ((5*math.pi)/6))
            p1_x, p1_y = pointConversion.PointToCartesianWrtRobot(flattenedLidar[obstacleIndex], angle1)
            p2_x, p2_y = pointConversion.PointToCartesianWrtRobot(flattenedLidar[obstacleIndex+1], angle2)
            angle = math.atan2((p2_y - p1_y),(p2_x - p1_x))
            # print(angle)
            angleList.append(angle)
            xList.append(p1_x)
            yList.append(p1_y)

    # print(angleList)
    return angleList, xList, yList

def generateTranslationHistogram(flattenedLidar, angleHist, angleBinEdges):
    global pointConversion
    global rotation
    global xList
    global yList
    xList = []
    yList = []

    rotation = angleBinEdges[numpy.argmax(angleHist)]
    for obstacleIndex in range(len(flattenedLidar)-1): #-150 degree to 150 degree
        if flattenedLidar[obstacleIndex] < 60:
            angle1 = mapValue(obstacleIndex, 0, len(flattenedLidar), (-(5*math.pi)/6), ((5*math.pi)/6))
            angle2 = mapValue(obstacleIndex+1, 0, len(flattenedLidar), (-(5*math.pi)/6), ((5*math.pi)/6))
            p1_x, p1_y = pointConversion.PointToCartesianWrtRobotRotated(flattenedLidar[obstacleIndex], angle1, rotation)
            p2_x, p2_y = pointConversion.PointToCartesianWrtRobotRotated(flattenedLidar[obstacleIndex+1], angle2, rotation)
            xList.append(p1_x)
            yList.append(p1_y)

    # print(angleList)
    return xList, yList

def mapValue(value, lowerBefore, upperBefore, lowerAfter, upperAfter):
    newValue = 0
    newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
    newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

    return newValue

def callback_pointcloud(data):
    global LidarDataPoints
    global newLidarDataReceived
    if(newLidarDataReceived == False):
        LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        newLidarDataReceived = True
    # self.newLIDARDataReceived = True

def callback_robotPose(data):
    global newRobotPoseReceived
    global robotPoint
    global robotYaw
    global newLidarDataReceived

    if(newLidarDataReceived == True and newRobotPoseReceived == False):
        robotPose = data
        robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)
        newRobotPoseReceived = True

def shutDown():
    print("shutdown")

def main():
    global flattenedObstacles
    global LidarDataPoints
    global robotPoint
    global newLidarDataReceived
    global newRobotPoseReceived
    global robotYaw
    global robotPoint
    global histogram
    global conv
    global flattenNearestObstacles

    conv = math.pi / 180 #Convert degrees to radians

    rospy.init_node('SLAM_histogram_planner', anonymous=True)
    rate = rospy.Rate(5)

    # plt.show(block = False)
    plt.ion()

    rospy.Subscriber('/odom', Odometry, callback_robotPose)
    rospy.on_shutdown(shutDown)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, callback_pointcloud)

    # fixed bin size
    angBins = numpy.arange(-math.pi, math.pi, (2*math.pi)/360) # fixed bin size
    xyBins = numpy.arange(-60, 60, 0.5) # fixed bin size

    while not rospy.is_shutdown():

        if newLidarDataReceived == True and newRobotPoseReceived == True:

            newLidarDataReceived = False
            newRobotPoseReceived = False
            flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(LidarDataPoints)
            plt.figure(1)
            plt.cla()
            #plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5', linewidth= 0.1)


            # print(LidarDataPoints)
            # print(flattenedObstacles)
            angleHistList, xHistList, yHistList = generateHistogram(flattenedObstacles)
            angleHist, angleBinEdges = numpy.histogram(angleHistList, bins=angBins)
            # angleHist2 = numpy.append([angleHist],[0])
            # print(angleHist)
            # print("bin edges = ")
            # print(angleBinEdges)
            plt.bar(angleBinEdges[:-1],angleHist,width=numpy.diff(angleBinEdges), align='edge')
            plt.xlabel('Angle / rads', fontsize=16)
            plt.ylabel('Number of occurences', fontsize=16)
            plt.title('Angle histogram', fontsize = 20)

            fig = plt.gcf()
            fig.canvas.draw()
            # plt.hist(angleHistList,360)
            # plt.show(block=False)

            # # plt.hist(xHistList, bins=xyBins, alpha=0.5)
            # # plt.show(block=False)
            # # plt.hist(yHistList, bins=xyBins, alpha=0.5)
            # # plt.show(block=False)
            xHistListRot, yHistListRot = generateTranslationHistogram(flattenedObstacles, angleHist, angleBinEdges)
            xHist, xBinEdges = numpy.histogram(xHistListRot, bins=xyBins)
            plt.figure(2)
            plt.cla()
            plt.bar(xBinEdges[:-1],xHist,width=numpy.diff(xBinEdges), align='edge')
            # # plt.show(block=False)
            plt.xlabel('X axis distance', fontsize=16)
            plt.ylabel('Number of occurences', fontsize=16)
            plt.title('X translational histogram', fontsize = 20)
            plt.xlim(-15, 15)
            fig = plt.gcf()
            fig.canvas.draw()
            plt.figure(3)
            plt.cla()
            yHist, yBinEdges = numpy.histogram(yHistListRot, bins=xyBins)
            plt.bar(yBinEdges[:-1],yHist,width=numpy.diff(yBinEdges), align='edge')
            plt.xlabel('Y axis distance', fontsize=16)
            plt.ylabel('Number of occurences', fontsize=16)
            plt.title('Y translational histogram', fontsize = 20)
            plt.xlim(-15, 15)
            # # plt.show(block=False)
            fig = plt.gcf()
            fig.canvas.draw()

        rate.sleep()


if __name__ == '__main__':
    main()
