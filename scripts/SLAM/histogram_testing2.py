#! /usr/bin/env python

import rospy
import numpy
import math
import tf
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from matplotlib import pyplot as plt

from pointConversion import pointConversion
from local_path_planning.create_obstacle_map_from_sensors import flattenLidar2DMap

resolution = 0.05
width = 500
height = 500

robotX = 0
robotY = 0

robotRadius = 0.3

robotPoint = Point(0, 0, 0)
robotYaw = 0
LidarDataPoints = 0
numberAngleSegments = 300
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
pointConversion = pointConversion()
newLidarDataReceived = False
newRobotPoseReceived = False
flattenedObstacles = []
angleList = []
xList = []
yList = []

def setFreeCells(grid, robotPosition, robotRadius):
    global resolution
    off_x = resolution + width  // 2 # // means floor division
    off_y = resolution + height // 2

	# set the roi to 1: known free positions
    for i in range(-robotRadius, robotRadius):
        for j in range(-robotRadius, robotRadius):
            grid[int(i + off_x), int(j + off_y)] = 1

def setObstacleCells(grid, robotPosition, robotOrientation, flattenedObstacles):

    global pointConversion
    off_x = width  // 2 # // means floor division
    off_y = height // 2
    for obstacleIndex in range(len(flattenedObstacles)): #-150 degree to 150 degree
        if flattenedObstacles[obstacleIndex] < 60:
            angle = mapValue(obstacleIndex, 0, len(flattenedObstacles), (-(5*math.pi)/6), ((5*math.pi)/6))
            #print("obstacle dist from robot: " + str(flattenedObstacles[obstacleIndex]))
            #print("angle is " + str(angle))
            obstacle_x_from_origin, obstacle_y_from_origin  = pointConversion.PointToCartesianWrtOrigin(0, 0, robotOrientation, flattenedObstacles[obstacleIndex], angle)
            #print("obstacle x from origin: " + str(obstacle_x_from_origin) + " y: " + str(obstacle_y_from_origin))
            obstacle_grid_x = (obstacle_x_from_origin // resolution) + (width  // 2)
            obstacle_grid_y = (obstacle_y_from_origin // resolution) + (height // 2)
            #print("obstacle x from origin grid: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))
            obstacle = [obstacle_grid_x, obstacle_grid_y]
            #print("obstacel x: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))

            #print("robot x: " + str(off_x) + " y: " + str(off_y))

            grid[int(obstacle[0]), int(obstacle[1])] = int(100)
            if  grid[int(obstacle[0]+1), int(obstacle[1])]   < int(1):
        		grid[int(obstacle[0]+1), int(obstacle[1])]   = int(50)
            if  grid[int(obstacle[0]), 	 int(obstacle[1]+1)] < int(1):
        		grid[int(obstacle[0]),   int(obstacle[1]+1)] = int(50)
            if  grid[int(obstacle[0]-1), int(obstacle[1])]   < int(1):
        		grid[int(obstacle[0]-1), int(obstacle[1])]   = int(50)
            if  grid[int(obstacle[0]),   int(obstacle[1]-1)] < int(1):
        		grid[int(obstacle[0]),   int(obstacle[1]-1)] = int(50)


            change_x = (obstacle_grid_x-off_x)
            change_y = (obstacle_grid_y-off_y)
            grid_distance = int(math.floor(math.sqrt(math.pow(change_x, 2) + math.pow(change_y, 2))))
            #print("grid dist: " + str(grid_distance))
            for i in range(grid_distance):
                new_x = int(off_x + i*math.cos(angle - robotOrientation))
                new_y = int(off_y + i*math.sin(angle - robotOrientation))
                #print("new x: " + str(new_x) + " , new y: " + str(new_y))
                if  grid[new_x, new_y]   < int(1):
                    grid[new_x, new_y] = int(0)

def generateTranslationHistogram(grid):
    global xList
    global yList
    xList = []
    yList = []

    for gridXIndex in range(width): # size of grid
        yCounter = 0
        for gridYIndex in range(height): # size of grid
            if grid[gridXIndex, gridYIndex] == 100:
                yCounter = yCounter + 1

        xList.append(yCounter)
    for gridYIndex in range(height): # size of grid
        xCounter = 0
        for gridXIndex in range(width): # size of grid
            if grid[gridXIndex, gridYIndex] == 100:
                xCounter = xCounter + 1

        yList.append(xCounter)

    return xList, yList

def mapValue(value, lowerBefore, upperBefore, lowerAfter, upperAfter):
    newValue = 0
    newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
    newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

    return newValue

def callback_pointcloud(data):
    global LidarDataPoints
    global newLidarDataReceived
    if(newLidarDataReceived == False):
        LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        newLidarDataReceived = True

def callback_robotPose(data):
    global newRobotPoseReceived
    global robotPoint
    global robotYaw
    global newLidarDataReceived

    if(newLidarDataReceived == True and newRobotPoseReceived == False):
        robotPose = data
        robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)
        newRobotPoseReceived = True

def shutDown():
    print("shutdown")

def condenseList(list, reductionFactor):
    l = len(list)
    finalLength = l//reductionFactor
    output = [0] * finalLength
    for i in range(l):
        value = list[i]
        newIndex = i//reductionFactor
        currentvalue = output[newIndex]
        newValue = value + currentvalue
        output[newIndex] = newValue
    return output

def main():
    global flattenedObstacles
    global LidarDataPoints
    global robotPoint
    global newLidarDataReceived
    global newRobotPoseReceived
    global robotYaw
    global robotPoint
    global histogram
    global conv

    conv = math.pi / 180 #Convert degrees to radians

    rospy.init_node('SLAM_histogram_planner', anonymous=True)
    rate = rospy.Rate(5)

    # plt.show(block = False)
    plt.ion()

    grid = numpy.ndarray((width, height), buffer=numpy.zeros((width, height), dtype=numpy.int), dtype=numpy.int)
    grid.fill(int(-1))

    histBinSize = 0.1 # in metres

    rospy.Subscriber('/odom', Odometry, callback_robotPose)
    rospy.on_shutdown(shutDown)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, callback_pointcloud)

    while not rospy.is_shutdown():

        if newLidarDataReceived == True and newRobotPoseReceived == True:
            newLidarDataReceived = False
            newRobotPoseReceived = False
            flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(LidarDataPoints)
            # print(flattenedObstacles)
            setFreeCells(grid, robotPoint, int(robotRadius//resolution))
            setObstacleCells(grid, robotPoint, robotYaw, flattenedObstacles)
            xHistList, yHistList = generateTranslationHistogram(grid) #the heights of the histogram bars
            xHist = condenseList(xHistList, 5)
            plt.figure(1)
            plt.cla()
            x = numpy.arange(0, len(xHist), 1)
            plt.bar(x,xHist)
            fig = plt.gcf()
            fig.canvas.draw()
            yHist = condenseList(yHistList, 5)
            plt.figure(2)
            plt.cla()
            y = numpy.arange(0, len(yHist), 1)
            plt.bar(y, yHist)
            fig = plt.gcf()
            fig.canvas.draw()

        rate.sleep()
        grid.fill(int(-1))

if __name__ == '__main__':
    main()
