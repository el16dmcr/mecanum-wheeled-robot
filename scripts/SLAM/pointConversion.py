#! /usr/bin/env python

import rospy
from time import sleep
import math

class pointConversion():
    def __init__(self):
        #Initialise
        global conv
        conv = math.pi / 180 #Convert degrees to radians

     ###################################### END ############################################

    def PointToCartesianWrtOrigin(self, posX, posY, posThetaRad, range, thetaRad):
        #Convert scanned point in degrees to cartesian coordinates with respect to world
        # Deg to radians
        # print(posThetaDeg, thetaDeg)
        # posThetaRad = posThetaDeg*conv
        # thetaRad = thetaDeg*conv
        # print(posThetaRad, thetaRad)
        x = posX + (range*(math.cos(-1 * (posThetaRad-thetaRad))))
        y = -posY + (range*(math.sin(-1 * (posThetaRad-thetaRad))))

        return x, y

    def PointToCartesianWrtRobot(self, range, thetaRad):
        #Convert scanned point in degrees to cartesian coordinates with respect to robot
        # Deg to radians
        # thetaRad = thetaDeg*conv
        # print(thetaRad)
        x = (range*(math.cos(thetaRad)))
        y = (range*(math.sin(thetaRad)))

        return x, y

    def PointToCartesianWrtRobotRotated(self, range, thetaRad, rotation):
        #Convert scanned point in degrees to cartesian coordinates with respect to robot
        # Deg to radians
        # thetaRad = thetaDeg*conv
        # print(thetaRad)
        x = (range*(math.cos(thetaRad+rotation)))
        y = (range*(math.sin(thetaRad+rotation)))

        return x, y
