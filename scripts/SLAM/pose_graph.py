#! /usr/bin/env python

import math
import numpy as np
from geometry_msgs.msg import Point
import random
from scipy import sparse
from pointConversion import pointConversion



# Stores all of the pose data from each observation and sensor based observations between states.
# The has functions for composing all the poses into an occupancy grid by trying to minimise
# the global error between poses.
#
# Developed by David Russell March 2021
# Edited by David Russell March 2021

###################################### END ############################################
class poseGraph():
    def __init__(self, initialMapWidth, initialMapHeight, gridResolution, angleSegments):
        # Matrices we require for pose graph optimisation
        # - State vector - stores every sequential pose that was measured from odometry data, x, y, theta
        # - Infomation matrix - stores how confident we are in every prediciton update - for odometry and sensor edges in graph
        # -

        self.stateVector = np.empty((0,3))
        self.stateSensorData = np.empty((0,angleSegments))
        self.infomation_matrix = np.array([])

        self.mapWidth = initialMapWidth
        self.mapHeight = initialMapHeight
        self.mapResolution = gridResolution

        self.robotRadius = 0.3

        self.pointConversion = pointConversion()


    # Add the new pose to the growing pose map, along with accompanying sensor data.
    # Every new pose will have at least1  constraint, which is the odometry constraint from
    # previous pose to this one, but also possibly sensor constraints to previous poses.
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def updatePoseGraph(self, newPose, sensorData, sensorConstraints):
        self.addNewPose(newPose)
        self.addNewSensorData(sensorData)


        pass

    # Simple function to store the new pose into the state vector matrix
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def addNewPose(self, newPose):
        self.stateVector = np.append(self.stateVector, newPose, axis=0)

    # Simple function to store the new sensor data into the sensor data matrix for map construction
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def addNewSensorData(self, newSensorData):
        self.stateSensorData = np.append(self.stateSensorData, newSensorData, axis = 0)



    # Loop through all of the stored poses and associated flattened Lidar readings
    # and create an occupancy grid map from them.
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def calculateOccupancyGrid(self):
        occGrid = np.ndarray((self.mapWidth, self.mapHeight), buffer=np.zeros((self.mapWidth, self.mapHeight), dtype=np.int), dtype=np.int)
        occGrid.fill(int(-1))
        for state in range(len(self.stateVector)):
            occGrid = self.setFreeCells(occGrid, self.stateVector[state], int(self.robotRadius//self.mapResolution))
            occGrid = self.setObstacleCells(occGrid, self.stateVector[state], self.stateSensorData[state])

        return occGrid

    # For every state we want to mark the cells where the robot was as free as it is theoretically
    # impossible for the robot to be inside an obstacle.
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def setFreeCells(self, grid, robotPose, robotRadius):
        global resolution
        off_x = (robotPose[0] // self.mapResolution) + (self.mapWidth  // 2)
        off_y = (-robotPose[1] // self.mapResolution) + (self.mapHeight // 2)

    	# set the roi to 1: known free positions
        for i in range(-robotRadius, robotRadius):
            for j in range(-robotRadius, robotRadius):
                grid[int(i + off_x), int(j + off_y)] = 1

        return grid



    # Loop through all sensor data and mark all obstacles around the robot on the map, need to also
    # mark the space up to the obstacle as free space
    #
    # Developed by David Russell March 2021
    # Edited by David Russell March 2021

    ###################################### END ############################################
    def setObstacleCells(self, grid, robotPose, flattenedObstacles):

        # Find the position of the robot in the map frame
        off_x = (robotPose[0] // self.mapResolution) + (self.mapWidth  // 2)
        off_y = (-robotPose[1] // self.mapResolution) + (self.mapHeight // 2)

        # loop through every obstacle in flattened array
        for obstacleIndex in range(len(flattenedObstacles)): # -150 degree to 150 degree
            if flattenedObstacles[obstacleIndex] < 60:
                angle = self.mapValue(obstacleIndex, 0, len(flattenedObstacles), (-(5*math.pi)/6), ((5*math.pi)/6))
                #print("obstacle dist from robot: " + str(flattenedObstacles[obstacleIndex]))
                #print("angle is " + str(angle))
                obstacle_x_from_origin, obstacle_y_from_origin  = self.pointConversion.PointToCartesianWrtOrigin(robotPose[0], robotPose[1], robotPose[2], flattenedObstacles[obstacleIndex], angle)
                #print("obstacle x from origin: " + str(obstacle_x_from_origin) + " y: " + str(obstacle_y_from_origin))
                obstacle_grid_x = (obstacle_x_from_origin // self.mapResolution) + (self.mapWidth  // 2)
                obstacle_grid_y = (obstacle_y_from_origin // self.mapResolution) + (self.mapHeight // 2)
                #print("obstacle x from origin grid: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))
                obstacle = [obstacle_grid_x, obstacle_grid_y]
                #print("obstacel x: " + str(obstacle_grid_x) + " y: " + str(obstacle_grid_y))
                #print("robot x: " + str(off_x) + " y: " + str(off_y))

                grid[int(obstacle[0]), int(obstacle[1])] = int(100)
                # if  grid[int(obstacle[0]+1), int(obstacle[1])]   < int(1):
            	# 	grid[int(obstacle[0]+1), int(obstacle[1])]   = int(50)
                # if  grid[int(obstacle[0]), 	 int(obstacle[1]+1)] < int(1):
            	# 	grid[int(obstacle[0]),   int(obstacle[1]+1)] = int(50)
                # if  grid[int(obstacle[0]-1), int(obstacle[1])]   < int(1):
            	# 	grid[int(obstacle[0]-1), int(obstacle[1])]   = int(50)
                # if  grid[int(obstacle[0]),   int(obstacle[1]-1)] < int(1):
            	# 	grid[int(obstacle[0]),   int(obstacle[1]-1)] = int(50)
                #
                #
                change_x = (obstacle_grid_x-off_x)
                change_y = (obstacle_grid_y-off_y)
                grid_distance = int(math.floor(math.sqrt(math.pow(change_x, 2) + math.pow(change_y, 2))))
                for i in range(grid_distance):
                    new_x = int(off_x + i*math.cos(angle - robotPose[2]))
                    new_y = int(off_y + i*math.sin(angle - robotPose[2]))
                    if  grid[new_x, new_y]   < int(1):
                        grid[new_x, new_y] = int(0)


        return grid

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue
