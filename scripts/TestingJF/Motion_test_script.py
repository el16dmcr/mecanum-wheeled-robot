#! /usr/bin/env python

import rospy, math
from geometry_msgs.msg import Twist

from create_obstacle_map_from_sensors import flattenLidar2DMap

rospy.init_node('test_controller', anonymous=True)

xVelocity = 1
yVelocity = 0
zAngVelocity = 2

movementPublisher = rospy.Publisher('cmd_vel', Twist, queue_size=1)
msg = Twist()

def main():
    rospy.on_shutdown(shutdown)
    msg.linear.x = xVelocity
    msg.linear.y = yVelocity
    msg.angular.z = zAngVelocity
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        movementDecision
        msg.linear.x = msg.linear.x + 1
        if msg.linear.x > 3:
            msg.linear.x = -3
        msg.angular.z = msg.angular.z + 1
        if msg.angular.z > 3:
            msg.angular.z = -3
        movementPublisher.publish(msg)
        rate.sleep()

def movementDecision():
    lidarOffset = [0, 0.136]
    numberAngleSegments = 360
    flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, pointcloud)

def pointcloud():
    gen = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
    flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(gen)

def shutdown():
    msg.linear.x = 0
    msg.linear.y = 0
    msg.linear.z = 0
    msg.angular.x = 0
    msg.angular.y = 0
    msg.angular.z = 0
    movementPublisher.publish(msg)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
