#! /usr/bin/env python

import rospy
from time import sleep
import math
import matplotlib.pyplot as plt
import numpy as np
from geometry_msgs.msg import Point

# This class is designed to accept a 3D point cloud and flatten the point cloud with respect
# to the robot, any obstacles that are within the robots height are tracked and an array is
# returned with the closest obstacle at each angle segment in 360 degrees around the robot
#
# Developed by David Russell December 2020
# Edited by David Russell Decemeber 2020

###################################### END ############################################
class flattenLidar2DMap():
    def __init__(self, numberOfAngleSegments, heightOfLidar, heightOfRobot, lidarOffset):
        self.numberOfAngleSegments = numberOfAngleSegments
        self.heightOfLidar = heightOfLidar
        self.heightOfRobot = heightOfRobot
        self.lidarOffSet = lidarOffset
        self.nearestObstaclesFlattened = [100 for x in range(self.numberOfAngleSegments)]
        self.angleSegUpdated = [False for x in range(self.numberOfAngleSegments)]

        self.lowerAngleObstruct1 = 230
        self.higherAngleObstruct1 = 245
        self.lowerAngleObstruct2 = 298
        self.higherAngleObstruct2 = 315

        self.occlusionZone1 = np.array([Point(-0.2, -0.4, -0.12), 0.7, 0.9, 0.1])
        self.occlusionZone2 = np.array([Point(0.1, 0.3, -0.15), 0.2, 0.15, 0.07])


        self.xAxis = [0] * numberOfAngleSegments
        for angleSeg in range(self.numberOfAngleSegments):
            self.xAxis[angleSeg] = int(self.mapValue(angleSeg, 0, self.numberOfAngleSegments, -180, 180))


    # This function is the main function for this class, it takes the lidar point cloud and processes it
    # to return a flattened array of distances at each angle segment around the robot to the closest obstacle
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def return2DObstacleMap(self, lidarScan):
        # Initialise empty obstacle flattened array
        nearestObstaclePoint2DFlattened = [100 for x in range(self.numberOfAngleSegments)]
        self.angleSegUpdated = [False for x in range(self.numberOfAngleSegments)]
        # loop through all points in the point cloud
        for point in lidarScan:
            # if the point is something the robot could crash into
            angleOfPoint = self.calculateAngle(point[0], point[1])

            if(self.checkIfPointWithinObstacleHeight(point[2], self.heightOfRobot, self.heightOfLidar) == True ):
                if(self.checkIfPointInOcclusionZone(point, self.occlusionZone1, self.occlusionZone2) == False):
                    # calculate the angle segment index this point would lie in
                    point = self.changeFrameFromLidarToRobotCentre(point, self.lidarOffSet)

                    angleIndex = self.calculateAngleSegmentIndex(angleOfPoint, self.numberOfAngleSegments)
                    # calculate the horizontal distance to the point and then update angle segment if it is lower than the current value
                    # at that angle segment
                    horizontalDistanceToPoint = self.calculatePythagorasDistance(point[0], point[1])
                    nearestObstaclePoint2DFlattened[angleIndex] = self.returnLowest(nearestObstaclePoint2DFlattened[angleIndex], horizontalDistanceToPoint)

        maxValidSensorReading = self.findMaxValidSensorReading(nearestObstaclePoint2DFlattened)
        #nearestObstaclePoint2DFlattened = self.fillInBlanksForRobotObstruction(nearestObstaclePoint2DFlattened, maxValidSensorReading)
        # Eliminate any outliers in the flattened obstacle array
        nearestObstaclePoint2DFlattened = self.eliminateOutliers(nearestObstaclePoint2DFlattened, maxValidSensorReading)
        # Change the array fromat so that 0 is in fron of the robot rather than to the right and its range is -180 to 180 degrees
        nearestObstaclePoint2DFlattened = self.changeArrayAngleFormat(nearestObstaclePoint2DFlattened, self.numberOfAngleSegments)
        self.nearestObstaclesFlattened = nearestObstaclePoint2DFlattened

        return nearestObstaclePoint2DFlattened

    # This function is to purely plot the data provided by this class for debugging purposes
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def plotData(self):
        self.frameCounter = self.frameCounter + 1
        if self.frameCounter > 100000:
            self.frameCounter = 0
            plt.cla()
            plt.bar(self.xAxis, self.nearestObstaclesFlattened, width = 1.5)
            plt.xlabel('angle')
            plt.ylabel('Distance_to_nearest_object', fontsize=5)
            plt.title('Flattened graph of nearest obstacles')
            self.fig.canvas.draw()


    # Finds the max valid sensor reading throughout the obstacle array, only want to use
    # values that were updated and not default
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def findMaxValidSensorReading(self, obstacleData):
        maxVal = 0
        for angleSegment in range(len(obstacleData)):
            if self.angleSegUpdated[angleSegment] == True:
                if(obstacleData[angleSegment] > maxVal):
                    maxVal = obstacleData[angleSegment]
        return maxVal


    # We need to change the array format so instead of 0 to 360 degrees data format, it is instead -180 to 180
    # and 0 is directly in front of the robot, the reason behind this is we need all the different frames to have
    # the same perspective so the whole system math works.
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def changeArrayAngleFormat(self, zeroTo2PiArray, numberOfAngleSegments):
        minusPiToPiArray = [0] * numberOfAngleSegments
        tempArray = [0] * numberOfAngleSegments
        # reverse array
        for angleSeg in range(numberOfAngleSegments):
            tempArray[angleSeg] = zeroTo2PiArray[numberOfAngleSegments - angleSeg - 1]

        for angleSeg in range(numberOfAngleSegments):
            index = (angleSeg + (numberOfAngleSegments / 2)) % numberOfAngleSegments
            minusPiToPiArray[angleSeg] = tempArray[index]
        # to switch to correct angle array format, we need to flip direction and shift it by pi radians

        return minusPiToPiArray

    # All the points come from the LIDAR which is not in the centre of the robot, we need to adjust all
    # points so they are relative to the robot centre.
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def changeFrameFromLidarToRobotCentre(self, point, lidarOffset):
        newPoint = [0, 0, 0]
        newPoint[0] = point[0] + lidarOffset[0]
        newPoint[1] = point[1] + lidarOffset[1]
        #print("point was " + str(point))
        #print("new point is " + str(newPoint))
        return newPoint

    # Returns the angle from the robot to a certain point
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculateAngle(self, x, y):
        #return angle between 0 and 2 pi
        angle = math.atan2(y, x) + math.pi
        return angle

    # Returns the correct angle segment from around the robot depnding on the point passed in.
    # If point is 1,1 that is at 45 degrees to the robot, if there are 360 segments, it would be
    # segment 45
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculateAngleSegmentIndex(self, angle, numberOfSegments):
        angleSegment = (angle / (2 * math.pi)) * numberOfSegments
        angleSegment = int(angleSegment)
        self.angleSegUpdated[angleSegment] = True
        return angleSegment

    # Calculates the pythagorean distance to any point from the robot
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculatePythagorasDistance(self, x, y):
        distance = math.sqrt(math.pow(x,2) + math.pow(y,2))
        return distance

    # returns true or false depending on whether a certain point could be an obstacle that the
    # robot could crash with. Any points above the robots height or that are below the wheels.
    # A lot of points will be the lasers hitting the floor which are not obstacles!
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def checkIfPointWithinObstacleHeight(self, heightOfObstacle, heightOfRobot, heightOfLidar):
        pointWithinRobotHeight = False
        heightOfObstacle = heightOfObstacle + heightOfLidar

        if((heightOfObstacle < heightOfRobot) and heightOfObstacle > 0.01):
            pointWithinRobotHeight = True


        return pointWithinRobotHeight

    # Checks if a given point is in the occlusion zone foir the robot, that is an angle segment that
    # will have sensor infomation, however the lidar picks up the robot itself for some angle beams
    # which are useless measuremenbts and want to be occluded.
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def checkIfPointInOcclusionZone(self, point, occlusionZone1, occlusionZone2):
        pointInOcclusionZone = False
        #Check if in occlusion zone 1
        if point[0] > occlusionZone1[0].x and point[0] < occlusionZone1[0].x + occlusionZone1[1]:
            if point[1] > occlusionZone1[0].y and point[1] < occlusionZone1[0].y + occlusionZone1[2]:
                if point[2] > occlusionZone1[0].z and point[2] < occlusionZone1[0].z + occlusionZone1[3]:
                    pointInOcclusionZone = True


        # # check if in occlusion zone 2
        # if point[0] > occlusionZone2[0].x and point[0] < occlusionZone2[0].x + occlusionZone2[1]:
        #     if point[1] > occlusionZone2[0].y and point[1] < occlusionZone2[0].y + occlusionZone2[2]:
        #         if point[2] > occlusionZone2[0].z and point[2] < occlusionZone2[0].z + occlusionZone2[3]:
        #             pointInOcclusionZone = True


        return pointInOcclusionZone

    # Returns the lower of two distances, primarily used to make sure the closes obstacle
    # in an angle segment is registered
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def returnLowest(self, distance1, distance2):
        if(distance1 < distance2):
            return distance1
        else:
            return distance2

    # The lidar data is not perfect so sometimes certain angle segments dont get an update value,
    # even though likely it wouldnt go from an obstacle in one segment and then max range in the next
    # this replaces any unuodated values with the value in the previosu angle segment
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def eliminateOutliers(self, obstacleAngleArray, maxVal):
        for angleSeg in range(len(obstacleAngleArray)):
            if(obstacleAngleArray[angleSeg] > maxVal and self.angleSegUpdated[angleSeg] == True):
                maxVal = obstacleAngleArray[angleSeg]

        for angleSeg in range(len(obstacleAngleArray)):
            if(self.angleSegUpdated[angleSeg] == False):
                if(angleSeg == 0):
                    obstacleAngleArray[angleSeg] = maxVal
                else:
                    obstacleAngleArray[angleSeg] = obstacleAngleArray[angleSeg - 1]

        return obstacleAngleArray

    # Solution to filling in the gaps left by the zones where the lidar picks up the robot
    # look at the values on either end of the obsrtruction zone and fill in the gap using
    # y = mx + c
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def fillInBlanksForRobotObstruction(self, obstacleAngleArray, maxObstacleDist):
        lower1Index = int(self.mapValue(self.lowerAngleObstruct1, 0, 360, 0, self.numberOfAngleSegments))
        robotObstruct1Range = int(self.mapValue(self.higherAngleObstruct1  - self.lowerAngleObstruct1, 0, 360, 0, self.numberOfAngleSegments))
        y1 =  obstacleAngleArray[lower1Index - 1]
        y2 =  obstacleAngleArray[lower1Index + robotObstruct1Range - 1]
        m = (y2 - y1) / robotObstruct1Range
        for i in range(robotObstruct1Range):
            obstacleAngleArray[lower1Index + i] = y1 + (m * i)


        lower2Index = int(self.mapValue(self.lowerAngleObstruct2, 0, 360, 0, self.numberOfAngleSegments))
        robotObstruct2Range = int(self.mapValue(self.higherAngleObstruct2  - self.lowerAngleObstruct2, 0, 360, 0, self.numberOfAngleSegments))
        y1 =  obstacleAngleArray[lower2Index - 1]
        y2 =  obstacleAngleArray[lower2Index + robotObstruct2Range - 1]
        m = (y2 - y1) / robotObstruct2Range
        for i in range(robotObstruct2Range):
            obstacleAngleArray[lower2Index + i] = y1 + (m * i)

        return obstacleAngleArray

    # maps a value from one range to another, useful for mapping from degrees to radians and
    # back again
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue
