#!/usr/bin/env python

import rospy  # rospy library
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import String
from tf import transformations
import math
import time
from Queue import PriorityQueue, Queue
from nav_msgs.msg import OccupancyGrid
from skimage.morphology import disk, dilation
from skimage.morphology import square

import matplotlib.pyplot as plt
import matplotlib as mpl

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap =[]
dilatedGmap = []
Position = Point()
NewPath = []

computePath = False
globalPathActive = False
askForNewFrontierPoint = True
frontierPointNonReachable = False
frontierPoint = Point(0, 0, 0)
globalPathPoints = []


def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position
    if(computePath == False):

        # get position of robot from odometry message
        Position = msg.pose.pose.position

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap, computePath

    # get size of map occupancy grid in rows and columns
    if(computePath == False):
        map_columns = msg.info.width
        map_rows = msg.info.height

        # get map resolution
        map_res = msg.info.resolution

        # get coordinates of gmap cell[0,0]
        map_origin = msg.info.origin.position

        # get occupancy grid map
        gmap = msg.data

        gmap = np.array(gmap)

        gmap = gmap.reshape(map_rows, map_columns)

def clbk_newFrontierPoint(data):
    global frontierPoint
    global computePath
    global askForNewFrontierPoint
    global frontierPointNonReachable
    if(computePath == False):
        if((askForNewFrontierPoint == True) or (frontierPointNonReachable == True)):
            #print("after first if statement")
            if(data != frontierPoint):
                frontierPoint = data
                computePath = True
                askForNewFrontierPoint = False
                frontierPointNonReachable = False
                print("new frontier point is: " + str(frontierPoint))

def clbk_goalReached(data):
    global askForNewFrontierPoint
    global globalPathActive

    if(globalPathActive == True):
        askForNewFrontierPoint = True
        globalPathActive = False
        print("received new goal request - GPP")



class Node():

     #Initialse function
     def __init__(self, row, col, total_rows):
       self.row = row
       self.col = col
       self.neighbors = []
       self.total_rows = total_rows

    #Get position (indexing is row and column of node)
     def get_pos(self):
       return self.row, self.col


     #Function updates nodes that can be used for path/i.e. that are not boundaries
     def update_neighbors(self):

      self.neighbors = []

        #OBSTACLE AVOIDANCE
        #Checks if node below current node is a barrier, if not add to neighbour list
      if self.row < self.total_rows - 1 and not is_barrier((self.row + 1, self.col)): # DOWN
         self.neighbors.append((self.row + 1, self.col))


        #Checks if node above current node is a barrier, if not add to neighbour list
      if self.row > 0 and not is_barrier((self.row - 1, self.col)):
         self.neighbors.append((self.row - 1, self.col))


        #Checks if node to the right of current node is a barrier, if not add to neighbour list
      if self.col < self.total_rows - 1 and not is_barrier((self.row, self.col + 1)):
         self.neighbors.append((self.row, self.col + 1))

        #Checks if node to the left of current node is a barrier, if not add to neighbour list
      if self.col > 0 and not is_barrier([self.row, self.col - 1]):
         self.neighbors.append((self.row, self.col - 1))


      return self.neighbors


     def __lt__(self, other):
       return False

#Heuristic Function using Manhattan distance
def h(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return abs(x1 - x2) + abs(y1 - y2)

#Draw shortest path by using the came from array
def reconstruct_path(came_from, current):

    global NewPath

    while current in came_from:
          current = came_from[current]
          NewPath.append(current)


def dilate_obstacles(gmap):

    global map_rows, map_columns, map_res

    obstacles = []
    neighbors = []
    for row in range(map_rows):
     for node in range(map_columns):
         if gmap[row][node] > 1:
            gmap[row][node] = 1
            obstacles.append((row,node))

    # chhange back to 0.5 for highly congested map once kenny fixed start illegal error
    Robot_Radius = (0.5/map_res)
    squaresInflation = int(Robot_Radius*2)
    mask = disk(squaresInflation)
    #gmap = dilation(gmap, selem=mask)
    dilatedGmap = dilation(gmap, square(squaresInflation))
    return dilatedGmap

def is_barrier(cell):
    global dilatedGmap
    row = cell[0]
    col = cell[1]

    if (dilatedGmap[row][col] > 0) or (dilatedGmap[row][col] < 0):
        return True
    else:
        return False



#"Make" changes the state of a given node
def get_start():
    global Position, map_origin, map_res
    global dilatedGmap
    #print("robot position is: " + str(Position))

    #finds index of gmap cell in centre of robot
    start_row = int(round((Position.y - map_origin.y) / map_res - 1))
    start_col = int(round((Position.x - map_origin.x) / map_res - 1))

    if dilatedGmap[start_row][start_col] > 0 or dilatedGmap[start_row][start_col] < 0:
        print("starting cell not valid")
        return find_nearest_reachable_cell(start_row,start_col)
    else:
        return start_row, start_col

def find_nearest_reachable_cell(row,col):
    global dilatedGmap, map_origin, map_columns, map_rows, map_res

    checked = [[False for i in range(map_columns)] for i in range(map_rows)]

    #convert coordinates to row and column indices

    #set start point for search
    start_p = [row, col]
    queue_e = Queue()  # create explore queue
    queue_e.put(start_p)  # enqueue start point to explore queue

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    #initialise reachable cell
    reachable_cell = []

    #beign search
    while queue_e.empty() == False:
        p = queue_e.get() #get next point in explore queue

        #get row and column indices from p
        r = p[0]
        c = p[1]

        #if cell is open space then see if it is surrounded by open space neighbours
        if dilatedGmap[r][c] == 0:
            # count open space neighbours
            number_open_neighbours = count_open_space_neighbours(p)

            # if cell is surrounded by open space neighbours then return cell
            if number_open_neighbours == 8:
                reachable_cell = p
                break


        #mark p as checked
        checked[r][c] = True

        # cycle through p's adjacent cells, and add them to explore queue as long as they're valid and unchecked
        for i in range(len(drow)):
            adj_r = r + drow[i]
            adj_c = c + dcol[i]
            adj_cell = [adj_r, adj_c]


            if checked[adj_r][adj_c] == False:
                queue_e.put(adj_cell)

    return reachable_cell[0], reachable_cell[1]

def count_open_space_neighbours(cell):
    global dilatedGmap
    #returns number of open space neighbours a cell has

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0, -1, -1, 1, 1]
    dcol = [0, 1, 0, -1, -1, 1, 1, -1]

    # cycle through adjacent cells, checking for open cells
    open_space_neighbours = 0
    for i in range(len(drow)):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if dilatedGmap[adj_r][adj_c] == 0:
            open_space_neighbours += 1

    return open_space_neighbours


def get_end(endPoint):
    global map_origin, map_res
    #print("desired end point is: " + str(endPoint))
    #print("map origin is: " + str(map_origin))
    #print("map resolution: " + str(map_res))

    end_row = int(round((endPoint.y - map_origin.y) / map_res - 1))
    end_col = int(round((endPoint.x - map_origin.x)/ map_res - 1))

    #return 380, 380
    return end_row, end_col

def convertIndicesToGlobalCoordinates(Indicies):
    global map_origin, map_res
    globalCoordPoint = Point(0, 0, 0)
    globalCoordPoint.x = ((Indicies[1] + 1) * map_res) + map_origin.x
    globalCoordPoint.y = ((Indicies[0] + 1) * map_res) + map_origin.y

    return globalCoordPoint

#A* Algorithm function
#Initialse the variables used in the algorithm
def algorithm(grid, start, end):
    global map_rows, map_columns

    count = 0
    node1 = Node(start[0],start[1],map_rows)
    node2 = Node(end[0],end[1],map_rows)
    open_set = PriorityQueue()
    open_set.put((0, count, start))
    came_from = {}
    g_score = {}
    for row in range(map_rows):
        for node in range(map_columns):
            g_score[(row, node)] = float("inf")
    #print(len(g_score))
    g_score[start] = 0
    f_score = {}
    for row in range(map_rows):
        for node in range(map_columns):
            f_score[(row, node)] = float("inf")
    f_score[start] = h(node1.get_pos(), node2.get_pos())

    open_set_hash = {start}

    while not open_set.empty():
        #While the open set is not empty get the node with the lowest f score and remove it from open set
        current = open_set.get()[2]
        open_set_hash.remove(current)

        node3 = Node(current[0],current[1],map_rows)

        #If the current node is the end node, then the algorithm is completed so the path is made

        #Find the temporary g-score of all the neighbours of the current node
        for neighbor in node3.update_neighbors():
            temp_g_score = g_score[current] + 1

            #If the temporary g score is less than the current g score of the neighbour, the curent g score is replaced
            #with the temporary g score and the f score is updated.
            if temp_g_score < g_score[neighbor]:
                came_from[neighbor] = current
                node4 = Node(neighbor[0],neighbor[1],map_rows)
                g_score[neighbor] = temp_g_score
                f_score[neighbor] = temp_g_score + h(node4.get_pos(), node2.get_pos())

                #If the neighbour is not in the open set, the count is incremented by 1 and the neighbour is added to the open set.
                if neighbor not in open_set_hash:
                    count += 1
                    #print(count)
                    open_set.put((f_score[neighbor], count, neighbor))
                    open_set_hash.add(neighbor)
                    #print(node3.update_neighbors())

        if not current == end:
            continue
        else:
            reconstruct_path(came_from, end)
            print("current=end")
            print(count)
            return True

#Main function
def main():

    global gmap, askForNewFrontierPoint, computePath, globalPathPoints, globalPathActive
    global frontierPoint, map_rows, NewPath, frontierPointNonReachable
    global dilatedGmap

    rospy.init_node('Global_path_planner')
    print("Global path planning script running")

    odom_subscriber = rospy.Subscriber('/odom', Odometry, clbk_odom)
    map_subscriber = rospy.Subscriber('/map', OccupancyGrid, clbk_map)

    nextPointPublisher = rospy.Publisher('globalPath', numpy_msg(Point), queue_size=1)
    requestNewDestinationPublisher = rospy.Publisher('frontierPointStatus', String, queue_size=1)

    rospy.Subscriber('goalStatus', String, clbk_goalReached)
    rospy.Subscriber('nextFrontierPoint', Point, clbk_newFrontierPoint)


	#Create the A star path publisher
	##AstarPathPub = rospy.Publisher("/local_planner_points", Int32MultiArray, queue_size = 10)

    start = None
    end = None
    List_of_points = None

    rate = rospy.Rate(100)

    while not rospy.is_shutdown():
        if(computePath == True):
            print("path being computed")
            NewPath = []
            dilatedGmap = dilate_obstacles(gmap)
            start = get_start()

            #If there is no end node then make next left clicked node end node
            end = get_end(frontierPoint)

            node = Node(start[0], start[1], map_rows)

            node.update_neighbors()
            try:
                algorithm(dilatedGmap, start, end)
                print("algorithm finished")
                globalPathPoints = NewPath
                publishedPath = np.array([])

                ################### Plotting code for global path ######################################
                #'plot received gmap as a colour map
                plt.figure(1)

                        # create gmap with frontier points highlighted
                for point in NewPath:
                    r = point[0]
                    c = point[1]
                    gmap[r][c] = 150

                        # make colour map of fixed colours: dark grey, light grey, black, red
                colour_map = mpl.colors.ListedColormap(['#808080', '#C5C9C7', 'black', 'red'])
                        # specify boundary values for each colour
                bounds = [-1.5, -0.5, 0.5, 101, 200]
                        # set how to scale values to be coloured
                norm = mpl.colors.BoundaryNorm(bounds, colour_map.N)

                        # create plot
                        #dilatedGmap
                plt.imshow(gmap, interpolation='nearest', cmap=colour_map, norm=norm)
                plt.gca().invert_yaxis()
                plt.suptitle("4 Adjacent Cell Sampling A* Path")

                plt.show()

                for i in range(len(NewPath)):
                    # convert from indices to global coordinates
                    globalPathPoints[i] = convertIndicesToGlobalCoordinates(NewPath[i])
                    #print(globalPathPoints[0::4])

                globalPathPoints = globalPathPoints[0::6]

                ## global path points is coordinates x y in metres for the real world map.
                # Change tis variable so there are less to publish
                # globalPathPoints is a list of points
                if(len(globalPathPoints) > 0):
                    for i in range(len(globalPathPoints) - 1):
                        nextPointPub = globalPathPoints[len(globalPathPoints) - 1]
                        globalPathPoints.pop()
                        nextPointPublisher.publish(nextPointPub)
                    globalPathActive = True
                else:
                    frontierPointNonReachable = True
                    requestNewDestinationPublisher.publish('point_non_reachable')

            except:
                print("error occured")
                frontierPointNonReachable = True
                requestNewDestinationPublisher.publish('point_non_reachable')

            computePath = False

        if(askForNewFrontierPoint == True):
            requestNewDestinationPublisher.publish('new_point_requested')


        rate.sleep()


if __name__ == '__main__':
    main()
