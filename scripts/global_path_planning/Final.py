#!/usr/bin/env python

import rospy  # rospy library
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from tf import transformations
import math
import time
from Queue import PriorityQueue
from nav_msgs.msg import OccupancyGrid

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap =[]
Position = Point()
NewPath = []


def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position

    # get position of robot from odometry message
    Position = msg.pose.pose.position

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap

    # get size of map occupancy grid in rows and columns
    map_columns = msg.info.width
    map_rows = msg.info.height

    # get map resolution
    map_res = msg.info.resolution

    # get coordinates of gmap cell[0,0]
    map_origin = msg.info.origin.position

    # get occupancy grid map
    gmap = msg.data

    gmap = np.array(gmap)

    gmap = gmap.reshape(map_rows, map_columns)




class Node():

     #Initialse function
     def __init__(self, row, col, total_rows):
       self.row = row
       self.col = col
       self.neighbors = []
       self.total_rows = total_rows

    #Get position (indexing is row and column of node)
     def get_pos(self):
       return self.row, self.col


     #Function updates nodes that can be used for path/i.e. that are not boundaries
     def update_neighbors(self, grid):

       self.neighbors = []


       #OBSTACLE AVOIDANCE
       #Checks if node below current node is a barrier, if not add to neighbour list
       if self.row < map_rows - 1 and not grid[self.row + 1][self.col].is_barrier(): # DOWN
         self.neighbors.append(grid[self.row + 1][self.col])

       #Checks if node above current node is a barrier, if not add to neighbour list
       if self.row > 0 and not grid[self.row - 1][self.col].is_barrier():
         self.neighbors.append(grid[self.row - 1][self.col])

       #Checks if node to the right of current node is a barrier, if not add to neighbour list
       if self.col < map_rows - 1 and not grid[self.row][self.col + 1].is_barrier():
         self.neighbors.append(grid[self.row][self.col + 1])

       #Checks if node to the left of current node is a barrier, if not add to neighbour list
       if self.col > 0 and not grid[self.row][self.col - 1].is_barrier():
         self.neighbors.append(grid[self.row][self.col - 1])

     def __lt__(self, other):
       return False

#Heuristic Function using Manhattan distance
def h(p1, p2):
 x1, y1 = p1
 x2, y2 = p2
 return abs(x1 - x2) + abs(y1 - y2)


#Draw shortest path by using the came from array
def reconstruct_path(came_from, current):

    global NewPath

    while current in came_from:
          current = came_from[current]
          NewPath.append([current])




def is_barrier(row,col):
    global gmap

    if gmap[row][col] > 0:
        return True
    else:
        return False



#"Make" changes the state of a given node
def get_start():
    global Position, map_origin, map_res


    #finds index of gmap cell in centre of robot
    start_row = int(round((Position.y - map_origin.y) / map_res - 1))
    start_col = int(round((Position.x - map_origin.x)/map_res - 1))

    return start_row, start_col


def get_end():


    end_row = 400
    end_col = 400

    return end_row, end_col
#A* Algorithm function

#Initialse the variables used in the algorithm
def algorithm(draw, grid, start, end):
 count = 0
 open_set = PriorityQueue()
 open_set.put((0, count, start))
 came_from = {}
 g_score = {node: float("inf") for row in grid for node in row}
 g_score[start] = 0
 f_score = {node: float("inf") for row in grid for node in row}
 f_score[start] = h(start.get_pos(), end.get_pos())

 open_set_hash = {start}


 while not open_set.empty():


   #While the open set is not empty get the node with the lowest f score and remove it from open set
   current = open_set.get()[2]
   open_set_hash.remove(current)

   #If the current node is the end node, then the algorithm is completed so the path is made
   if current == end:
     reconstruct_path(came_from, end)

     return True

   #Find the temporary g-score of all the neighbours of the current node
   for neighbor in current.neighbors:
     temp_g_score = g_score[current] + 1

     #If the temporary g score is less than the current g score of the neighbour, the curent g score is replaced
     #with the temporary g score and the f score is updated.
     if temp_g_score < g_score[neighbor]:
       came_from[neighbor] = current
       g_score[neighbor] = temp_g_score
       f_score[neighbor] = temp_g_score + h(neighbor.get_pos(), end.get_pos())

       #If the neighbour is not in the open set, the count is incremented by 1 and the neighbour is added to the open set.
       if neighbor not in open_set_hash:
         count += 1
         open_set.put((f_score[neighbor], count, neighbor))
         open_set_hash.add(neighbor)


 return False


#Main function
def main():

    global gmap, NewPath


    rospy.init_node('Final')
    print("Global path planning script running")

    odom_subscriber = rospy.Subscriber('/odom', Odometry, clbk_odom)
    map_subscriber =  rospy.Subscriber('/map', OccupancyGrid, clbk_map)


	#Create the A star path publisher
	##AstarPathPub = rospy.Publisher("/local_planner_points", Int32MultiArray, queue_size = 10)

    start = None
    end = None
    List_of_points = None


    rate = rospy.Rate(0.4)

    while not rospy.is_shutdown():
        rate.sleep()

        #If there is no start node then make next left clicked node start node

        start = get_start()

        #If there is no end node then make next left clicked node end node
        end = get_end()

        print(start)

        print(end)

        print(gmap)



        for row in gmap:
            for node in row:
                node.update_neighbors(gmap)


        #Run Algorithm
        algorithm(gmap, start, end)

if __name__ == '__main__':
    main()
