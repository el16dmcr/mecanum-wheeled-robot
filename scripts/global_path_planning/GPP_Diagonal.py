#!/usr/bin/env python

import rospy  # rospy library
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from tf import transformations
import math
import time
from Queue import PriorityQueue
from nav_msgs.msg import OccupancyGrid
import matplotlib.pyplot as plt
import matplotlib as mpl

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap = []
Position = Point()
NewPath = []


def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position

    # get position of robot from odometry message
    Position = msg.pose.pose.position

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap

    # get size of map occupancy grid in rows and columns
    map_columns = msg.info.width
    map_rows = msg.info.height

    # get map resolution
    map_res = msg.info.resolution

    # get coordinates of gmap cell[0,0]
    map_origin = msg.info.origin.position

    # get occupancy grid map
    gmap = msg.data

    gmap = np.array(gmap)

    gmap = gmap.reshape(map_rows, map_columns)




class Node():

     #Initialse function
     def __init__(self, row, col, total_rows):
       self.row = row
       self.col = col
       self.neighbors = []
       self.total_rows = total_rows

    #Get position (indexing is row and column of node)
     def get_pos(self):
       return self.row, self.col


     #Function updates nodes that can be used for path/i.e. that are not boundaries
     def update_neighbors(self):

      self.neighbors = []

        #OBSTACLE AVOIDANCE
        #Checks if node below current node is a barrier, if not add to neighbour list
      if self.row < self.total_rows - 1 and not is_barrier((self.row + 1, self.col)): # DOWN
         self.neighbors.append((self.row + 1, self.col))

      if self.row < self.total_rows - 1 and not is_barrier((self.row + 1, self.col - 1)): # DOWN DIAGONAL LEFT
         self.neighbors.append((self.row + 1, self.col - 1))

      if self.row < self.total_rows - 1 and not is_barrier((self.row + 1, self.col + 1)): # DOWN DIAGONAL RIGHT
         self.neighbors.append((self.row + 1, self.col + 1))

        #Checks if node above current node is a barrier, if not add to neighbour list
      if self.row > 0 and not is_barrier((self.row - 1, self.col)):
         self.neighbors.append((self.row - 1, self.col))

        #Checks if node above current node is a barrier, if not add to neighbour list UP DIAGONAL LEFT
      if self.row > 0 and not is_barrier((self.row - 1, self.col - 1)):
        	self.neighbors.append((self.row - 1, self.col - 1))

        #Checks if node above current node is a barrier, if not add to neighbour list UP DIAGONAL RIGHT
      if self.row > 0 and not is_barrier((self.row - 1, self.col + 1)):
        	self.neighbors.append((self.row - 1, self.col + 1))

        #Checks if node to the right of current node is a barrier, if not add to neighbour list
      if self.col < self.total_rows - 1 and not is_barrier((self.row, self.col + 1)):
         self.neighbors.append((self.row, self.col + 1))

        #Checks if node to the left of current node is a barrier, if not add to neighbour list
      if self.col > 0 and not is_barrier([self.row, self.col - 1]):
         self.neighbors.append((self.row, self.col - 1))


         return self.neighbors


     def __lt__(self, other):
       return False

#Heuristic Function using Manhattan distance
def h(p1, p2):
 x1, y1 = p1
 x2, y2 = p2
 return abs(x1 - x2) + abs(y1 - y2)


#Draw shortest path by using the came from array
def reconstruct_path(came_from, current):

    global NewPath

    while current in came_from:
          current = came_from[current]
          NewPath.append(current)



def is_barrier(cell):
    global gmap

    row = cell[0]
    col = cell[1]

    if gmap[row][col] > 0:
        return True
    else:
        return False



#"Make" changes the state of a given node
def get_start():
    global Position, map_origin, map_res


    #finds index of gmap cell in centre of robot
    start_row = int(round((Position.y - map_origin.y) / map_res - 1))
    start_col = int(round((Position.x - map_origin.x)/map_res - 1))

    return start_row, start_col


def get_end():

    end_row = 450
    end_col = 450

    return end_row, end_col
#A* Algorithm function

#Initialse the variables used in the algorithm
def algorithm(grid, start, end):
    global map_rows, map_columns

    count = 0
    node1 = Node(start[0],start[1],map_rows)
    node2 = Node(end[0],end[1],map_rows)
    open_set = PriorityQueue()
    open_set.put((0, count, start))
    came_from = {}
    g_score = {}
    for row in range(map_rows):
     for node in range(map_columns):
         g_score[(row, node)] = float("inf")
    print(len(g_score))
    g_score[start] = 0
    f_score = {}
    for row in range(map_rows):
     for node in range(map_columns):
         f_score[(row, node)] = float("inf")
    f_score[start] = h(node1.get_pos(), node2.get_pos())

    open_set_hash = {start}




    while not open_set.empty():

    #While the open set is not empty get the node with the lowest f score and remove it from open set
        current = open_set.get()[2]
        open_set_hash.remove(current)

        node3 = Node(current[0],current[1],map_rows)


    #If the current node is the end node, then the algorithm is completed so the path is made

        #Find the temporary g-score of all the neighbours of the current node
        for neighbor in node3.update_neighbors():
         temp_g_score = g_score[current] + 1



         #If the temporary g score is less than the current g score of the neighbour, the curent g score is replaced
         #with the temporary g score and the f score is updated.
         if temp_g_score < g_score[neighbor]:
           came_from[neighbor] = current
           node4 = Node(neighbor[0],neighbor[1],map_rows)
           g_score[neighbor] = temp_g_score
           f_score[neighbor] = temp_g_score + h(node4.get_pos(), node2.get_pos())

           #If the neighbour is not in the open set, the count is incremented by 1 and the neighbour is added to the open set.
           if neighbor not in open_set_hash:
             count += 1
             print(count)
             open_set.put((f_score[neighbor], count, neighbor))
             open_set_hash.add(neighbor)
             print(node3.update_neighbors())

         if not current == end:
          continue
         else:
          reconstruct_path(came_from, end)
          print("current=end")
          return True



#Main function
def main():

    global gmap, map_rows, NewPath


    rospy.init_node('Final', anonymous=True)
    print("Global path planning script running")

    odom_subscriber = rospy.Subscriber('/odom', Odometry, clbk_odom)
    map_subscriber =  rospy.Subscriber('/map', OccupancyGrid, clbk_map)


	#Create the A star path publisher
	##AstarPathPub = rospy.Publisher("/local_planner_points", Int32MultiArray, queue_size = 10)

    start = None
    end = None
    List_of_points = None



    rate = rospy.Rate(0.4)

    while not rospy.is_shutdown():
        rate.sleep()

        #If there is no start node then make next left clicked node start node

        start = get_start()




        #If there is no end node then make next left clicked node end node
        end = get_end()




        node = Node(start[0], start[1], map_rows)

        node.update_neighbors()

        print(start)
        print(end)


        #Run Algorithm
        algorithm(gmap, start, end)

        print(NewPath)


        for point in NewPath:
            r = point[0]
            c = point[1]
            gmap[r][c] = 150

        # make colour map of fixed colours: dark grey, light grey, black, red
        colour_map = mpl.colors.ListedColormap(['#808080', '#C5C9C7', 'black', 'red'])
        # specify boundary values for each colour
        bounds = [-1.5, -0.5, 0.5, 101, 200]
        # set how to scale values to be coloured
        norm = mpl.colors.BoundaryNorm(bounds, colour_map.N)

        # create plot
        plt.imshow(gmap, interpolation='nearest', cmap=colour_map, norm=norm)
        plt.gca().invert_yaxis()
        plt.suptitle("Gmap - A start path highlighted")

        plt.show()



if __name__ == '__main__':
    main()
