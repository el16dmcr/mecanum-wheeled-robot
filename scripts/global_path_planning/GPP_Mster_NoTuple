#!/usr/bin/env python

import rospy  # rospy library
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from tf import transformations
import math
import time
from Queue import PriorityQueue
from nav_msgs.msg import OccupancyGrid

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap =[]
Position = Point()
neighbors = []
NewPath = []


def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position

    # get position of robot from odometry message
    Position = msg.pose.pose.position

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap

    # get size of map occupancy grid in rows and columns
    map_columns = msg.info.width
    map_rows = msg.info.height

    # get map resolution
    map_res = msg.info.resolution

    # get coordinates of gmap cell[0,0]
    map_origin = msg.info.origin.position

    # get occupancy grid map
    gmap = msg.data

    gmap = np.array(gmap)

    gmap = gmap.reshape(map_rows, map_columns)



#Get position (indexing is row and column of node)
def get_pos(cell):

        row = cell[0]
        col = cell[1]

	return row,col

#If node is a barrier = BLACK
def is_barrier(cell):
    global gmap

    row = cell[0]
    col = cell[1]

    if gmap[row][col] > 0:
        return True
    else:
        return False


#If node is start node = ORANGE
def is_start(cell):
    global Position, map_origin, map_res

    row = cell[0]
    col = cell[1]

    return row == int(round((Position.y - map_origin.y) / map_res - 1))
    return col == int(round((Position.x - map_origin.x)/map_res - 1))

#If node is end node = TURQUOISE
def is_end(cell):

    row = cell[0]
    col = cell[1]

    return row == Frontier.x
    return col == Frontier.y


#"Make" changes the state of a given node
def get_start():
    global Position, map_origin, map_res


    #finds index of gmap cell in centre of robot
    start_row = int(round((Position.y - map_origin.y) / map_res - 1))
    start_col = int(round((Position.x - map_origin.x)/map_res - 1))

    return [start_row, start_col]


def get_end():


    end_row = 400
    end_col = 400

    return [end_row, end_col]


	#Function updates nodes that can be used for path/i.e. that are not boundaries
def update_neighbors(row, col):

    global neighbors, map_rows, map_columns


    neighbors = []


    #OBSTACLE AVOIDANCE
    #Checks if node below current node is a barrier, if not add to neighbour list
    if row < map_rows - 1 and not is_barrier([row + 1, col]): # DOWN
        neighbors.append([row + 1, col])
    #Checks if node above current node is a barrier, if not add to neighbour list
    if row > 0 and not is_barrier([row - 1, col]):
        neighbors.append([row - 1, col])

        #Checks if node to the right of current node is a barrier, if not add to neighbour list
    if col < map_columns - 1 and not is_barrier([row, col + 1]):
        neighbors.append([row, col + 1])

            #Checks if node to the left of current node is a barrier, if not add to neighbour list
    if col > 0 and not is_barrier([row, col - 1]):
        neighbors.append([row, col - 1])

def current_neighbors(cell):

    row = cell[0]
    col = cell[1]

    current = update_neighbors(row,col)

    return current



#Heuristic Function using Manhattan distance
def h(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return abs(x1 - x2) + abs(y1 - y2)

#Draw shortest path by using the came from array
def reconstruct_path(came_from, current):

    global NewPath

    while current in came_from:
        current = came_from[current]
        NewPath.append([current])

#A* Algorithm function

#Initialse the variables used in the algorithm
def algorithm(start, end):

    global gmap, neighbors

    r1 = 0
    c1 = 0
    n1 = 0
    n2 = 0
    r = start[0]
    c = start[1]

    current = None
    count = 0
    open_set = PriorityQueue()
    open_set.put((0, count, start))
    came_from = {}
    g_score = [[float("inf") for element in row] for row in gmap]
    g_score[r][c] = 0
    f_score = [[float("inf") for element in row] for row in gmap]
    f_score[r][c] = h(get_pos(start), get_pos(end))

    open_set_hash = [start]


    while not open_set.empty():

        #While the open set is not empty get the node with the lowest f score and remove it from open set
        current = open_set.get()[2]
        open_set_hash.remove(current)

        r1 = current[0]
        c1 = current[1]
        #If the current node is the end node, then the algorithm is completed so the path is made
        if current == end:
            reconstruct_path(came_from, end)
            return True



            #Find the temporary g-score of all the neighbours of the current node
        for neighbor in neighbors:
            temp_g_score = g_score[r1][c1] + 1

            n1 = neighbor[0]
            n2 = neighbor[1]

            neighbor1 = tuple(neighbor)

			#If the temporary g score is less than the current g score of the neighbour, the curent g score is replaced
            #with the temporary g score and the f score is updated.
            if temp_g_score < g_score[n1][n2]:
                came_from[neighbor1] = current
                g_score[n1][n2] = temp_g_score
                f_score[n1][n2] = temp_g_score + h(get_pos(neighbor), get_pos(neighbor))

                #If the neighbour is not in the open set, the count is incremented by 1 and the neighbour is added to the open set.
                if neighbor not in open_set_hash:
                    count += 1
                    open_set.put((f_score[n1][n2], count, neighbor))
                    open_set_hash.append(neighbor)

	return False

def indices_to_coordinates(cell_list):
    global map_res, map_origin
    #converts cell indices to x,y coordinates for a list of cells
    #returns list of coordinates

    coordinate_list = [] #initialise coordinate list

    for cell in cell_list:
        r = cell[0]
        c = cell[1]
        y = (r + 1)*map_res + map_origin.y
        x = (c + 1)*map_res + map_origin.x
        coordinate_list.append([x,y])

    return coordinate_list

#Main function
def main():

    global NewPath, gmap


    rospy.init_node('GPP_Mster_NoTuple')
    print("Global path planning script running")

    odom_subscriber = rospy.Subscriber('/odom', Odometry, clbk_odom)
    map_subscriber =  rospy.Subscriber('/map', OccupancyGrid, clbk_map)


	#Create the A star path publisher
	##AstarPathPub = rospy.Publisher("/local_planner_points", Int32MultiArray, queue_size = 10)

    start = None
    end = None
    start_r = None
    start_c = None
    List_of_points = None

    run = True
    rate = rospy.Rate(0.4)

    while not rospy.is_shutdown():
        rate.sleep()

        #If there is no start node then make next left clicked node start node

        start = get_start()

        #If there is no end node then make next left clicked node end node
        end = get_end()

        start_r = start[0]
        start_c = start[1]


        update_neighbors(start_r, start_c)

        print(start)

        print(end)

        print(gmap)


        #Run Algorithm
        algorithm(start, end)

        List_of_ponts = indices_to_coordinates(NewPath)

        print(List_of_points)

if __name__ == '__main__':
    main()
