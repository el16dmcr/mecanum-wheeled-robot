 #!/usr/bin/env python

import pygame
import math
from Queue import PriorityQueue

WIDTH = 800
WIN = pygame.display.set_mode((WIDTH,WIDTH))

pygame.display.set_caption("A* Path Finding Algorithm")

#Definitions of colours
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 255, 0)
YELLOW = (255, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
PURPLE = (128, 0, 128)
ORANGE = (255, 165 ,0)
GREY = (128, 128, 128)
TURQUOISE = (64, 224, 208)

class Node:

	#Initialse function
	def __init__(self, row, col, width, total_rows):
		self.row = row
		self.col = col
		self.x = row * width
		self.y = col * width
		self.color = WHITE
		self.neighbors = []
		self.width = width
		self.total_rows = total_rows

#Get position (indexing is row and column of node)
	def get_pos(self):
		return self.row, self.col

#If node has been checked/part of closed set = RED
	def is_closed(self):
		return self.color == RED

#If node part of open set = GREEN
	def is_open(self):
		return self.color == GREEN

#If node is a barrier = BLACK
	def is_barrier(self):
		return self.color == BLACK

#If node is start node = ORANGE
	def is_start(self):
		return self.color == ORANGE

#If node is end node = TURQUOISE
	def is_end(self):
		return self.color == TURQUOISE

#If nodes are reset = WHITE
	def reset(self):
		self.color = WHITE

#"Make" changes the state of a given node
	def make_start(self):
		self.color = ORANGE

	def make_closed(self):
		self.color = WHITE

	def make_open(self):
		self.color = GREEN

	def make_barrier(self):
		self.color = BLACK

	def make_end(self):
		self.color = TURQUOISE

	def make_path(self):
		self.color = PURPLE

#Draw the nodes in pygame
	def draw(self, win):
		pygame.draw.rect(win, self.color, (self.x, self.y, self.width, self.width))

	#Function updates nodes that can be used for path/i.e. that are not boundaries
	def update_neighbors(self, grid):
		self.neighbors = []


		#OBSTACLE AVOIDANCE
		#Checks if node below current node is a barrier, if not add to neighbour list
		if self.row < self.total_rows - 1 and not grid[self.row + 1][self.col].is_barrier(): # DOWN
			self.neighbors.append(grid[self.row + 1][self.col])

		#Checks if node above current node is a barrier, if not add to neighbour list
		if self.row > 0 and not grid[self.row - 1][self.col].is_barrier():
			self.neighbors.append(grid[self.row - 1][self.col])

		#Checks if node to the right of current node is a barrier, if not add to neighbour list
		if self.col < self.total_rows - 1 and not grid[self.row][self.col + 1].is_barrier():
			self.neighbors.append(grid[self.row][self.col + 1])

		#Checks if node to the left of current node is a barrier, if not add to neighbour list
		if self.col > 0 and not grid[self.row][self.col - 1].is_barrier():
			self.neighbors.append(grid[self.row][self.col - 1])


	def __lt__(self, other):
		return False

#Heuristic Function using Manhattan distance
def h(p1, p2):
	x1, y1 = p1
	x2, y2 = p2
	return abs(x1 - x2) + abs(y1 - y2)

#Draw shortest path by using the came from array
def reconstruct_path(came_from, current, draw):
	while current in came_from:
		current = came_from[current]
		current.make_path()
		draw()

#A* Algorithm function

#Initialse the variables used in the algorithm
def algorithm(draw, grid, start, end):
	count = 0
	open_set = PriorityQueue()
	open_set.put((0, count, start))
	came_from = {}
	g_score = {node: float("inf") for row in grid for node in row}
	g_score[start] = 0
	f_score = {node: float("inf") for row in grid for node in row}
	f_score[start] = h(start.get_pos(), end.get_pos())

	open_set_hash = {start}


	while not open_set.empty():
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()

		#While the open set is not empty get the node with the lowest f score and remove it from open set
		current = open_set.get()[2]
		open_set_hash.remove(current)


		#If the current node is the end node, then the algorithm is completed so the path is made
		if current == end:
			reconstruct_path(came_from, end, draw)
			end.make_end()
			return True

		#Find the temporary g-score of all the neighbours of the current node
		for neighbor in current.neighbors:
			temp_g_score = g_score[current] + 1

			#If the temporary g score is less than the current g score of the neighbour, the curent g score is replaced
			#with the temporary g score and the f score is updated.
			if temp_g_score < g_score[neighbor]:
				came_from[neighbor] = current
				g_score[neighbor] = temp_g_score
				f_score[neighbor] = temp_g_score + h(neighbor.get_pos(), end.get_pos())

				#If the neighbour is not in the open set, the count is incremented by 1 and the neighbour is added to the open set.
				if neighbor not in open_set_hash:
					count += 1
					open_set.put((f_score[neighbor], count, neighbor))
					open_set_hash.add(neighbor)
					neighbor.make_open()

		draw()

		#If the current node is not the start node it is made a closed node, i.e. it has already been considered
		if current != start:
			current.make_closed()

	return False

#Creates the grid of nodes
def make_grid(rows, width):
	grid = []
	gap = width // rows
	for i in range(rows):
		grid.append([])
		for j in range(rows):
			node = Node(i, j, gap, rows)
			grid[i].append(node)

	return grid


#Draws grid lines Grey
def draw_grid_lines(win, rows, width):
	gap = width // rows
	for i in range(rows):
		pygame.draw.line(win, GREY, (0, i * gap), (width, i * gap))
		for j in range(rows):
			pygame.draw.line(win, GREY, (j * gap, 0), (j * gap, width))


#Draw Function draws Each node cube
def draw(win, grid, rows, width):
	#Makes all nodes white every 60 frames
	win.fill(WHITE)

	#Draws every individual node in for loop
	for row in grid:
		for node in row:
			node.draw(win)

	#Draws grid lines on top of nodes so they are visible
	draw_grid_lines(win, rows, width)
	pygame.display.update()


#Function Translates mouse position to node on screen
def get_clicked_pos(pos, rows, width):
	gap = width // rows
	y, x = pos

	row = y // gap
	col = x // gap

	return row, col

#Main function
def main(win, width):
	ROWS = 50
	grid = make_grid(ROWS, width)

	start = None
	end = None

	run = True
	while run:
		draw(win, grid, ROWS, width)
		for event in pygame.event.get():
			#If exit button pressed exit pygame
			if event.type == pygame.QUIT:
				run = False

			#If there is no start node then make next left clicked node start node
			if pygame.mouse.get_pressed()[0]:
				pos = pygame.mouse.get_pos()
				row, col = get_clicked_pos(pos, ROWS, width)
				node = grid[row][col]
				if not start and node != end:
					start = node
					start.make_start()

				#If there is no end node then make next left clicked node end node
				elif not end and node != start:
					end = node
					end.make_end()

				#If node clicked is not start or end node make node a barrier
				elif node != end and node != start:
					node.make_barrier()

			#Right clicking mouse removes node
			elif pygame.mouse.get_pressed()[2]:
				pos = pygame.mouse.get_pos()
				row, col = get_clicked_pos(pos, ROWS, width)
				node = grid[row][col]
				node.reset()
				if node == start:
					start = None
				elif node == end:
					end = None

			#If Space Key pressed, update neighbours and run A* algorithm
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_SPACE and start and end:
					for row in grid:
						for node in row:
							node.update_neighbors(grid)

					algorithm(lambda: draw(win, grid, ROWS, width), grid, start, end)

				#Clears screen if lowercase c button pressed
				if event.key == pygame.K_c:
					start = None
					end = None
					grid = make_grid(ROWS, width)

	pygame.quit()

main(WIN, WIDTH)
