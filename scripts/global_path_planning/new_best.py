#!/usr/bin/env python

import rospy  # rospy library
import numpy as np
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from tf import transformations
import math
import time
from Queue import PriorityQueue
from nav_msgs.msg import OccupancyGrid

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap =[]
Position = Point()



def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position

    # get position of robot from odometry message
    Position = msg.pose.pose.position

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap

    # get size of map occupancy grid in rows and columns
    map_columns = msg.info.width
    map_rows = msg.info.height

    # get map resolution
    map_res = msg.info.resolution

    # get coordinates of gmap cell[0,0]
    map_origin = msg.info.origin.position

    # get occupancy grid map
    gmap = msg.data

    gmap = np.array(gmap)

    gmap = gmap.reshape(map_rows, map_columns)

#"Make" changes the state of a given node
def get_start():
    global Position, map_origin, map_res


    #finds index of gmap cell in centre of robot
    start_row = int(round((Position.y - map_origin.y) / map_res - 1))
    start_col = int(round((Position.x - map_origin.x)/map_res - 1))

    return [start_row, start_col]


def get_end():


    end_row = 400
    end_col = 400

    return [end_row, end_col]

def indices_to_coordinates(cell_list):
    global map_res, map_origin
    #converts cell indices to x,y coordinates for a list of cells
    #returns list of coordinates

    coordinate_list = [] #initialise coordinate list

    for cell in cell_list:
        r = cell[0]
        c = cell[1]
        y = (r + 1)*map_res + map_origin.y
        x = (c + 1)*map_res + map_origin.x
        coordinate_list.append([x,y])

    return coordinate_list


class Node():
    """A node class for A* Pathfinding"""

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position


def astar(grid, start, end):
    """Returns a list of tuples as a path from the given start to the given end in the given grid"""

    # Create start and end node
    start_node = Node(None, start)
    start_node.g = start_node.h = start_node.f = 0
    end_node = Node(None, end)
    end_node.g = end_node.h = end_node.f = 0

    # Initialize both open and closed list
    open_list = []
    closed_list = []

    # Add the start node
    open_list.append(start_node)

    # Loop until you find the end
    while len(open_list) > 0:

        # Get the current node
        current_node = open_list[0]
        current_index = 0
        for index, item in enumerate(open_list):
            if item.f < current_node.f:
                current_node = item
                current_index = index

        # Pop current off open list, add to closed list
        open_list.pop(current_index)
        closed_list.append(current_node)

        # Found the goal
        if current_node == end_node:
            path = []
            current = current_node
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1] # Return reversed path

        # Generate children
        children = []
        for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]: # Adjacent squares

            # Get node position
            node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

            # Make sure within range
            if node_position[0] > (len(grid) - 1) or node_position[0] < 0 or node_position[1] > (len(grid[len(grid)-1]) -1) or node_position[1] < 0:
                continue

            # Make sure walkable terrain
            if grid[node_position[0]][node_position[1]] != -1:
                continue


            # Create new node
            new_node = Node(current_node, node_position)

            # Append
            children.append(new_node)

        # Loop through children
        for child in children:

            # Child is on the closed list
            for closed_child in closed_list:
                if child == closed_child:
                    continue

            # Create the f, g, and h values
            child.g = current_node.g + 1
            child.h = ((child.position[0] - end_node.position[0]) ** 2) + ((child.position[1] - end_node.position[1]) ** 2)
            child.f = child.g + child.h

            # Child is already in the open list
            for open_node in open_list:
                if child == open_node and child.g > open_node.g:
                    continue

            # Add the child to the open list
            open_list.append(child)


#Main function
def main():

    global gmap


    rospy.init_node('new_bes')
    print("Global path planning script running")

    odom_subscriber = rospy.Subscriber('/odom', Odometry, clbk_odom)
    map_subscriber =  rospy.Subscriber('/map', OccupancyGrid, clbk_map)

    print(gmap)

	#Create the A star path publisher
	##AstarPathPub = rospy.Publisher("/local_planner_points", Int32MultiArray, queue_size = 10)

    run = True
    rate = rospy.Rate(0.4)

    while not rospy.is_shutdown():
        rate.sleep()

        #If there is no start node then make next left clicked node start node

        start = get_start()

        #If there is no end node then make next left clicked node end node
        end = get_end()

        start_1 = tuple(start)
        end_1 = tuple(end)

        path = astar(gmap, start_1, end_1)

        print(path)


if __name__ == '__main__':
    main()
