#! /usr/bin/env python
import rospy
from time import sleep
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
import math

class moveRobot():
    def __init__(self):
        self.movementPublisher = rospy.Publisher('cmd_vel', Twist, queue_size=1)
        self.msg = Twist()
        self.robotVelX = 0
        self.robotVelY = 0
        self.robotw = 0

        self.robotHeading = 0

        self.ANGLE_DIFF_MIN_ALIGNED = 0.1
        self.ROTATION_DAMPING_FACTOR = 0.8
        self.ROTATION_MAX_ACCEL = 0.4
        self.MAX_ROBOT_ANGULAR_VEL = 2.0

        self.stopRobot()

    def moveAndDontCrash(self):
        self.movementPublisher.publish(self.msg)


    def calcRobotVelocities(self, robotVelocity, goalHeadingRobotFrame, robotAngularVel):
        robotXVelocity = robotVelocity * math.cos(goalHeadingRobotFrame)
        robotYVelocity = -robotVelocity * math.sin(goalHeadingRobotFrame)
        robotAngularVelocity = robotAngularVel
        self.msg.linear.x = robotXVelocity
        self.msg.linear.y = robotYVelocity
        self.msg.angular.z = robotAngularVelocity
        self.movementPublisher.publish(self.msg)

    # def calcRobotVelocities(self, robotAccMagnitude, goalHeadingRobotFrame):
    #     robotXAccel = robotAccMagnitude * math.cos(goalHeadingRobotFrame)
    #     robotYAccel = -robotAccMagnitude * math.sin(goalHeadingRobotFrame)
    #     robotAngularAccel = 0
    #
    #     self.robotVelX = self.robotVelX + robotXAccel
    #     self.robotVelY = self.robotVelY + robotYAccel
    #     self.robotw = self.robotw + robotAngularAccel
    #
    #     self.normaliseVelocities(1)
    #
    #     if self.robotw > 1:
    #         self.robotw = 1
    #     elif self.robotw < -1:
    #         self.robotw = -1
    #
    #     self.msg.linear.x = self.robotVelX
    #     self.msg.linear.y = self.robotVelY
    #     self.msg.angular.z = self.robotw
    #
    #     self.movementPublisher.publish(self.msg)


    def normaliseVelocities(self, maxVel):

        currentTotalVel = math.sqrt(math.pow(self.robotVelX, 2) + math.pow(self.robotVelY, 2))
        normalisingFactor = currentTotalVel / maxVel
        if currentTotalVel > maxVel:
            self.robotVelX = self.robotVelX / normalisingFactor
            self.robotVelY = self.robotVelY / normalisingFactor

    def alignRobotToPoint(self, theta_robotToGoal, robotOrientation):
        robotAligned = False
        self.msg.linear.x = 0
        self.msg.linear.y = 0

        angleDiff = robotOrientation - theta_robotToGoal

        # Keeps angle diff between -pi and pi
        if(angleDiff < -math.pi):
            angleDiff = angleDiff + (2*math.pi)
        if(angleDiff > math.pi):
            angleDiff = angleDiff - (2*math.pi)

        if abs(angleDiff) < self.ANGLE_DIFF_MIN_ALIGNED:
            self.robotw = 0
            robotAligned = True
        else:
            accel = self.mapValue(angleDiff, -math.pi, math.pi, self.ROTATION_MAX_ACCEL, -self.ROTATION_MAX_ACCEL)
            # Apply acceleration
            self.robotw = self.robotw + accel
            # Apply damping
            self.robotw = self.ROTATION_DAMPING_FACTOR * self.robotw

        if self.robotw > self.MAX_ROBOT_ANGULAR_VEL:
            self.robotw = self.MAX_ROBOT_ANGULAR_VEL
        elif self.robotw < -self.MAX_ROBOT_ANGULAR_VEL:
            self.robotw = -self.MAX_ROBOT_ANGULAR_VEL

        self.msg.angular.z = self.robotw
        self.movementPublisher.publish(self.msg)

        return robotAligned


    def moveRobotForwards(self):
        self.msg.linear.x = 1
        self.movementPublisher.publish(self.msg)


    def stopRobot(self):
        self.msg.angular.x = 0
        self.msg.angular.y = 0
        self.msg.angular.z = 0
        self.msg.linear.x = 0
        self.msg.linear.y = 0
        self.msg.linear.z = 0
        self.movementPublisher.publish(self.msg)

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter


        return newValue
