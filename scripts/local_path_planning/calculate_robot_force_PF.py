#! /usr/bin/env python

import rospy
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from sensor_msgs.msg import LaserScan
from time import sleep
import math
import matplotlib.pyplot as plt
from array import *
import numpy as np
from matplotlib.animation import FuncAnimation


# This class takes the flattened obstacle array from the robots perpective along with a goal
# heading and returns an optimal robot heading to travel along which should enable the robot
# to navigate to a point without crashing into any obstacles.
#
# Developed by David Russell December 2020
# Edited by David Russell Decemeber 2020

###################################### END ############################################
class obstclDetGaussPotentialFields():
    def __init__(self, upperDistThreshold, inflationDistance, lidarRangeMax, numberOfAngleSegments):
        self.obstacleUpperDistThreshold = upperDistThreshold
        self.inflationDistance = inflationDistance
        self.rangeLidarMax = lidarRangeMax
        self.numberOfAngleSegments = numberOfAngleSegments

        self.repulsiveField = [0] * numberOfAngleSegments
        self.attractiveField = [0] * numberOfAngleSegments
        self.totalField = [0] * numberOfAngleSegments

        self.attractMax = 5

        # plotting total field code
        self.xAxis = [0] * numberOfAngleSegments
        for angleSeg in range(self.numberOfAngleSegments):
            self.xAxis[angleSeg] = int(self.mapValue(angleSeg, 0, self.numberOfAngleSegments, -180, 180))

        self.frameCounter = 0
        self.tempNumberOfObstacles = 0

    # This is the main function for this class, it will return the optimal heading angle
    # for the robot to travel in when given the heading to the goal point and the flattened
    # obstacle array
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calcHeadingAngle(self, goalHeading, flattenedObstacles):

        headingAngle = 0
        repulsiveFieldsPerObstacle = []
        repulsiveFields = []
        attractiveFields = []
        totalFields = []
        theta_goal = goalHeading
        nearestObstacleDist = 0

        relevantObstacleData = self.returnRelevantObstaclesData(self.obstacleUpperDistThreshold, flattenedObstacles, self.numberOfAngleSegments)

        # in this data array we have d_k - distance to each obstacle, theta_k - angle of each obstacle and thi_k - angle occupied by the obstacle
        relevantObstacleData = self.inflateRelevantObstaclesWithRobotWidth(self.inflationDistance, relevantObstacleData)
        nearestObstacleDist = self.findClosestObstacleDist(relevantObstacleData, self.obstacleUpperDistThreshold)

        maxDistToObject = self.returnMaxDistToObjectReading(relevantObstacleData)
        #calculate repulsive gaussian field for each obstacle for each discrete angle (0 to 359 degrees)
        repulsiveFieldsPerObstacle = self.calculateRepulsiveGaussianFieldForRelevantObstacle(relevantObstacleData, self.numberOfAngleSegments, self.obstacleUpperDistThreshold, self.attractMax)

        # Sum gaussian repulsive fields for each obstacle and represent for each discrete angle ( 0 to 359 degrees)
        self.repulsiveField = self.sumGaussianPotentialFields(repulsiveFieldsPerObstacle, self.numberOfAngleSegments)
        #self.repulsiveField = self.maxGaussianRepulsiveField(repulsiveFieldsPerObstacle, self.numberOfAngleSegments)

        # calculate attractive field for each discrete angle (0 to 359 degrees)
        self.attractiveField = self.calculateAttractiveGaussianPotentialFields(theta_goal, self.attractMax, self.numberOfAngleSegments)

        # calculate total field from repuslive field and attractive field for each discrete angle (0 to 359 degrees)
        self.totalField = self.calculateTotalGaussianPotentialField(self.numberOfAngleSegments, self.attractiveField, self.repulsiveField)

        # find minimum potnetial field and retun that angle as the heading angle
        headingAngle = self.returnOptimumAngleHeading(self.numberOfAngleSegments, self.totalField)

        tempDegreeAngle = self.mapValue(headingAngle, -math.pi, math.pi, -180, 180)
        #print("optimal heading is: " + str(tempDegreeAngle))
        return headingAngle, nearestObstacleDist

    # Plots the data of the attractive, repulsive and total field around the robot
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def plotData(self):
        self.frameCounter = self.frameCounter + 1
        if self.frameCounter > 100000:
            self.frameCounter = 0

            plt.cla()
            plt.plot(self.xAxis, self.repulsiveField, label = 'repulsive')
            plt.plot(self.xAxis, self.attractiveField, label = 'attractive')
            plt.plot(self.xAxis, self.totalField, label = 'total')
            for i in range(self.tempNumberOfObstacles):
                plt.plot(self.xAxis, self.tempGaussRepFields[i], label = 'gaussrep' + str(i))
            self.fig.canvas.draw()

    # Plots the raw flattened obstacle data
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def plot2dobstacleData(self, obstacles, xaxis):
        #index = np.arange(len(obstacles))
        plt.bar(xaxis, obstacles)
        plt.xlabel('angle', fontsize=5)
        plt.ylabel('Distance_to_nearest_object', fontsize=5)
        #plt.xticks(index, label, fontsize=5, rotation=30)
        plt.title('Flattened graph of nearest obstacles')
        plt.show()

    # This functions needs to lops through the flattened obstacle array and locate any potential
    # obstacles as individual obstacles will span several angle segments, it will identify obstacles
    # close enough for the robot to crash into and mark their relative position (angle occur at - theta_k)
    # (angle width - thi_k) and (average distance to obstacle - d_k)
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def returnRelevantObstaclesData(self, distThreshold, flattenedObstacles, numberOfAngleSegments):
        # return an 2D array, number of obstacles - per obstacle return d_k, theta_k and thi_k
        relevantObstaclesData = []
        obstalceStartandStopIndexes = self.determineLowerAndUpperBoundOfObstacledIndexes(distThreshold, flattenedObstacles)
        self.tempNumberOfObstacles = len(obstalceStartandStopIndexes)
        # loop the the 360 degree angle segments
        for obstacle in range(len(obstalceStartandStopIndexes)):
            startingAngle = self.mapValue(obstalceStartandStopIndexes[obstacle][0], 0, numberOfAngleSegments, -math.pi, math.pi)
            indexWidth = obstalceStartandStopIndexes[obstacle][1] - obstalceStartandStopIndexes[obstacle][0]
            distSum = 0

            # for angleSegment in range(obstalceStartandStopIndexes[obstacle][0], obstalceStartandStopIndexes[obstacle][1]):
            #     distSum = distSum + flattenedObstacles[angleSegment]

            #midIndex = int((obstalceStartandStopIndexes[obstacle][1] + obstalceStartandStopIndexes[obstacle][0]) / 2)
            #d_k = distSum / indexWidth
            #d_k = flattenedObstacles[midIndex]
            minDist = distThreshold
            minDistIndex = 0
            for angleSegment in range(obstalceStartandStopIndexes[obstacle][0], obstalceStartandStopIndexes[obstacle][1]):
                if(flattenedObstacles[angleSegment] < minDist):
                    minDist = flattenedObstacles[angleSegment]
                    minDistIndex = angleSegment
            d_k = minDist
            # thi is angle width of the obstacle
            thi_k = (indexWidth / float(numberOfAngleSegments)) * 2 * math.pi
            # theta is specific mid point of the angle
            theta_k = startingAngle + self.mapValue(indexWidth, 0, numberOfAngleSegments, 0, math.pi)
            #theta_k = self.mapValue(minDistIndex, 0, numberOfAngleSegments, -math.pi, math.pi)
            #theta_k =  startingAngle + ((indexWidth / float(numberOfAngleSegments)) * math.pi)
            relevantObstaclesData.append([d_k, theta_k, thi_k, obstalceStartandStopIndexes[obstacle][2]])
            #print("obstacle: " + str(obstacle) + " start angle: " + str(startingAngle) + " half angle: " + str(self.mapValue(indexWidth, 0, 360, 0, 2 * math.pi)) + " thi: " + str(thi_k) + " index width: " + str(indexWidth) + " start inde: " + str(obstalceStartandStopIndexes[obstacle][0]))
            #print("obstacle: " + str(obstacle) + " d: " + str(relevantObstaclesData[obstacle][0]) + " theta: " + str(relevantObstaclesData[obstacle][1]) + " thi: " + str(relevantObstaclesData[obstacle][2]))
        #print(relevantObstaclesData)
        return relevantObstaclesData


    # Finds the closest obstacle distance, will be used to claculate magnitude of acceleration vector
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def findClosestObstacleDist(self, relevantObstacleData, maxSensorReading):
        closestDist = maxSensorReading
        for obstacle in range(len(relevantObstacleData)):
            closestDist = self.returnSmallest(closestDist, relevantObstacleData[obstacle][0])

        return closestDist

    # loops through the flattened obstacle array and determines the start and stop indexes of various
    # obstacles
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def determineLowerAndUpperBoundOfObstacledIndexes(self, distThreshold, flattenedObstacles):
        indexes = []
        obstacleFound = False
        startIndex = 0
        endIndex = 0
        closeObstacle = False
        sameObstacleDistMax = 0.5
        lastIndexBiGJump = False
        for angleSegment in range(len(flattenedObstacles)):
            # If we have not yet found an obstacle i.e the distances havent dropped below the threshold yet
            if obstacleFound == False:
                if flattenedObstacles[angleSegment] < distThreshold:
                    obstacleFound = True
                    # save the start index of this obstacle to this angle segment
                    startIndex = angleSegment

            else:
                # gods rewrite this
                change1 = 0
                change2 = 0
                change3 = 0
                change4 = 0
                bigChange1 = 0
                bigChange2 = 0
                if(angleSegment != 0):
                    distThisIndex = flattenedObstacles[angleSegment]
                    distLastIndex = flattenedObstacles[angleSegment - 1]
                    change2 = distThisIndex - distLastIndex
                    if(angleSegment != 1 and angleSegment != len(flattenedObstacles) - 1 and angleSegment != len(flattenedObstacles) - 2):
                        distTwoIndexesAgo = flattenedObstacles[angleSegment - 2]
                        distNextIndex = flattenedObstacles[angleSegment + 1]
                        distTwoIndexesNext = flattenedObstacles[angleSegment + 2]
                        change1 = distLastIndex - distTwoIndexesAgo
                        change3 = distNextIndex - distThisIndex
                        change4 = distTwoIndexesNext - distNextIndex
                        bigChange1 = change1+ change2
                        bigChange2 = change3 = change4


                #print("dist: " + str(flattenedObstacles[angleSegment]) + " at index: " +str(angleSegment))
                if flattenedObstacles[angleSegment] > distThreshold:
                    obstacleFound = False
                    # save the end index of this obstacle
                    endIndex = angleSegment
                    closeObstacle = True
                # if big jump from value to value
                else:
                    if( abs(change2) > sameObstacleDistMax):
                        #print("too big jump, index: " + str(angleSegment))
                        endIndex = angleSegment
                        closeObstacle = True
                        lastIndexBiGJump = True
                    elif( (bigChange1 * bigChange2) < -0.001 and lastIndexBiGJump == False):
                        #print("obstacle end found here, change1: " + str(change1) + " change2: " + str(change2) + " index: " + str(angleSegment))
                        endIndex = angleSegment
                        closeObstacle = True
                    else:
                        lastIndexBiGJump = False

                if closeObstacle == True:
                    closeObstacle = False
                    if endIndex - startIndex > 4:
                        indexes.append([startIndex,endIndex, False])

                    if obstacleFound == True:
                        startIndex = angleSegment + 1




        # if last obstacle wasnt closed, maunally input its indexes
        if( obstacleFound == True ):
            indexes.append([startIndex,len(flattenedObstacles) - 1, True])
            #check last object middl iindex height vs first object
            lastObjectLastHeight = flattenedObstacles[indexes[len(indexes) - 1][1]]
            firstObjectFirstHeight = flattenedObstacles[indexes[0][0]]
            heightDiff = abs(lastObjectLastHeight - firstObjectFirstHeight)
            if heightDiff < 0.1:
                # extend first object backwards
                gapAtEnd = indexes[len(indexes) - 1][1] - indexes[len(indexes) - 1][0]
                indexes[0][0] = -gapAtEnd

                # delete last object
                indexes.pop()

        return indexes

    # Inflate the relevant obstacles by a margin relative to the robots width as a safety margin
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def inflateRelevantObstaclesWithRobotWidth(self, inflationDistance, rawRelevantObstalces):
        inflatedObstacles = rawRelevantObstalces
        for obstacle in range(len(inflatedObstacles)):
            # enlarge this by robot width
            #if(rawRelevantObstalces[obstacle][3] == True):
            numerator = (inflationDistance / 2) + (inflatedObstacles[obstacle][0] * math.tan(inflatedObstacles[obstacle][2]/2))
                #print("obstacle: " + str(obstacle) + " numerator: " + str(numerator))
            inflatedObstacles[obstacle][2] = 2 * math.atan2(numerator, inflatedObstacles[obstacle][0])

        return inflatedObstacles


    # Finds the max distant to an obstacle from the found obstacles
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def returnMaxDistToObjectReading(self, relevantObstacleData):
        maxDist = 0
        for obstacle in range(len(relevantObstacleData)):
            if relevantObstacleData[obstacle][0] > maxDist:
                maxDist = relevantObstacleData[obstacle][0]
        return maxDist

    # Calculates the gaussian repulsive field for each obstacle over the 360 angle view range of the robot
    # important to have strength of field inversely proportional to distance to the obstacle. Fancy workaround code
    # to extend field past 0 and 360 to enable wrap around of gaussian fields
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def calculateRepulsiveGaussianFieldForRelevantObstacle(self, relevantObstacleData, numberOfAngleSegments, maxSensorReading, attractMax):
        gaussianRepulsiveFieldsPerObstacle = [[0 for i in range(numberOfAngleSegments)] for j in range(len(relevantObstacleData))]
        excessAngleSegments = int(numberOfAngleSegments / 2)
        excessAngleRadians = self.mapValue(excessAngleSegments, -180, 180, -math.pi, math.pi)
        gaussianRepulsiveFieldsWithOverFlowPerObstacle = [[0 for i in range(numberOfAngleSegments + (2 * excessAngleSegments))] for j in range(len(relevantObstacleData))]

        for obstacle in range(len(relevantObstacleData)):
            theta_k = relevantObstacleData[obstacle][1]
            sigma_k = relevantObstacleData[obstacle][2] / 2
            #d_k_hat = self.mapValue((maxSensorReading - relevantObstacleData[obstacle][0]), 0, 9, 0, 3)
            #d_k_hat = (attractMax / 1.5) * (maxSensorReading - relevantObstacleData[obstacle][0])
            d_k_hat = (3 / math.pow(relevantObstacleData[obstacle][0] + 0.1,2))
            guassianObstacleCoefficient = attractMax * d_k_hat
            #guassianObstacleCoefficient =  d_k_hat * math.exp(0.5)

            for angleSegment in range(numberOfAngleSegments + (2*excessAngleSegments)):
                theta_i = self.mapValue(angleSegment - excessAngleSegments, 0 - excessAngleSegments, numberOfAngleSegments + excessAngleSegments, -math.pi - excessAngleRadians, math.pi + excessAngleRadians)

                exponentialPower = -((math.pow((theta_k - theta_i), 2)) / (2 * math.pow(sigma_k , 2)))

                # the formuala behind the gaussian obstacle coefficient still confuses me somewhat. Easiest to think of it as the coefficient of repulsion
                gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][angleSegment] = guassianObstacleCoefficient * math.exp(exponentialPower)
                #print("obstacle: " + str(obstacle) + " theta_i: " + str(theta_i) + " exp power: " + str(exponentialPower) + "dk_hat" + str(d_k_hat) + " coefficient: " + str(guassianObstacleCoefficient))
            for angleSegment in range(numberOfAngleSegments):
                if(angleSegment >= 0 and angleSegment <= excessAngleSegments):
                    val1 = gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][angleSegment + excessAngleSegments]
                    val2 = gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][numberOfAngleSegments + angleSegment + excessAngleSegments - 1]
                    biggerVal = self.returnLargest(val1, val2)
                    gaussianRepulsiveFieldsPerObstacle[obstacle][angleSegment] = biggerVal

                elif(angleSegment >= (numberOfAngleSegments - excessAngleSegments) and angleSegment <= numberOfAngleSegments):
                    val1 = gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][angleSegment + (excessAngleSegments)]
                    #print("val1 is: " + str(val1))
                    val2 = gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][excessAngleSegments - (numberOfAngleSegments - angleSegment)]
                    biggerVal = self.returnLargest(val1, val2)
                    gaussianRepulsiveFieldsPerObstacle[obstacle][angleSegment] = biggerVal

                else:
                    gaussianRepulsiveFieldsPerObstacle[obstacle][angleSegment] = gaussianRepulsiveFieldsWithOverFlowPerObstacle[obstacle][angleSegment + excessAngleSegments]


        self.tempGaussRepFields = gaussianRepulsiveFieldsPerObstacle
        return gaussianRepulsiveFieldsPerObstacle

    # Sums all the gaussian repulsive fields into one total repulsive field that the robot can use
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def sumGaussianPotentialFields(self, gaussianFieldsPerObstacle, numberOfAngleSegments):
        gaussianRepulsiveFields = []
        for angleSegment in range(numberOfAngleSegments):
            fieldSum = 0
            for obstacle in range(len(gaussianFieldsPerObstacle)):
                fieldSum = fieldSum + gaussianFieldsPerObstacle[obstacle][angleSegment]

            gaussianRepulsiveFields.append(fieldSum)

        return gaussianRepulsiveFields


    # Instead of summing the gaussian fields at each index, we pick the max value to try stop the
    # effects of lots of small fields summining to one large field
    #
    # Developed by David Russell Jan 2021s
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def maxGaussianRepulsiveField(self, gaussianFieldsPerObstacle, numberOfAngleSegments):
        gaussianRepulsiveFields = []
        for angleSegment in range(numberOfAngleSegments):
            maxVal = 0
            for obstacle in range(len(gaussianFieldsPerObstacle)):
                if(gaussianFieldsPerObstacle[obstacle][angleSegment] > maxVal):
                    maxVal = gaussianFieldsPerObstacle[obstacle][angleSegment]
            gaussianRepulsiveFields.append(maxVal)

        return gaussianRepulsiveFields

    # Calculates the attractive field over the 360 range from the robots view -
    # currently creates a gaussian field and inverts it around the direct heading
    # to the goal
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def calculateAttractiveGaussianPotentialFields(self, theta_goal, coefficientOfAttraction, numberOfAngleSegments):
        gaussianAttractiveFields = [0 for i in range(numberOfAngleSegments)]
        extendedAttractiveField = []
        excessAngleSegments = 100
        excessAngleRadians = self.mapValue(excessAngleSegments, -180, 180, -math.pi, math.pi)
        # for angleSegment in range(numberOfAngleSegments):
        #     theta_i = self.mapValue(angleSegment, 0, numberOfAngleSegments, -math.pi, math.pi)
        #     thetaDifference = theta_goal - theta_i
        #     gaussianAttractiveFields[angleSegment] = (coefficientOfAttraction * abs(thetaDifference))

        for angleSegment in range(numberOfAngleSegments + (2*excessAngleSegments)):
            theta_i = self.mapValue(angleSegment - excessAngleSegments, 0 - excessAngleSegments, numberOfAngleSegments + excessAngleSegments, -math.pi - excessAngleRadians, math.pi + excessAngleRadians)
            exponent = -((math.pow((theta_goal - theta_i), 2)) / (2 * math.pow(1.5 , 2)))
            coefficient = 3.3 * coefficientOfAttraction
            field = coefficient * math.exp(exponent)
            extendedAttractiveField.append(field)

        maxFieldVal = 0
        for angleSegment in range(numberOfAngleSegments + (2*excessAngleSegments)):
            if(extendedAttractiveField[angleSegment] > maxFieldVal):
                maxFieldVal = extendedAttractiveField[angleSegment]

        for angleSegment in range(numberOfAngleSegments + (2*excessAngleSegments)):
             extendedAttractiveField[angleSegment] = maxFieldVal - extendedAttractiveField[angleSegment]

        for angleSegment in range(numberOfAngleSegments):
            if(angleSegment >= 0 and angleSegment <= excessAngleSegments):
                val1 = extendedAttractiveField[angleSegment + excessAngleSegments + 1]
                val2 = extendedAttractiveField[len(gaussianAttractiveFields) + angleSegment + excessAngleSegments - 1]
                biggerVal = self.returnSmallest(val1, val2)
                gaussianAttractiveFields[angleSegment] = biggerVal

            elif(angleSegment >= (len(gaussianAttractiveFields) - excessAngleSegments) and angleSegment <= len(gaussianAttractiveFields)):
                val1 = extendedAttractiveField[angleSegment + (excessAngleSegments)]
                val2 = extendedAttractiveField[excessAngleSegments - (len(gaussianAttractiveFields) - angleSegment) + 1]
                biggerVal = self.returnSmallest(val1, val2)
                gaussianAttractiveFields[angleSegment] = biggerVal

            else:
                gaussianAttractiveFields[angleSegment] = extendedAttractiveField[angleSegment + excessAngleSegments]

        return gaussianAttractiveFields

    # Sums the total repuslvie field and the total attractive field to make a total field for the robot
    # in the range of 0 to 360 degrees
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def calculateTotalGaussianPotentialField(self, numberOfAngleSegments, attractiveField, repulsiveField):
        gaussianTotalField = []
        for angleSegment in range(numberOfAngleSegments):
            gaussianTotalField.append(attractiveField[angleSegment] + repulsiveField[angleSegment])

        return gaussianTotalField

    # Loops over the total field valuess and finds the direction in which the field strength is a minimum,
    # this is the direction the robot should travel in
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def returnOptimumAngleHeading(self, numberOfAngleSegments, totalField):
        optimumAngle = 0
        lowestField = 1000
        for angleSegment in range(numberOfAngleSegments):
            if(totalField[angleSegment] < lowestField):
                lowestField = totalField[angleSegment]
                optimumAngle = angleSegment
        optimumAngle = self.mapValue(optimumAngle, 0, numberOfAngleSegments, -math.pi, math.pi)

        return optimumAngle

    # Map a value from one range to another, useful for mapping from degrees to radians and back
    # again
    #
    # Developed by David Russell December 2020
    # Edited by David Russell Decemeber 2020

    ###################################### END ############################################
    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter

        return newValue

    # Simple function that returns the larger of two values.
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def returnLargest(self, value1, value2):
        if value1 > value2:
            return value1
        else:
            return value2

    # Simple function that returns the smaller of two values.
    #
    # Developed by David Russell Jan 2021
    # Edited by David Russell Jan 2021

    ###################################### END ############################################
    def returnSmallest(self, value1, value2):
        if value1 < value2:
            return value1
        else:
            return value2
