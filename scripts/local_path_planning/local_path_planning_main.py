#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String

from create_obstacle_map_from_sensors import flattenLidar2DMap
from calculate_robot_force_PF import obstclDetGaussPotentialFields
from calc_and_publish_robot_vel import moveRobot
from testing_create_obstacle_map_from_sensors import testCreateObstacleMap

class localPathPlanning():
    def __init__(self):
        self.nextGoalPoint = Point(0,0,0)
        self.navigating = False
        self.robotAlginedToGoal = False

        self.lidarOffset = [0, 0.136]
        self.numberAngleSegments = 300
        self.flattenNearestObstacles = flattenLidar2DMap(self.numberAngleSegments, 0.246, 0.36185, self.lidarOffset)
        self.ODGPF = obstclDetGaussPotentialFields(4, 0.1, 10, self.numberAngleSegments)
        self.moveRobot = moveRobot()
        self.LidarDataPoints = 0
        self.newLIDARDataReceived = False

        self.robotPoint = Point(0, 0, 0)
        self.goalPoints = []
        self.robotYaw = 0
        self.usingBackupLocalPlanner = False

        self.robotVel = 0
        self.robotw = 0
        self.robotHeading = 0


        self.ANGLE_DIFF_MIN_ALIGNED = 0.1
        self.ROTATION_DAMPING_FACTOR = 0.8
        self.ROTATION_MAX_ACCEL = 0.4
        self.MAX_ROBOT_ANGULAR_VEL = 2.0



        rospy.on_shutdown(self.shutDown)
        rospy.Subscriber('/UGV/laser/scan', PointCloud2, self.callback_pointcloud)
        rospy.Subscriber('nextGoalPoint', Point, self.callback_goalPoint)
        odom_sub = rospy.Subscriber('/odom', Odometry, self.callback_robotPose)
        self.goalStatusPublisher = rospy.Publisher('goalStatus', String, queue_size=1)

    def shutDown(self):
        print("shut down called")
        self.moveRobot.stopRobot()

    def callback_robotPose(self, data):
        robotPose = data
        self.robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, self.robotYaw) = euler_from_quaternion (robotQuaternionList)



    def callback_goalPoint(self, data):
        #print("goal point: " + str(data))
        newGoalPoint = data
        if newGoalPoint != self.nextGoalPoint:
            print("this happened?")
            self.nextGoalPoint = newGoalPoint
            self.navigating = True


    def callback_pointcloud(self, data):
        self.LidarDataPoints = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        self.newLIDARDataReceived = True


    def handleLocalPathPlanning(self):
        if self.navigating == True:
            if self.robotAlginedToGoal == True:
                timeNow = datetime.datetime.now()
                self.updateRobotPositionLog(self.robotPoint, timeNow)
                self.handleNavigationSwitchingAlgorithms()

            else:
                theta_goalHeadingNoRobotOrientation = self.calculateRobotToGoalHeading(self.nextGoalPoint, self.robotPoint)
                self.robotAlginedToGoal = self.moveRobot.alignRobotToPoint(theta_goalHeadingNoRobotOrientation, self.robotYaw)
        pass

    def updateRobotPositionLog(self, robotPoint, time):
        ########################### ----------------------------------------------------------
        ############### missing code needed here ################################################
        ##########################--------------------------------------------------------------
        pass

    def checkIfRobotStuck(self):
        ########################### ----------------------------------------------------------
        ############### missing code needed here ################################################
        ##########################--------------------------------------------------------------
        return False

    def resolveRobotStuck(self, robotStuck):
        pass

    def handleNavigationSwitchingAlgorithms(self):
        robotStuck = self.checkIfRobotStuck()
        if robotStuck == False:
            # navigate using current local path planner
            robotAtGoal = self.checkIfGoalReached(self.nextGoalPoint, self.robotPoint)
            if robotAtGoal == True:
                #next goal point reached, reset navigation variables and ask for new goal point
                self.resetNavigationVariables()
                self.askForNextPointOnPath()
            else:
                #navigate via either the potential field method or bug method
                self.navigateWithBugOrPotentialFields(self.usingBackupLocalPlanner)
        else:
            # If were stuck using the potential field method, switch to the bug algorithm
            if self.usingBackupLocalPlanner == False:
                self.usingBackupLocalPlanner = True
            # If we are stuck on both methods, local planner has failed and new goal point required
            else:
                self.resetNavigationVariables()
                # publish and ask for new goal point
                ########################### ----------------------------------------------------------
                ############### missing code needed here ################################################
                ##########################--------------------------------------------------------------

    def navigateWithBugOrPotentialFields(self, backupAlgorithmStatus):

        desiredVel = 0
        desiredHeading = 0
        if self.newLIDARDataReceived == True:
            self.newLIDARDataReceived = False
            flattenedObstacles = self.flattenNearestObstacles.return2DObstacleMap(self.LidarDataPoints)
            if backupAlgorithmStatus == False:
                #use main algorithm
                theta_goalHeadingNoRobotOrientation = self.calculateRobotToGoalHeading(self.nextGoalPoint, self.robotPoint)

                theta_goalHeadingWithRobotOrientation = self.changeHeadingToRobotFrame(theta_goalHeadingNoRobotOrientation, self.robotYaw)
                self.robotHeading, fieldStrength = self.ODGPF.calcHeadingAngle(theta_goalHeadingWithRobotOrientation, flattenedObstacles)
                self.robotw = self.calcRobotAngularForGoalAlignment(self.robotw, self.robotYaw, theta_goalHeadingNoRobotOrientation)
                self.robotVel = 0.5
            else:
                # use bug algorithm backup
                pass

        self.moveRobot.calcRobotVelocities(self.robotVel, self.robotHeading, self.robotw)
        # use the desired vel and heading




    def calcRobotAngularForGoalAlignment(self, lastRobotw, robotOrientation, theta_robotToGoal):
        angularVel = 0

        angleDiff = robotOrientation - theta_robotToGoal

        # Keeps angle diff between -pi and pi
        if(angleDiff < -math.pi):
            angleDiff = angleDiff + (2*math.pi)
        if(angleDiff > math.pi):
            angleDiff = angleDiff - (2*math.pi)

        if abs(angleDiff) < self.ANGLE_DIFF_MIN_ALIGNED:
            angularVel = 0
        else:
            accel = self.mapValue(angleDiff, -math.pi, math.pi, self.ROTATION_MAX_ACCEL, -self.ROTATION_MAX_ACCEL)
            # Apply acceleration
            angularVel = lastRobotw + accel
            # Apply damping
            angularVel = self.ROTATION_DAMPING_FACTOR * angularVel


        return angularVel

    def resetNavigationVariables(self):
        self.usingBackupLocalPlanner = False
        self.robotAlginedToGoal = True
        self.navigating = False
        self.moveRobot.stopRobot()

    def askForNextPointOnPath(self):
        print("next point")
        self.goalStatusPublisher.publish("next")


    def calculateRobotToGoalHeading(self, goalPoint, robotPoint):
        changeInX = goalPoint.x - robotPoint.x
        changeInY = goalPoint.y - robotPoint.y
        theta_robotToGoal = math.atan2(changeInY, changeInX)            # returns a global headin betwen -Pi and Pi

        return theta_robotToGoal

    def changeHeadingToRobotFrame(self, theta_robotToGoal, robotYaw):
        theta_heading = robotYaw - theta_robotToGoal
        if(theta_heading < -math.pi):
            theta_heading = theta_heading + (2*math.pi)
        if(theta_heading > math.pi):
            theta_heading = theta_heading - (2*math.pi)
        return theta_heading

    def checkIfGoalReached(self, goalPoint, robotPoint):
        changeInX = goalPoint.x - robotPoint.x
        changeInY = goalPoint.y - robotPoint.y
        distance = self.distBetweenPoints(goalPoint, robotPoint)
        if distance < 0.20:
            return True
        else:
            return False

    def distBetweenPoints(self, point1, point2):
        changeInX = point2.x - point1.x
        changeInY = point2.y - point1.y
        distance = math.sqrt(math.pow(changeInX, 2) + math.pow(changeInY, 2))
        return distance

    def mapValue(self, value, lowerBefore, upperBefore, lowerAfter, upperAfter):
        newValue = 0
        newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
        newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter


        return newValue



def main():
    rospy.init_node('local_path_planner', anonymous=True)
    localPathPlanner = localPathPlanning()
    rate = rospy.Rate(5)
    while not rospy.is_shutdown():
        localPathPlanner.handleLocalPathPlanning()
        rate.sleep()


if __name__ == '__main__':
    main()
