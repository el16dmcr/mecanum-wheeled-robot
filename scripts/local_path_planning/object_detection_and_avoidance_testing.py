#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler

from create_obstacle_map_from_sensors import flattenLidar2DMap
from calculate_robot_force_PF import obstclDetGaussPotentialFields
from calc_and_publish_robot_vel import moveRobot
from testing_create_obstacle_map_from_sensors import testCreateObstacleMap

lidarOffset = [0, 0.136]
rospy.init_node('object_avoidance', anonymous=True)
numberAngleSegments = 300
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)
ODGPF = obstclDetGaussPotentialFields(4, 0.1, 10, numberAngleSegments)
testFile = testCreateObstacleMap()
moveRobot = moveRobot()


# Point needs arguments x, y, z
goalIndex = 0
#, Point(0,0,0), Point(0, 10, 0)
goalPoints = [Point(10, 0, 0)]
robotAlignedToPoint = False
robotReachedGoal = False
robotPose = PoseStamped()
waiting = True
errorsFound = False
plotDelay = 101
pointCloudProcessed = True

timeLast = datetime.datetime.now()
timeNow = datetime.datetime.now()

def main():
    #Code begins
    global ODGPF
    global moveRobot
    global errorsFound
    rospy.on_shutdown(shutDown)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, callback_pointcloud)
    odom_sub = rospy.Subscriber('/odom', Odometry, callback_robotPose)
    plt.ion()


    #errorsFound = testFile.testFunctions()
    while not rospy.is_shutdown():
        if errorsFound == False:
            plotLiveData()
            testHeading = 0
            #moveRobot.calcRobotVelocities(0.1, testHeading)

def plotLiveData():
    global plotDelay
    global ODGPF
    plotDelay = plotDelay + 1
    if plotDelay > 100:
        plotDelay = 0
        plt.figure(1)
        plt.cla()
        plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5', linewidth= 0.1)
        plt.xlabel('Angle around robot / degrees', fontsize=14)
        plt.ylabel('Distance to nearest object / m', fontsize=14)
        plt.title('Flattened graph of nearest obstacles', fontsize=18)
        fig = plt.gcf()
        fig.canvas.draw()


        plt.figure(2)
        plt.cla()
        #plt.style.use('seaborn')
        plt.plot(ODGPF.xAxis, ODGPF.repulsiveField, '#e69ca4', label = 'Repulsive')
        plt.plot(ODGPF.xAxis, ODGPF.attractiveField, '#b3d6ac', label = 'Attractive')
        plt.plot(ODGPF.xAxis, ODGPF.totalField, '#89c3e5', label = 'Total')
        plt.xlabel('Angle around robot / degrees', fontsize=14)
        plt.ylabel('Field strength', fontsize=14)
        plt.title('Obstacle detection gaussian potential fields', fontsize = 18)
        plt.legend()
        # if(len(ODGPF.tempGaussRepFields) != 0):
        #     for i in range(ODGPF.tempNumberOfObstacles):
        #         try:
        #             #, label = 'gaussrep' + str(i)
        #             plt.plot(ODGPF.xAxis, ODGPF.tempGaussRepFields[i])
        #         except:
        #             print("random error plotting all repulsive fields")
        fig = plt.gcf()
        fig.canvas.draw()


def testing():
    global waiting
    global robotPose
    while waiting == True:
        pass

    # goalCoords = Point(1, 0 ,0)
    # answer = calculateRobotToGoalHeading(goalCoords, robotPose)
    # printAnswerVsExpected(answer, 0)
    #
    # goalCoords = Point(0, 1 ,0)
    # answer = calculateRobotToGoalHeading(goalCoords, robotPose)
    # printAnswerVsExpected(answer, (math.pi / 2))
    #
    # goalCoords = Point(1, 1 ,0)
    # answer = calculateRobotToGoalHeading(goalCoords, robotPose)
    # printAnswerVsExpected(answer, (math.pi / 4))
    #
    # goalCoords = Point(-1, -1 ,0)
    # answer = calculateRobotToGoalHeading(goalCoords, robotPose)
    # printAnswerVsExpected(answer, -(3 * math.pi / 4))
    #
    # goalCoords = Point(-1, -1 ,0)
    # answer = calculateRobotToGoalHeading(goalCoords, robotPose)
    # printAnswerVsExpected(answer, -(3 * math.pi / 4))

    #testing_robotToGoalHeadings()

def testing_robotToGoalHeadings():
    global waiting
    while waiting == True:
        pass

    robotPose = PoseStamped()
    print(robotPose)
    goalCoords = Point(10, 0, 0)
    robotPose.pose.position.x = 5
    robotPose.pose.position.y = 0
    robotPose.pose.position.z = 0

    answerHeading = calculateRobotToGoalHeading(goalCoords, robotPose)
    if(answerHeading != 0):
        print("Incorrect answer, expected: " + str(0) + ", got: " + str(answerHeading))

    robotPose.pose.position.x = 10
    robotPose.pose.position.y = -10
    answerHeading = calculateRobotToGoalHeading(goalCoords, robotPose)
    if(answerHeading != math.pi/2):
        print("Incorrect answer, expected: " + str(math.pi/2) + ", got: " + str(answerHeading))

    robotPose.pose.position.x = 20
    robotPose.pose.position.y = 0
    answerHeading = calculateRobotToGoalHeading(goalCoords, robotPose)
    if(answerHeading != math.pi):
        print("Incorrect answer, expected: " + str(math.pi) + ", got: " + str(answerHeading))

    robotPose.pose.position.x = 10
    robotPose.pose.position.y = 10
    answerHeading = calculateRobotToGoalHeading(goalCoords, robotPose)
    if(answerHeading != -math.pi/2):
        print("Incorrect answer, expected: " + str(-math.pi/2) + ", got: " + str(answerHeading))


def printAnswerVsExpected(answer, expected):
    print("answer is " + str(answer) + " should be: " + str(expected))


def shutDown():
    global moveRobot
    print("shutdown time!")
    moveRobot.stopRobot()

# this eventually needs to be replaced by our encoders sending odometry data over a serial line
def callback_robotPose(msg):
    global waiting
    global robotPose
    global robotReachedGoal
    global goalPoints

    global moveRobot
    global goalIndex
    global robotAlignedToPoint
    waiting = False
    robotPose = msg

    if(robotReachedGoal == False):
        goalCoords = goalPoints[goalIndex]
        if checkIfGoalReached(goalCoords, robotPose) == True:
            goalIndex = goalIndex + 1
            moveRobot.stopRobot()
            robotAlignedToPoint = False
            if(goalIndex == len(goalPoints)):
                robotReachedGoal = True
                robotAlignedToPoint = True
                moveRobot.stopRobot()




def callback_pointcloud(data):
    global robotReachedGoal
    global moveRobot
    global flattenNearestObstacles
    global ODGPF
    global goalPoints
    global goalIndex
    global robotPose
    global errorsFound
    global robotAlignedToPoint

    global timeLast
    global timeNow

    timeNow = datetime.datetime.now()
    delta = timeNow - timeLast
    milliSecondsPassed = int(delta.total_seconds() * 1000)
    #print("millisecondspassed: " + str(milliSecondsPassed))
    timeLast = datetime.datetime.now()

    if robotReachedGoal == False:
        goalCoord =  goalPoints[goalIndex]

    robotOrientation = robotPose.pose.pose
    robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
    (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)


    if robotAlignedToPoint == True:

        if robotReachedGoal == False:


            # return the point cloud to a generator variable
            gen = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)

            # generate a 2D map of obstacles surrounding he robot using lidar pointcloud
            flattenedObstacles = flattenNearestObstacles.return2DObstacleMap(gen)

            # calculate the heading towards the goal
            theta_goalHeadingNoRobotOrientation = calculateRobotToGoalHeading(goalCoord, robotPose)
            #print("goal heading no robot orientation: " + str(theta_goalHeadingNoRobotOrientation))

            theta_goalHeadingWithRobotOrientation = changeHeadingToRobotFrame(theta_goalHeadingNoRobotOrientation, robotYaw)
            optimumHeading, closestDist = ODGPF.calcHeadingAngle(theta_goalHeadingWithRobotOrientation, flattenedObstacles)
            #print("heading: " + str(optimumHeading))

            #acceleMagnitude = mapValue(closestDist, 4, 0, 0.005, 0.01)
            #print("accel mag: " + str(acceleMagnitude))
            #moveRobot.calcRobotVelocities(0.2, optimumHeading)
            #moveRobot.calcRobotVelocities(0.8, optimumHeading)
        else:
            moveRobot.stopRobot()
    else:
        theta_goalHeadingNoRobotOrientation = calculateRobotToGoalHeading(goalCoord, robotPose)
        robotAlignedToPoint = moveRobot.alignRobotToPoint(theta_goalHeadingNoRobotOrientation, robotYaw)





def calculateRobotToGoalHeading(goalPoint, robotPose):
    changeInX = goalPoint.x - robotPose.pose.pose.position.x
    changeInY = goalPoint.y - robotPose.pose.pose.position.y
    theta_robotToGoal = math.atan2(changeInY, changeInX)            # returns a global headin betwen -Pi and Pi

    return theta_robotToGoal

def changeHeadingToRobotFrame(theta_robotToGoal, robotYaw):
    theta_heading = robotYaw - theta_robotToGoal
    if(theta_heading < -math.pi):
        theta_heading = theta_heading + (2*math.pi)
    if(theta_heading > math.pi):
        theta_heading = theta_heading - (2*math.pi)
    return theta_heading

def checkIfGoalReached(goalPoint, robotPose):
    changeInX = goalPoint.x - robotPose.pose.pose.position.x
    changeInY = goalPoint.y - robotPose.pose.pose.position.y
    distance = math.sqrt(math.pow(changeInX, 2) + math.pow(changeInY, 2))
    #print("dist to goal " + str(distance))
    if distance < 0.20:
        return True
    else:
        return False

def mapValue(value, lowerBefore, upperBefore, lowerAfter, upperAfter):
    newValue = 0
    newValue = (value - lowerBefore) / (float(upperBefore - lowerBefore))
    newValue = (newValue * (upperAfter - lowerAfter)) + lowerAfter


    return newValue


if __name__ == '__main__':
    main()
