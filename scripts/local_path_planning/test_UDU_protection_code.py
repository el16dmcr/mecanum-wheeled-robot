#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

def main():
    rospy.init_node('test_UDU_avoid_crashing', anonymous=True)
    rate = rospy.Rate(5)

    desiredVelPub = rospy.Publisher('desired_vel', numpy_msg(Floats), queue_size=1)
    while not rospy.is_shutdown():
        desiredVel = np.array([0.6, math.pi, 0],  dtype=np.float32)
        desiredVelPub.publish(desiredVel)
        rate.sleep()


if __name__ == '__main__':
    main()
