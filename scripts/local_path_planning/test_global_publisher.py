#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String

goalIndex = 0

def main():
    global goalIndex
    rospy.init_node('test_global_publisher', anonymous=True)
    rate = rospy.Rate(0.4)
    #goalPoints = [Point(2, 1, 0), Point(3, 2, 0), Point(5, 2, 0), Point(6, 0, 0), Point(8, -1, 0), Point(10, 0, 0), Point(8, -1, 0), Point(6, 0, 0), Point(5, 2, 0), Point(3, 2, 0), Point(2,1,0), Point(0,0,0)]
    goalPoints = [Point(10, -10, 0), Point(10.1, -10, 0)]
    nextGoalPub = rospy.Publisher('/globalPath', Point, queue_size=1)
    counter = 0

    #rospy.Subscriber('frontierPointStatus', String, handleGlobalPathPoints)
    while not rospy.is_shutdown():
        if(counter < 5):
            #counter = counter + 1
            for i in range(len(goalPoints)):
                print("publish point: " + str(goalPoints[i]))
                nextGoalPub.publish(goalPoints[i])

        rate.sleep()

def handleGlobalPathPoints(data):
    global goalIndex
    print("new frontier asked for")
    goalIndex = goalIndex + 1


if __name__ == '__main__':
    main()
