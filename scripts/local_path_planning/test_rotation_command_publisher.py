#! /usr/bin/env python

import rospy, math, random
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from std_msgs.msg import String
from rospy_tutorials.msg import Floats
from rospy.numpy_msg import numpy_msg

rotationsIndex = 0
goalRotations = np.array([math.pi, 2*math.pi, math.pi / 2])

def main():
    global rotationsIndex
    global goalRotations
    global newGoalRotationPub
    rospy.init_node('test_rotation_command', anonymous=True)
    rate = rospy.Rate(5)


    rotRequiredPub = rospy.Publisher('desired_angle_rotation', numpy_msg(Floats), queue_size=1)
    rospy.Subscriber('rotation_complete', String, callback_rotComplete)
    while not rospy.is_shutdown():
        if rotationsIndex <= len(goalRotations):
            pubValue = np.array([goalRotations[rotationsIndex]], dtype=np.float32)
            #print(pubValue)
            #print("val published " + str(pubValue))
            rotRequiredPub.publish(pubValue)
    rate.sleep()

def callback_rotComplete(data):
    global rotationsIndex
    global goalRotations
    print("rotation complete of: " + str(goalRotations[rotationsIndex]) + " radians")
    rotationsIndex = rotationsIndex + 1


if __name__ == '__main__':
    main()
