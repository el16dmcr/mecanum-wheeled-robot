#! /usr/bin/env python
import rospy
from time import sleep
import math
import matplotlib.pyplot as plt
import numpy as np
from geometry_msgs.msg import Point
from create_obstacle_map_from_sensors import flattenLidar2DMap

class testCreateObstacleMap():
    def __init__(self):
        lidarOffset = [-0.07 , 0]
        self.testFlatten = flattenLidar2DMap(360, 0.246, 0.36185, lidarOffset)
        self.occlusionZone1 = np.array([Point(-0.3, 0.3, -0.15), 0.2, 0.15, 0.07])
        self.occlusionZone2 = np.array([Point(0.1, 0.3, -0.15), 0.2, 0.15, 0.07])

    def testFunctions(self):
        errorsFound = False


        expected = True
        testPoint = [-0.21, 0.37, -0.11]
        answer = self.testFlatten.checkIfPointInOcclusionZone(testPoint, self.occlusionZone1,self.occlusionZone2)
        if(answer != expected):
            errorsFound = True
            print("testPoint " + str(testPoint) + " should have been in occlusion zone")

        expected = False
        testPoint = [-1, 0.37, -0.11]
        answer = self.testFlatten.checkIfPointInOcclusionZone(testPoint, self.occlusionZone1,self.occlusionZone2)
        if(answer != expected):
            errorsFound = True
            print("testPoint" + str(testPoint) + " should not have been in occlusion zone")

        


        if errorsFound == True:
            print("Error found")

        return errorsFound
