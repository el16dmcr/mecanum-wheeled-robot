#! /usr/bin/env python

### ------------------------- General imports ---------------------------###
import rospy, math, random
import numpy as np
import time
import matplotlib.pyplot as plt
import os
from enum import Enum
from std_srvs.srv import Empty
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
#from keras.layers import Dense

### -------------- Message types and subscirbers for gazebo -----------------###
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Twist                 # This is the message type to send to the cmd_vel topic
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion, quaternion_from_euler

from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState

### ------------------------- Personal classes ---------------------------- ###
from create_obstacle_map_from_sensors import flattenLidar2DMap

class convergenceTypes(Enum):
    EPOCHS = 1
    MIN_GRAD = 2
    BEST_PERF = 3

# Even though this is a robot population, can only run one robot at a time and therefore subscribers and publishers
# should be in this class
class robotPopulation():
    def __init__(self, brainsExist, convergenceType, popSize):

        self.trainingFinished = False
        self.convergenceType = convergenceType
        self.currentGeneration = 0
        self.populationSize = popSize
        self.robots = []
        self.currentRobotIndex = 0
        self.currentRobotInitialised = False
        self.robotPose = PoseStamped()
        self.currentGoalCounter = 0
        self.goalIndex = 0

        self.possibleGoals = np.array([[Point(10,0,0)], [Point(0,10,0)], [Point(-10,0,0)], [Point(0,-10,0)]])
        self.robotGoal = self.possibleGoals[self.goalIndex]
        #self.chooseNewGoalRandomly()
        self.selectNextGoal()
        self.newDataReceived = False
        self.timeWorldReset = time.time()
        self.worldReset = True
        self.bestFitnessSoFar = 0
        self.bestFitnessLastGen = 0
        self.robotTimeLimit = 30
        self.timeLimitIncreasedCounter = 0
        self.generationsSinceLastTimeUpdate = 0
        self.bestFitnessEachGeneration = []
        self.meanFitnessEachGeneration = []
        self.worstFitnessEachGeneration = []
        self.robotsReachedGoalSequentiallyCounter = 0

        self.robotContactsRobotDimensions = [0.272, 0.51, 0.340, 0.51, 0.272, 0.51, 0.340, 0.51]
        self.robotMinDistContactPointArray = []
        for i in range(len(self.robotContactsRobotDimensions)):
            self.robotMinDistContactPointArray.append(self.robotContactsRobotDimensions[i] * 1.08 )

        self.timeStartForNextGeneration = time.time()

        self.numberOfAngleSegments = 90

        self.bestRobotThisSession = [0]

        self.dirname = os.path.dirname(os.path.realpath(__import__("__main__").__file__))

        self.robots = [0 for x in range(self.populationSize)]

        if(brainsExist == False):
            #generate random robot brains
            for robotIndex in range(self.populationSize):
                self.robots[robotIndex] = robot(self.robotGoal, None, robotIndex, self.robotTimeLimit, self.numberOfAngleSegments)
        else:
            #load the previous robot brains from wherever in their training they were
            self.loadSavedRobotBrain()

        self.lidarOffset = [0 , -0.07]

        self.flattenNearestObstacles = flattenLidar2DMap(self.numberOfAngleSegments, 0.246, 0.36185, self.lidarOffset)
        self.flattenedObstacles = [100 for x in range(self.numberOfAngleSegments)]

        self.matingPool = [0 for x in range((self.populationSize) - 1)]

        rospy.Subscriber('/UGV/laser/scan', PointCloud2, self.callback_pointcloud)
        rospy.Subscriber('/odom', Odometry, self.callback_robotPose)
        self.movementPublisher = rospy.Publisher('cmd_vel', Twist, queue_size=1)

        self.moveRobotmsg = Twist()
        self.state_msg = ModelState()
        self.state_msg.model_name = 'UGV'
        self.state_msg.pose.position.x = 0
        self.state_msg.pose.position.y = 0
        self.state_msg.pose.position.z = 0
        self.state_msg.pose.orientation.x = 0
        self.state_msg.pose.orientation.y = 0
        self.state_msg.pose.orientation.z = 0
        self.state_msg.pose.orientation.w = 0

        self.stopRobot()

        plt.ion()
        self.fig = plt.gcf()
        self.ax = self.fig.add_subplot(111)


    def plotData(self):
        plt.cla()
        plt.plot(self.bestFitnessEachGeneration, 'g-', label = 'Best Fitness Per Generation')
        plt.plot(self.meanFitnessEachGeneration, 'b-', label = 'Mean Fitness Per Generation')
        plt.plot(self.worstFitnessEachGeneration,'r-', label = 'Worst Fitness Per Generation')
        plt.xlabel('Generation number')
        plt.ylabel('Fitness')
        plt.legend()
        plt.title('Genetic algorithm fitness tracking')

        self.fig.canvas.draw()

    def returnWorstFitnessForGeneration(self):
        worstFitness = 10
        for robotIndex in range(self.populationSize):
            if(self.robots[robotIndex].fitness < worstFitness):
                worstFitness = self.robots[robotIndex].fitness
        return worstFitness

    def callback_pointcloud(self, data):
        gen = point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True)
        # generate a 2D map of obstacles surrounding he robot using lidar pointcloud
        self.flattenedObstacles = self.flattenNearestObstacles.return2DObstacleMap(gen)
        self.newDataReceived = True


    def callback_robotPose(self, msg):
        self.robotPose = msg

    def stopRobot(self):
        self.moveRobotmsg.angular.x = 0
        self.moveRobotmsg.angular.y = 0
        self.moveRobotmsg.angular.z = 0
        self.moveRobotmsg.linear.x = 0
        self.moveRobotmsg.linear.y = 0
        self.moveRobotmsg.linear.z = 0
        self.movementPublisher.publish(self.moveRobotmsg)

    def resetRobotPosition(self):
        self.timeWorldReset = time.time()
        self.worldReset = False
        rospy.wait_for_service('/gazebo/set_model_state')
        try:
            set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
            resp = set_state( self.state_msg  )

        except rospy.ServiceException, e:
            print "Service call failed: %s" % e



    def handlePopulation(self):
        #self.flattenNearestObstacles.plotData()

        if( self.trainingFinished == False ):
            robotFinished = self.runCurrentRobot(self.currentRobotIndex)

            if( robotFinished == True ):
                self.currentRobotIndex = self.currentRobotIndex + 1
                # reset robot and velocities
                self.stopRobot()
                self.resetRobotPosition()
                self.currentRobotInitialised = False

            if( self.currentRobotIndex >= self.populationSize ):
                self.nextGeneration()
                self.plotData()


    def selectNextGoal(self):

        self.currentGoalCounter = self.currentGoalCounter + 1
        if (self.currentGoalCounter > 3 ):
            self.currentGoalCounter = 0
            self.goalIndex = self.goalIndex + 1
            self.goalIndex = self.goalIndex % 4
            self.robotGoal = self.possibleGoals[self.goalIndex]



    def checkIfWorldReset(self):
        currentTime = time.time()
        if(currentTime - self.timeWorldReset > 1):
            self.worldReset = True


    def runCurrentRobot(self, robotIndex):
        robotFinished = False
        if(self.worldReset == True):
            if(self.currentRobotInitialised == False):
                self.robots[robotIndex].initRobotForGeneration()
                self.currentRobotInitialised = True

            if(self.newDataReceived == True):
                self.newDataReceived = False
                # check if robot crashed using lidar
                crashed = self.checkIfRobotTooCloseToObstacle(self.flattenedObstacles)
                self.robots[robotIndex].incrementRunandCrashCounter(crashed)
                robotFinished = False
                robotFinished, robotVelocities = self.robots[robotIndex].handleCurrentRobot(self.robotPose, self.flattenedObstacles)
                if( robotFinished == False ):
                    self.moveRobot(robotVelocities)
                else:
                    self.stopRobot()
                    # see if robot reached goal

        else:
            self.checkIfWorldReset()


        return robotFinished

    def checkIfRobotTooCloseToObstacle(self, flattenedObstacles):
        crashed = False
        numContactPoints = len(self.robotMinDistContactPointArray)
        numAngleSegments = len(flattenedObstacles)
        for contactPoint in range(len(self.robotMinDistContactPointArray)):

            index = contactPoint * (numAngleSegments/numContactPoints)
            dist = flattenedObstacles[index]
            if(dist < self.robotMinDistContactPointArray[contactPoint]):
                crashed = True

        return crashed

    def moveRobot(self, robotVelocities):
        #robotVx = ((robotVelocities[0, 0] - 0.5) * 2)
        #robotVy = ((robotVelocities[0, 1] - 0.5) * 2)
        velocityMagnitude = ((robotVelocities[0, 0] - 0.5) * 3)
        robotHeading = ((robotVelocities[0, 1] - (math.pi/2)) * 2)
        robotVx = velocityMagnitude * math.cos(robotHeading)
        robotVy = -velocityMagnitude * math.sin(robotHeading)
        #robotw = ((robotVelocities[0, 2] - 0.5) * 2)
        # need some sort of scaling from 0 to 1 to actual velocities and speeds. Including negative!!!!
        self.moveRobotmsg.linear.x = robotVx
        self.moveRobotmsg.linear.y = robotVy
        self.moveRobotmsg.angular.z = 0
        self.movementPublisher.publish(self.moveRobotmsg)

    def nextGeneration(self):
        self.timeStartForNextGeneration = time.time()
        self.currentRobotIndex = 0
        self.currentGeneration = self.currentGeneration + 1
        self.generationsSinceLastTimeUpdate = self.generationsSinceLastTimeUpdate + 1
        #self.chooseNewGoalRandomly()
        self.selectNextGoal()
        self.checkIfAnyRobotReachedGoal()

        if(self.checkForConvergence() == True):
            self.trainingFinished = True
            self.saveAllRobots()
        else:
            bestRobotIndex = self.evaluatePopulation()
            self.naturalSelection(bestRobotIndex, self.matingPool)
            self.clearNetsFromMemory()
            print("new generation generated-generation: " + str(self.currentGeneration))
            print("Best fitness last generation" + str(self.bestFitnessLastGen))
            print("Best fitness so far: " + str(self.bestFitnessSoFar))
            self.bestFitnessEachGeneration.append(self.bestFitnessLastGen)
            self.printTimeTakenForNextGeneration()
            #if(self.decideIfRobotTimeShouldBeIncreased() == True):
                #self.increaseTime()

    def printTimeTakenForNextGeneration(self):
        currentTime = time.time()
        timePassed = currentTime - self.timeStartForNextGeneration
        print("time passed for generation " + str(self.currentGeneration) + ": " + str(timePassed))

    def checkIfAnyRobotReachedGoal(self):
        for robotIndex in range(self.populationSize):
            if self.robots[robotIndex].robotReachedGoal == True:
                self.robotsReachedGoalSequentiallyCounter = self.robotsReachedGoalSequentiallyCounter  + 1
            else:
                self.robotsReachedGoalSequentiallyCounter = 0

    def evaluatePopulation(self):
        # loop through all robots and find the best robot index
        # log the best fitness in this generation
        #  generate a mating pool and pass to natural selection
        bestFitness = 0
        sumFitness = 0

        # Loop through and find best robot and sum all the fitness scores
        for robotIndex in range(self.populationSize):
            sumFitness = sumFitness + self.robots[robotIndex].fitness
            if( self.robots[robotIndex].fitness > bestFitness):
                bestFitness = self.robots[robotIndex].fitness
                bestRobotIndex = robotIndex

        # If new best fitness acquired, save that specific robots DNA
        self.bestFitnessLastGen = bestFitness
        self.meanFitnessEachGeneration.append(sumFitness / self.populationSize)
        self.worstFitnessEachGeneration.append(self.returnWorstFitnessForGeneration())

        if( self.bestFitnessLastGen > self.bestFitnessSoFar ):
            self.bestFitnessSoFar = self.bestFitnessLastGen
            self.saveNewBestRobot(bestRobotIndex)

        # Normalise scores by divinging by total score for each fitness
        for robot in range(self.populationSize):
            self.robots[robot].fitness = self.robots[robot].fitness / sumFitness


        # Populate a mating pool for natural selection to occur from - 2 parents per child
        self.matingPool = [0 for x in range(self.populationSize - 1)]
        for parent in range(self.populationSize - 1):
            index = self.pickOneBrainWithPriority()
            self.matingPool[parent] = index

        return bestRobotIndex

    def pickOneBrainWithPriority(self):
        index = 0
        r = random.uniform(0, 1)
        while( r > 0):
            r = r - self.robots[index].fitness
            index = index + 1

        index = index - 1
        return index


    def naturalSelection(self, bestRobotIndex, matingPool):
        newRobots = []
        randomIndex = 0
        print("mating pool is " + str(matingPool))
        newRobots = self.crossoverPurelyMutation(matingPool, newRobots, bestRobotIndex)

        for robotIndex in range(self.populationSize):
            self.robots[robotIndex] = newRobots[robotIndex]

    def generalCrossover(self, matingPool, newRobots, bestRobotIndex):
        for robotIndex in range(self.populationSize):
            if robotIndex == 0:
                # Always want to ensure that robots dont get stuipder by random chance, therefore we always pick the best robot at least once
                parent1 = self.robots[bestRobotIndex].brain         # select the brain not the whole robot class
                parent2 = self.robots[bestRobotIndex].brain
            else:
                randomIndex = random.randint(0, NumParentsInPool - 1)
                parent1 = self.robots[matingPool[randomIndex]].brain
                matingPool.pop(randomIndex)
                NumParentsInPool = NumParentsInPool - 1
                randomIndex = random.randint(0, NumParentsInPool - 1)
                parent2 = self.robots[matingPool[randomIndex]].brain
                matingPool.pop(randomIndex)
                NumParentsInPool = NumParentsInPool - 1

            # perform crossover of parent DNA to make child DNA
            childBrain = parent1.crossover(parent2)
            # Perform mutation with a random chance, but not on the best robot from last generation
            if( robotIndex != 0):
                childBrain.mutateAfterCrossover(0.1)

            newRobots.append(robot(self.robotGoal, childBrain, robotIndex, self.robotTimeLimit, self.numberOfAngleSegments))

        return newRobots

    def crossoverPurelyMutation(self, matingPool, newRobots, bestRobotIndex):
        NumParentsInPool = len(matingPool)
        numRobots = len(matingPool) + 1
        for robotIndex in range(numRobots):
            if robotIndex == 0:
                parent = self.robots[bestRobotIndex].brain
            else:
                randomIndex = random.randint(0, NumParentsInPool - 1)
                parent = self.robots[matingPool[randomIndex]].brain
                matingPool.pop(randomIndex)
                NumParentsInPool = NumParentsInPool - 1

            if( robotIndex != 0):
                childBrain = parent.mutateNoCrossover(0.2)
            else:
                childBrain = parent.mutateNoCrossover(0)

            newRobots.append(robot(self.robotGoal, childBrain, robotIndex, self.robotTimeLimit, self.numberOfAngleSegments))
        return newRobots


    def saveNewBestRobot(self, bestRobotIndex):
        parent = self.robots[bestRobotIndex].brain
        childBrain = parent.crossover(parent)
        self.bestRobotThisSession[0] = robot(self.robotGoal, childBrain, bestRobotIndex, self.robotTimeLimit, self.numberOfAngleSegments)

    def decideIfRobotTimeShouldBeIncreased(self):
        timeIncrease = False
        numberOfGensToCheck = 5
        gradients = []
        sumGradient = 0
        if(self.generationsSinceLastTimeUpdate > numberOfGensToCheck):
            for i in range(numberOfGensToCheck - 1):
                gradients.append(self.bestFitnessEachGeneration[len(self.bestFitnessEachGeneration) - i - 1] - self.bestFitnessEachGeneration[len(self.bestFitnessEachGeneration) - i - 2])
                sumGradient = sumGradient + gradients[i]
            averageGradient = sumGradient / (numberOfGensToCheck - 1)

            if(averageGradient < 0.02):
                timeIncrease = True
                print("extend time true")
                self.generationsSinceLastTimeUpdate = 0

        return timeIncrease

    def increaseTime(self):
        if(self.timeLimitIncreasedCounter > 2):
            self.timeLimitIncreasedCounter = self.timeLimitIncreasedCounter + 1
            self.robotTimeLimit = self.robotTimeLimit + 5

    def checkForConvergence(self):
        systemConverged = False
        if(self.convergenceType == convergenceTypes.EPOCHS):
            if self.currentGeneration >= 600:
                print("epochs finished")
                systemConverged = True
            elif self.robotsReachedGoalSequentiallyCounter >= 5:
                print("robots reached goal consecutively 5 times")
                systemConverged = True

        elif(self.convergenceType == convergenceTypes.MIN_GRAD):
            # check if performance isnt seeming to get any better at any rate here
            pass

        elif(self.convergenceType == convergenceTypes.BEST_PERF):
            # check if best fitness is rgeater than some value here
            pass

        return systemConverged

    def clearNetsFromMemory(self):
        self.saveAllRobots()
        keras.backend.clear_session()
        self.loadSavedRobotBrain()

    def saveBestRobot(self, bestRobot):
        bestRobot.brain.model.save(self.dirname + "/best_robot/bestRobot.h5")

    def saveAllRobots(self):
        for i in range(len(self.robots)):
            self.robots[i].brain.model.save(self.dirname + "/navigation_neural_nets/robot"+ str(i) + ".h5")

    def loadSavedRobotBrain(self):
        for i in range(self.populationSize):
            loadedModel = tf.keras.models.load_model(self.dirname + "/navigation_neural_nets/robot"+ str(i) + ".h5")

            robotBrain = neuralNet(loadedModel, self.numberOfAngleSegments + 3, 5, 2)
            self.robots[i] = robot(self.robotGoal, robotBrain, i, self.robotTimeLimit, self.numberOfAngleSegments)


class robot():
    def __init__(self, goalPoint, brain, robotIndex, maxRunTimeSeconds, angleSeg):
        self.goalPoint = Point(goalPoint[0].x, goalPoint[0].y, goalPoint[0].z)
        self.startTime = 0
        self.robotIndex = robotIndex
        self.fitness = 0
        self.maxTime = maxRunTimeSeconds
        self.timeOfLastUpdate = 0
        self.secondsBetweenPointsUpdate = 1
        self.robotGotStuck = False
        # Slightly hacky solution to stop robot being temrinated on first point update
        self.pointTracking = np.array([[Point(100,100,0)], [Point(0,0,0)], [Point(10,10,0)]])
        self.printUsefulInFirstIter = False
        self.robotReachedGoal = False
        self.runTimeCounter = 0
        self.crashedCounter = 0
        self.closestDistToGoal = 1000

        if( brain ):
            self.brain = brain.copy()

        else:
            # angle segments inputs plus one heading to the goal
            # hidden nodes arbitrary number
            # output nodes is 3 - vx, vy and w
            self.brain = neuralNet(angleSeg + 3, 5, 2, None)

    def initRobotForGeneration(self):
        self.startTime = time.time()
        self.timeOfLastUpdate = time.time()

    def handleCurrentRobot(self, robotPose, flattenedObstacles):
        robotFinished = False
        robotVelocities = []

        if(self.checkIfRobotFinished(robotPose) == True):
            robotFinished = True
            self.calcFitness(robotPose)


        else:
            heading_robotToGoal = self.calcHeadingToGoal(self.goalPoint, robotPose)
            distToGoal = self.returnDistance(self.pointTracking[0, 0], self.goalPoint)
            # pass flattened obstacles and heading to goal to a neural net predict function
            # returns vx, vy and w
            robotVelocities = self.brain.chooseRobotMovement(flattenedObstacles, heading_robotToGoal, distToGoal, robotPose)
            if(self.printUsefulInFirstIter == False):
                self.printUsefulInFirstIter = True
                print("heading is: " + str(heading_robotToGoal))
                print("goal is " + str(self.goalPoint))

        return robotFinished, robotVelocities


    def incrementRunandCrashCounter(self, crashed):
        #increment general run counter
        self.runTimeCounter = self.runTimeCounter + 1
        if crashed == True:
            self.crashedCounter = self.crashedCounter + 1
            # increment crashed counter


    def checkIfRobotFinished(self, robotPose):
        robotFinished = False
        if(self.checkIfAtGoal(self.goalPoint, robotPose) == True):
            self.robotReachedGoal = True
            robotFinished = True

        if(self.timeFinished() == True):
            robotFinished = True

        if(self.checkIfRobotStuck(robotPose)):
            self.robotGotStuck = True
            robotFinished = True

        return robotFinished

    def checkIfRobotStuck(self, robotPose):
        robotStuck = False
        timeSinceLastUpdate = time.time() - self.timeOfLastUpdate
        if(timeSinceLastUpdate >= self.secondsBetweenPointsUpdate):
            self.timeOfLastUpdate = time.time()
            self.updatePointTracking(robotPose)

            d1 = self.returnDistance(self.pointTracking[1, 0], self.pointTracking[0, 0])
            d2 = self.returnDistance(self.pointTracking[2, 0], self.pointTracking[1, 0])
            d3 = self.returnDistance(self.pointTracking[2, 0], self.pointTracking[0, 0])

            if(d1 < 0.2 and d2 < 0.2 and d3 < 0.3 ):
                print("robot stuck, terminating")
                robotStuck = True

        return robotStuck

    def updatePointTracking(self, robotPose):
        self.pointTracking[2, 0].x = self.pointTracking[1, 0].x
        self.pointTracking[2, 0].y = self.pointTracking[1, 0].y
        self.pointTracking[1, 0].x = self.pointTracking[0, 0].x
        self.pointTracking[1, 0].y = self.pointTracking[0, 0].y
        self.pointTracking[0, 0].x = robotPose.pose.pose.position.x
        self.pointTracking[0, 0].y = robotPose.pose.pose.position.y

    def calcFitness(self, robotPose):
        endDistToGoal = self.calcRobotDistToPoint(self.goalPoint, robotPose)
        bestDistToGoal = self.closestDistToGoal
        originPoint = Point(0, 0, 0)
        timePassed = time.time() - self.startTime
        #distToOrigin = self.calcHeadingToGoal(originPoint, robotPose)
        timeCrashed = (self.crashedCounter / self.runTimeCounter)
        if( self.robotReachedGoal == True):
            distToGoal = 0
            bestDistToGoal = 0

        self.fitness = (10 / (endDistToGoal + 1)) + (5 / (bestDistToGoal + 1))
        #self.fitness = (10 / (distToGoal + 1)) # + (1 / (timePassed + 1))
        # self.fitness = self.fitness * (1 - timeCrashed)
        #
        # if(self.robotGotStuck == True):
        #     self.fitness = self.fitness / 2

        print("fitness is: " + str(self.fitness))

    def calcHeadingToGoal(self, goalPoint, robotPose):
        changeInX = goalPoint.x - robotPose.pose.pose.position.x
        changeInY = goalPoint.y - robotPose.pose.pose.position.y
        theta_robotToGoal = -math.atan2(changeInY, changeInX)            # returns a global headin betwen -Pi and Pi

        return theta_robotToGoal

    def timeFinished(self):
        currentTime = time.time()

        if currentTime - self.startTime > self.maxTime:
            return True
        else:
            return False

    def returnDistance(self, point1, point2):
        changeInX = point1.x - point2.x
        changeInY = point1.y - point2.y
        distance = math.sqrt(math.pow(changeInX, 2) + math.pow(changeInY, 2))

        return distance

    def calcRobotDistToPoint(self, point, robotPose):
        robotPoint = Point(robotPose.pose.pose.position.x, robotPose.pose.pose.position.y, 0)
        distance = self.returnDistance(point, robotPoint)
        return distance

    def checkIfAtGoal(self, goalPoint, robotPose):
        distance = self.calcRobotDistToPoint(goalPoint, robotPose)
        if( distance < self.closestDistToGoal):
            self.closestDistToGoal = distance
        if distance < 0.50:
            print("goal reached ")
            return True
        else:
            return False

class neuralNet():
    def __init__(self, a, b, c, d):

        if(isinstance(a, keras.Sequential)):
            self.model = a
            self.inputNodes = b
            self.hiddenNodes1 = c
            self.outputNodes = d
        else:
            self.inputNodes = a
            self.hiddenNodes1 = b
            self.outputNodes = c
            self.model = self.createModel()


    def copy(self):
        modelCopy = self.createModel()
        modelCopy.set_weights(self.model.get_weights())
        return neuralNet(modelCopy, self.inputNodes, self.hiddenNodes1, self.outputNodes)

    def crossover(self, partner):
        newModel = self.createModel()
        #target_model.set_weights(model.get_weights())
        weights1 = self.model.get_weights()
        weights2 = partner.model.get_weights()
        childWeights = weights1 #copy format of the weights


        # weights has 4 arrays in it, first is 361 by 100 weights from input to hhidden layer
        # second array is bias to the hidden layer ( 100 elements all zero )
        # third is hidden to output layer 100 by 3
        # fourht is bias to the output layer 3 elements
        # we need to loop through and combine the two parent neural netowrks for each weight
        for k in range(len(weights1)):
            # arrays 0 and 2 have different shapes to 1 and 3
            if(k == 0 or k == 2):
                for i in range(len(weights1[k])):
                    for j in range(len(weights1[k][0])):
                            tensor1 = weights1[k][i, j]
                            tensor2 = weights2[k][i, j]
                            newValue = (tensor1 + tensor2) / 2
                            childWeights[k][i, j] = newValue
            else:
                pass

        newModel.set_weights(childWeights)
        return neuralNet(newModel, self.inputNodes, self.hiddenNodes1, self.outputNodes)

    def mutateAfterCrossover(self, chance):
        weights = self.model.get_weights()
        mutatedWeights = weights
        for k in range(len(weights)):
            if(k == 0 or k == 2):
                for i in range(len(weights[k])):
                    for j in range(len(weights[k][0])):
                        if(random.uniform(0, 1) < chance):
                            value = weights[k][i, j]
                            # mess around with standard deviation value
                            mutatedWeights[k][i, j] = value + random.gauss(0, 1)

        self.model.set_weights(mutatedWeights)

    def mutateNoCrossover(self, chance):
        newModel = self.createModel()
        weights = self.model.get_weights()
        #print(weights)
        mutatedWeights = weights
        for k in range(len(weights)):
            # these are the weights between layers
            if(k == 0 or k == 2):
                for i in range(len(weights[k])):
                    for j in range(len(weights[k][0])):
                        if(random.uniform(0, 1) < chance):
                            value = weights[k][i, j]
                            # mess around with standard deviation value
                            mutatedWeights[k][i, j] = value + random.gauss(0, 1)
            # these are the biases to the nodes
            if(k == 1 or k == 3):
                for i in range(len(weights[k])):
                    if(random.uniform(0, 1) < chance):
                        value = weights[k][i]
                        mutatedWeights[k][i] = value + random.gauss(0, 1)

        newModel.set_weights(mutatedWeights)
        return neuralNet(newModel, self.inputNodes, self.hiddenNodes1, self.outputNodes)

    def createModel(self):
        model = keras.Sequential()
        model.add(keras.layers.Dense(self.hiddenNodes1, input_dim=self.inputNodes, activation = 'relu'))
        model.add(keras.layers.Dense(self.outputNodes, activation = 'sigmoid'))

        return model

    def chooseRobotMovement(self, flattenedObstacles, headingToGoal, distToGoal, robotPose):
        inputs = flattenedObstacles
        #print("length of flattened obstacles this iteration: " + str(len(inputs)))

        inputs.append(headingToGoal)
        robotOrientation = robotPose.pose.pose
        robotQuaternionList = [robotOrientation.orientation.x, robotOrientation.orientation.y, robotOrientation.orientation.z, robotOrientation.orientation.w]
        (robotRoll, robotPitch, robotYaw) = euler_from_quaternion (robotQuaternionList)
        inputs.append(robotYaw)
        inputs.append(distToGoal)

        xs = np.array([inputs]);
        robotVelocities = self.model.predict(xs);

        return robotVelocities

rospy.init_node('genetic_training', anonymous=True)
robotPop = robotPopulation(False, convergenceTypes.EPOCHS, 16)

def shutDown():
    global robotPop
    robotPop.saveAllRobots()
    robotPop.stopRobot()
    robotPop.resetRobotPosition()
    print("shutdown time!")


def main():
    #Code begins - program what happens on code exit
    rospy.on_shutdown(shutDown)
    global robotPop

    while not rospy.is_shutdown():
        robotPop.handlePopulation()

if __name__ == '__main__':
    main()
