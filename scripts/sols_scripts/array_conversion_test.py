#!/usr/bin/env python
import numpy as np
import rospy  # rospy library
from nav_msgs.msg import OccupancyGrid
import matplotlib.pyplot as plt

gmap = []

def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap

    #get size of map occupancy grid in rows and columns
    map_columns = msg.info.width
    map_rows = msg.info.height

    #get map resolution
    map_res = msg.info.resolution

    #get coordinates of gmap cell[0,0]
    map_origin = msg.info.origin.position

    #get occupancy grid map
    gmap = msg.data

    gmap = np.array(gmap)

    gmap = gmap.reshape(map_rows, map_columns)
    #gmap.shape(map_rows, map_columns)


def main():
    global gmap, map_rows, map_columns

    rospy.init_node('array_conversion_test')

    sub_map = rospy.Subscriber('/map', OccupancyGrid, clbk_map)

    rate = rospy.Rate(0.4)
    while not rospy.is_shutdown():
        rate.sleep()
        #print(gmap.shape)
        #print(gmap.shape)
        #print(map_rows)
        #print(map_columns)

        plt.imshow(gmap, interpolation='none')
        plt.gca().invert_yaxis()
        plt.show()

if __name__ == '__main__':
    main()
