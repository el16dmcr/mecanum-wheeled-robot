#!/usr/bin/env python

import rospy  # rospy library
from local_path_planning.create_obstacle_map_from_sensors import \
    flattenLidar2DMap  # class to process lidar point clouds
from sensor_msgs.msg import PointCloud2  # message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2  # class to read data from /UGV/laser/scan topic messages
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from tf import transformations
import math


# set goal point
desired_position = Point()
desired_position.x = 0
desired_position.y = 15
desired_position.z = 0

# set precisions for distance abd yaw
yaw_precision = 5 * math.pi / 180
dist_precision = 0.3

#initialise robot position variables
Position = Point()
Yaw = 0

#create velocity publisher
pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

# initialise lidar reading class
numberAngleSegments = 100
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

# initialise safe distance
d = 1

# initialise dictionary to contain min distances to objects around robot
regions = {
        'front': d,
        'fright': d,
        'right': d,
        'left': d,
        'fleft': d,
    }

# initialise state of bug behaviour
State = 0
#State_dict = {
    #0: 'Go to point',
    #1: 'Wall following'
#}

# initialise state of go to point behaviour
State_gtp = 0
#State_dict_gtp = {
    #0: 'Fix yaw',
    #1: 'Proceed to goal',
    #2: 'Stop'
#}

# initialise state of wall following behaviour
State_wf = 0
#State_dict_wf = {
    #0: 'Find wall',
    #1: 'Turn left',
    #2: 'Follow wall'
#}

#initialise variable to store distance to goal each time an obstacle is encountered
goal_dist_hit = None

#initialise variables for tracking bug0 states (to enable debugging)
current_state = 0  # tracks current state of bug0 without affecting code execution
first_run = True  # tracks whether or not its the first run of go to point behaviour
robot_stopped = False # tracks whether goal point has been reached

##################################################### CALLBACKS ########################################################

def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position, Yaw

    # get position from odometry message
    Position = msg.pose.pose.position

    # determine yaw from odometry message
    quaternion = (msg.pose.pose.orientation.x,
                  msg.pose.pose.orientation.y,
                  msg.pose.pose.orientation.z,
                  msg.pose.pose.orientation.w)
    euler = transformations.euler_from_quaternion(quaternion)
    Yaw = euler[2]

def clbk_laser(msg):
    # returns flat distance to nearest obstacle in each region around robot (in regions) each time lidar message is received
    global regions, flattenNearestObstacles

    # read in lidar mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system - rear CW)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc)  # so-called 2D map

    # 360/8 = 45
    # define regions of robot using 2D map via a dictionary using 45 degree segments
    regions = {
        'front': min(dists[45:54]),
        'fright': min(dists[55:64]),
        'right': min(dists[65:74]),
        'left': min(dists[25:34]),
        'fleft': min(dists[35:44]),
    }


########################################################################################################################

################################################### GENERAL METHODS ####################################################

def normalize_angle(angle):
    # normalises an angle to between -pi and pi so that the shortest turn can be taken to reach desired heading
    normalized_angle = angle

    if math.fabs(angle) > math.pi:
        normalized_angle = angle - (2 * math.pi * angle) / math.fabs(angle)

    return normalized_angle


def stopRobot():
    global pub
    # stops the robot
    msg = Twist() #zero velocity message
    pub.publish(msg)


def change_state(state):
    # changes states within bug behaviour
    global State
    State = state

########################################################################################################################

########################################## METHODS FOR GO TO POINT BEHAVIOUR ###########################################

def change_state_gtp(state):
    # changes states within go to point behaviour
    global State_gtp
    State_gtp = state


def fix_yaw():
    # points the robot to face towards the goal point
    global Yaw, Position, State_gtp, yaw_precision, desired_position, pub

    # find desired yaw and current yaw error
    desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
    err_yaw = desired_yaw - Yaw
    err_yaw_norm = normalize_angle(err_yaw)

    # publish angular velocity to correct heading if error too big
    if math.fabs(err_yaw_norm) > yaw_precision:
        twist_msg = Twist()

        # if err_yaw > 0 must turn left (CCW) to face goal and vice versa
        if err_yaw_norm > 0:
            twist_msg.angular.z = 0.7
        else:
            twist_msg.angular.z = -0.7

        pub.publish(twist_msg)

    # change state to go straight towards goal if/when yaw is okay
    if math.fabs(err_yaw_norm) <= yaw_precision:
        change_state_gtp(1)


def go_straight():
    # moves the robot directly towards the goal point
    global Yaw, Position, State_gtp, dist_precision, yaw_precision, desired_position, pub

    # find desired yaw and current yaw error
    desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
    err_yaw = desired_yaw - Yaw

    # find position error (distance to goal)
    pos_error = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

    # if robot needs to travel towards goal then publish a forward velocity
    if pos_error > dist_precision:
        twist_msg = Twist()
        twist_msg.linear.x = 0.6

        pub.publish(twist_msg)

    # change state to stop once goal is reached
    if pos_error <= dist_precision:
        change_state_gtp(2)

    # change state to fix heading if needed
    if math.fabs(err_yaw) > yaw_precision:
        change_state_gtp(0)


def go_to_point():
    # implements go to point behaviour by switching between fixing heading and proceeding straight towards goal point
    global desired_position, first_run, current_state, State_gtp, robot_stopped, regions, dist_precision, d

    #signal each time go to point bug0 state is triggered
    if first_run == True:
        first_run = False
        print('Going to point')
        current_state = 0

    if first_run == False and current_state == 1:
        current_state = 0
        print('Going to point')

    #implement go to point behaviour
    if State_gtp == 0:
        fix_yaw()
        # print('Fixing yaw')
    elif State_gtp == 1:
        go_straight()
        # print('Proceeding to goal')
    elif State_gtp == 2:
        # print('Goal reached!')
        stopRobot()
        if robot_stopped == False:
            robot_stopped = True
            print('Robot Stopped')
    else:
        print('Go to point - Unknown State!')
        rospy.loginfo(State_gtp)

    # find desired yaw and current yaw error
    desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
    err_yaw = desired_yaw - Yaw
    # find yaw error between -pi and pi
    err_yaw_norm = normalize_angle(err_yaw)

    #if pointing towards goal and an obstacle is encountered, switch to wall following behaviour
    #print(regions['front'])
    if regions['front'] < d and err_yaw_norm <= yaw_precision:
        change_state(1)
    if regions['fleft'] < d and err_yaw_norm <= yaw_precision:
        change_state(1)
    if regions['fright'] < d and err_yaw_norm <= yaw_precision:
        change_state(1)

########################################################################################################################

####################################### METHODS FOR WALL FOLLOWING BEHAVIOUR ###########################################

def change_state_wf(state):
    # changes states within wall following behaviour
    global State_wf
    State_wf = state


def decide_wf_state():
    global regions, d, State_wf

    # locates obstacles relative to robot by finding which regions are currently occupied at less than safe distance
    # then dictates which state wall follower should be in to follow obstacle wall

    # d is safe wall following distance defined globally

    # decide how wall follower should behave depending on location of obstacles around robot
    # 0 = find wall (move right in an arc), 1 = turn left, 2 = move forward

    #now basically: 1 = should progress, 0 = should turn
    #progression towards or away from obstacle determined by distance to wall
    #if regions right > d*1.05 then turn towards wall
    #if regions right < d*0.95 then turn away from wall
    #if regions right < d*1.05 and > d*0.95 move straight
    #Hence, overall: 0 = move towards wall, 1 = turn left, 2 = move away from wall, 3 = move straight along wall

    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        change_state_wf(0)  # could try state 2 (follow wall) here
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        change_state_wf(1) #was 2
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        change_state_wf(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        change_state_wf(1)
    else:
        state_description = 'Unknown Obstacle Case'
        print(state_description)
        rospy.loginfo(regions)

    if State_wf == 0:
        if regions['right'] < d*0.975:
            change_state_wf(2)
            state_description = 'Too close to wall - must move away'

        elif regions['right'] >= d*0.975 and regions['right'] <= d*1.025:
            change_state_wf(3)
            state_description = 'Good distance to wall - move straight'

        else:
            state_description = 'Too far from wall - move towards it'

    #now need a controller: when wall too far away (regions right > d) turn right in proportion to distance away from d,
    #when too close (regions right < d) turn left in proportion to distance away from d

    #print(state_description)


def find_wall():
    # moves robot in right arc to find obstacle wall
    global pub

    msg = Twist()
    msg.linear.x = 0.35 #0.4
    msg.angular.z = -0.35 #-0.3
    pub.publish(msg)


def move_away():
    # moves robot in right arc to find obstacle wall
    global pub

    msg = Twist()
    msg.linear.x = 0.35 #0.4
    msg.angular.z = 0.35 #-0.3
    pub.publish(msg)


def turn_left():
    # turns robot left to keep obstacle wall on right side
    global pub

    msg = Twist()
    msg.angular.z = 0.7
    pub.publish(msg)


def follow_wall():
    # moves robot straight forwards to follow obstacle wall
    global pub

    msg = Twist()
    msg.linear.x = 0.35
    pub.publish(msg)


def wall_following():
    # implements wall following behaviour by switching states accordingly between find wall, turn left and follow wall (move straight)
    global State_wf, current_state, Position, desired_position, goal_dist_hit, robot_stopped, dist_precision, d

    #update wall follower state according to current environment
    decide_wf_state()

    #signal each time wall following bug0 state is triggered
    if current_state == 0:
        current_state = 1
        print('Following obstacle wall')

        # find distance to goal when wall following entered
        goal_dist_hit = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5
        print('Goal distance when obstacle encountered: ' + str(goal_dist_hit))

    #implement wall following behaviour
    if State_wf == 0:
        find_wall()
    elif State_wf == 1:
        turn_left()
    elif State_wf == 2:
        move_away()
    elif State_wf == 3:
        follow_wall()
    else:
        print('Wall following - Unknown State!')
        rospy.loginfo(State_wf)

    # find position error (distance to goal)
    pos_error = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

    #stop if goal reached
    if pos_error <= dist_precision:
        stopRobot()
        if robot_stopped == False:
            robot_stopped = True
            print('Robot Stopped')

    # find distance to goal during wall following
    goal_dist = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

    #if goal current goal distance is less than at last hit point by at least 2% then check to exit wall following
    # (now as long as robot is closer to goal by at least length of robot it can check to exit wall following)
    if goal_dist < (goal_dist_hit - 0.6):
        # find desired yaw and current yaw error
        desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
        err_yaw = desired_yaw - Yaw
        #find current normalised yaw error
        err_yaw_norm = normalize_angle(err_yaw)

        # if err_yaw_norm > 0 must turn left (CCW), if err_yaw_norm < 0 must turn right (CW) to face goal

        free_distance = d*1.25

        # go to point if no obstacle ahead, goal is on left of robot, and robot must turn less than 45 degrees to face goal
        if regions['front'] > free_distance and (err_yaw_norm <= 18*(math.pi / 180) and err_yaw_norm > -18*(math.pi / 180)):
            change_state(0)
            print('Space forward')
            print('Yaw error: ' + str(err_yaw_norm))
            print('Goal distance when leaving obstacle: ' + str(goal_dist))
            print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
        #go to point if no obstacle to left side, goal is on left of robot, and robot must turn by between 45 and 90 degrees to face goal
        if regions['left'] > free_distance and (err_yaw_norm > 54*(math.pi / 180) and err_yaw_norm <= 90*(math.pi / 180)):
            change_state(0)
            print('Space left')
            print('Yaw error: ' + str(err_yaw_norm))
            print('Goal distance when leaving obstacle: ' + str(goal_dist))
            print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
        # go to point if no obstacle to right side, goal is on right of robot, and robot must turn by between 45 and 90 degrees to face goal
        if regions['right'] > free_distance and (err_yaw_norm <= -54*(math.pi / 180) and err_yaw_norm > -90*(math.pi / 180)):
            change_state(0)
            print('Space right')
            print('Yaw error: ' + str(err_yaw_norm))
            print('Goal distance when leaving obstacle: ' + str(goal_dist))
            print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
        if regions['fleft'] > free_distance and (err_yaw_norm > 18*(math.pi / 180) and err_yaw_norm <= 54*(math.pi / 180)):
            change_state(0)
            print('Space front left')
            print('Yaw error: ' + str(err_yaw_norm))
            print('Goal distance when leaving obstacle: ' + str(goal_dist))
            print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
        if regions['fright'] > free_distance and (err_yaw_norm <= -18*(math.pi / 180) and err_yaw_norm > -54*(math.pi / 180)):
            change_state(0)
            print('Space front right')
            print('Yaw error: ' + str(err_yaw_norm))
            print('Goal distance when leaving obstacle: ' + str(goal_dist))
            print('Closer to goal by ' + str(goal_dist_hit - goal_dist))

########################################################################################################################

def main():
    global State

    rospy.init_node('bug0_new')

    #subscribe to odometry and lidar scanner topics
    rospy.Subscriber('/odom', Odometry, clbk_odom)
    rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk_laser)

    # line to stop robot if node is shutdown during operation
    rospy.on_shutdown(stopRobot)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():

        if State == 0:
            # initiate go to point behaviour
            go_to_point()

        elif State == 1:
            # initiate wall following behaviour
            wall_following()

        rate.sleep()


if __name__ == '__main__':
    main()
