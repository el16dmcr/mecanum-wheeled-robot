#!/usr/bin/env python

import rospy  # rospy library
from local_path_planning.create_obstacle_map_from_sensors import \
    flattenLidar2DMap  # class to process lidar point clouds
from sensor_msgs.msg import PointCloud2  # message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2  # class to read data from /UGV/laser/scan topic messages
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from tf import transformations
import math
import time

# initialise state stuff for bug behaviour
State = 0
State_dict = {
    0: 'Go to point',
    1: 'Wall following'
}

# initialise state stuff for wall following behaviour
State_wf = 0
State_dict_wf = {
    0: 'Find wall',
    1: 'Turn left',
    2: 'Follow wall'
}

# initialise state stuff for go to point behaviour
State_gtp = 0
State_dict_gtp = {
    0: 'Fix yaw',
    1: 'Proceed to goal',
    2: 'Stop'
}

#initialise robot position variables
Position = Point()
Yaw = 0
desired_yaw = 0
err_yaw = 0

# set goal point
desired_position = Point()
desired_position.x = 5
desired_position.y = 0
desired_position.z = 0

# set precisions
yaw_precision = 2 * math.pi / 180
dist_precision = 0.3

# initialise publisher
pub = None

# initialise dictionary to contain min distances to object around robot
regions = None

# initialise lidar class
numberAngleSegments = 360
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

#initialise variables for tracking bug0 states
emergency_avoiding = False  # tracks whether or not in emergency avoidance state
current_state = 0  # tracks current state of bug0 without affecting code execution
first_run = True  # tracks whether or not its the first run of go to point behaviour
robot_stopped = False # tracks whether goal point has been reached

#initialise variable to store distance to goal each time an obstacle is encountered
goal_dist_hit = None

# methods for go to point behaviour
def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position, Yaw, desired_yaw, err_yaw

    # get position from odometry message
    Position = msg.pose.pose.position

    # determine yaw from odometry message
    quaternion = (msg.pose.pose.orientation.x,
                  msg.pose.pose.orientation.y,
                  msg.pose.pose.orientation.z,
                  msg.pose.pose.orientation.w)
    euler = transformations.euler_from_quaternion(quaternion)
    Yaw = euler[2]

    #find desired yaw and current yaw error
    desired_yaw = math.atan2(desired_position.y - Position.y, desired_position.x - Position.x)
    err_yaw = desired_yaw - Yaw

def fix_yaw(des_pos):
    # points the robot to face towards the goal point
    global Yaw, Position, pub, State_gtp, yaw_precision, desired_yaw, err_yaw

    # print('Go to point - Fixing heading')

    # publish angular velocity to correct heading if error too big
    twist_msg = Twist()
    if math.fabs(err_yaw) > yaw_precision:
        twist_msg.angular.z = 0.7 if err_yaw > 0 else -0.7  # if err_yaw > 0 must turn left (CCW) to face goal and vice versa

        pub.publish(twist_msg)

    # change state to go straight towards goal if/when yaw is okay
    if math.fabs(err_yaw) <= yaw_precision:
        change_state_gtp(1)

def change_state_gtp(state):
    # changes states within go to point behaviour
    global State_gtp
    State_gtp = state

def go_straight(des_pos):
    # moves the robot directly towards the goal point
    global Yaw, Position, pub, State_gtp, dist_precision, yaw_precision, desired_yaw, err_yaw

    # print('Go to point - Proceeding to goal')

    # find position error (distance to goal)
    pos_error = ((des_pos.y - Position.y) ** 2 + (des_pos.x - Position.x) ** 2) ** 0.5

    # if robot needs to travel towards goal then publish a forward velocity
    twist_msg = Twist()
    if pos_error > dist_precision:
        twist_msg.linear.x = 0.6

        pub.publish(twist_msg)

    # change state to stop once goal is reached
    if pos_error <= dist_precision:
        change_state_gtp(2)

    # change state to fix heading if needed
    if math.fabs(err_yaw) > yaw_precision:
        change_state_gtp(0)

def go_to_point():
    # implements go to point behaviour by switching between fixing heading and proceeding straight towards goal point
    global desired_position, State, first_run, current_state, State_gtp, robot_stopped


    #signal each time go to point bug0 state is triggered
    if first_run == True:
        first_run = False
        print('Going to point')
        current_state = 0

    if first_run == False and current_state == 1:
        current_state = 0
        print('Going to point')

    #implement go to point behaviour
    if State_gtp == 0:
        fix_yaw(desired_position)
        # print('Fixing yaw')
    elif State_gtp == 1:
        go_straight(desired_position)
        # print('Proceeding to goal')
    elif State_gtp == 2:
        # print('Goal reached!')
        stopRobot()
        if robot_stopped == False:
            robot_stopped = True
            print('Robot Stopped')
    else:
        print('Go to point - Unknown State!')
        rospy.loginfo(State_gtp)

    #switch to wall following behaviour if an obstacle blocks direct path to goal

    # find current normalised yaw error
    err_yaw_norm = normalize_angle(err_yaw)

    # if pointing towards goal and obstacle in way then change bug0 state to wall following
    if math.fabs(err_yaw_norm) < yaw_precision and regions['front'] < 1.5:
        State = 1

# methods for wall following behaviour
def clbk_laser(msg):
    # returns flat distance to nearest obstacle in each region around robot (in regions) each time lidar message is received
    global regions, flattenNearestObstacles

    # read in lidar mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system - rear CW)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc)  # so-called 2D map

    # 360/8 = 45
    # define regions of robot using 2D map via a dictionary using 45 degree segments
    regions = {
        'front': min(dists[158:202]),
        'fright': min(dists[203:247]),
        'right': min(dists[248:292]),
        'rright': min(dists[293:337]),
        'rear': min(min(dists[338:360]), min(dists[0:22])),
        'rleft': min(dists[23:67]),
        'left': min(dists[68:112]),
        'fleft': min(dists[113:157]),
        'front180': min(dists[90:270])
    }
    # 'front': min(dists[158:202]),
    # 'fright': min(dists[203:247]),
    # 'right': min(dists[248:292]),
    # 'rright': min(dists[293:337]),
    # 'rear': min(min(dists[338:360]), min(dists[0:22])),
    # 'rleft': min(dists[23:67]),
    # 'left': min(dists[68:112]),
    # 'fleft': min(dists[113:157])
    # }

    # 'front': min(dists[175:185]),
    # 'fright': min(dists[186:234]),
    # 'right': min(dists[235:270]),
    # 'left': min(dists[91:126]),
    # 'fleft': min(dists[127:174])

    # use state of regions to identify where obstacles are relative to robot and take corresponding wall following action
    take_wf_action(regions)

def take_wf_action(regions):
    # locates obstacles relative to robot by finding which regions are currently occupied at less than safe distance
    # then dictates which action robot should take to follow obstacle wall

    d = 1.5  # safe distance

    # decide how wall follower should behave depending on location of obstacles around robot
    # 0 = find wall (move right in an arc), 1 = turn left, 2 = move forward
    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        change_state_wf(0)  # could try state 2 (follow wall) here
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        change_state_wf(2)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        change_state_wf(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        change_state_wf(1)
    else:
        state_description = 'Unknown Case'
        rospy.loginfo(regions)

    # rospy.loginfo(state_description)
    # print(regions['fleft'], regions['front'], regions['fright'])

def change_state_wf(state):
    # changes states within wall following behaviour
    global State_wf, State_dict_wf
    State_wf = state
    # if State_wf == 0:
    #    #print('Wall following - Find wall')
    # elif State_wf == 1:
    #    #print('Wall following - Turn left')
    # elif State_wf == 2:
    #    #print('Wall following - Follow wall')
    # else:
    #    #print('Wall following - Unknown state!')

def find_wall():
    # moves robot in right arc to find obstacle wall
    global pub

    # print('Wall following - finding wall')
    msg = Twist()
    msg.linear.x = 0.5 #0.4
    msg.angular.z = -0.4 #-0.3
    pub.publish(msg)
    # return msg

def turn_left():
    # turns robot left to keep obstacle wall on right side
    global pub

    # print('Wall following - turning left')
    msg = Twist()
    msg.angular.z = 0.6
    pub.publish(msg)
    # return msg

def follow_wall():
    # moves robot straight forwards to follow obstacle wall
    global pub

    # print('Wall following - following wall')
    msg = Twist()
    msg.linear.x = 0.6
    pub.publish(msg)
    # return msg

def wall_following():
    # implements wall following behaviour by switching states accordingly between find wall, turn left and follow wall (move straight)
    global pub, State_wf, State_dict_wf, State, current_state, Position, desired_position, goal_dist_hit

    #signal each time wall following bug0 state is triggered
    if current_state == 0:
        current_state = 1
        print('Following obstacle wall')

        # find distance to goal when wall following entered
        goal_dist_hit = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5
        print('Goal distance when obstacle encountered: ' + str(goal_dist_hit))

    #implement wall following behaviour
    if State_wf == 0:
        find_wall()
    elif State_wf == 1:
        turn_left()
    elif State_wf == 2:
        follow_wall()
    else:
        print('Wall following - Unknown State!')
        rospy.loginfo(State_wf)

    #find current normalised yaw error
    err_yaw_norm = normalize_angle(err_yaw)

    # if err_yaw_norm > 0 must turn left (CCW), if err_yaw_norm < 0 must turn right (CW) to face goal

    # find distance to goal during wall following
    goal_dist = ((desired_position.y - Position.y) ** 2 + (desired_position.x - Position.x) ** 2) ** 0.5

    # as long as robot is closer to goal by at least 2% it can check to exit wall following

    # go to point if no obstacle ahead, goal is on left of robot, and robot must turn less than 45 degrees to face goal
    if regions['front'] > 1.5 and err_yaw_norm > 0 and math.fabs(err_yaw) < (math.pi / 4) and goal_dist < 0.98*goal_dist_hit:
        State = 0
        print('Space forward')
        print('Yaw error: ' + str(err_yaw_norm))
        print('Goal distance when leaving obstacle: ' + str(goal_dist))
        print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
    # go to point if no obstacle to left side, goal is on left of robot, and robot must turn by between 45 and 90 degrees to face goal
    if regions['left'] > 1.5 and err_yaw_norm > 0 and math.fabs(err_yaw) > (math.pi / 4) and \
            math.fabs(err_yaw) < (math.pi / 2) and goal_dist < 0.98*goal_dist_hit:
        State = 0
        print('Space left')
        print('Yaw error: ' + str(err_yaw_norm))
        print('Goal distance when leaving obstacle: ' + str(goal_dist))
        print('Closer to goal by ' + str(goal_dist_hit - goal_dist))
    # go to point if no obstacle to right side, goal is on right of robot, and robot must turn by between 45 and 90 degrees to face goal
    if regions['right'] > 1.5 and err_yaw_norm < 0 and math.fabs(err_yaw) > (math.pi / 4) and \
            math.fabs(err_yaw) < (math.pi / 2) and goal_dist < 0.98*goal_dist_hit:
        State = 0
        print('Space right')
        print('Yaw error: ' + str(err_yaw_norm))
        print('Goal distance when leaving obstacle: ' + str(goal_dist))
        print('Closer to goal by ' + str(goal_dist_hit - goal_dist))

# method to stop robot
def stopRobot():
    # stops the robot
    global pub
    msg = Twist()
    msg.angular.x = 0
    msg.angular.y = 0
    msg.angular.z = 0
    msg.linear.x = 0
    msg.linear.y = 0
    msg.linear.z = 0
    pub.publish(msg)

# method to ensure an angle is expressed between -pi and pi
def normalize_angle(angle):
    # normalises an anle to between -pi and pi so that the shortest turn can be taken to reach desired heading
    if math.fabs(angle) > math.pi:
        angle = angle - (2 * math.pi * angle) / math.fabs(angle)
    return angle

# method for emergency obstacle avoidance if obstacles are too close
def emergency_avoidance():
    global emergency_avoiding, State, current_state
    if emergency_avoiding == False:
        emergency_avoiding = True
        print('Emergency avoidance!')

    turn_left()  # turn left until facing away from obstacles that are too close
    State = 1  # exit emergency avoidance into wall following bug0 state
    current_state = 0 #put back to default (so if robot enters emergency avoidance from wall following it will inform us when wall following again begins


# main script to implement bug0
def main():
    # implements bug0 behaviour by switching between go to point and wall following behaviour accordingly
    global State, regions, pub, emergency_avoiding

    rospy.init_node('bug0')

    # define subsciber to receive odometry data
    # when new messages are receuved, clbk_odom is invoked to update values of "Position" and "Yaw"
    sub_odom = rospy.Subscriber('/odom', Odometry, clbk_odom)

    # define subscriber to receive point cloud data from lidar
    # when new messages are received, clbk_laser is invoked with the message as the first argument
    sub_laser = rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk_laser)

    # define publisher to publish robot velocities
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    # line to stop robot if node is shutdown during operation
    rospy.on_shutdown(stopRobot)

    # pause code to give chance to populate certain global variables from ROS messages
    time.sleep(1)  # stops for 1 second

    # rate in Hz for main bug0 loop to run at
    rate = rospy.Rate(20)

    print('Bug 0 running')
    while not rospy.is_shutdown():
        # as long as no obstacles are too close, initiate normal bug0
        if regions['front180'] > 0.5:

            if emergency_avoiding == True:
                emergency_avoiding = False
                print('Emergency Avoided... (For now!)')

            if State == 0:
                # initiate go to point behaviour
                go_to_point()

            elif State == 1:
                # initiate wall following behaviour
                wall_following()

        # if obstacles are too close initiate emergency avoidance - this turns left until facing away from obstacles that are too close
        else:
            emergency_avoidance()

    rate.sleep()


if __name__ == '__main__':
    main()
