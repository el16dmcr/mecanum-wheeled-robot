#!/usr/bin/env python
import numpy as np
import rospy  # rospy library
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from Queue import Queue
from nav_msgs.msg import OccupancyGrid
import matplotlib.pyplot as plt
import matplotlib as mpl

#initialise global variables
map_rows = None
map_columns = None
map_res = None
map_origin = None
gmap =[]
Position = Point()
find_new_frontiers = True
map_open = []
map_closed = []
frontier_open = []
frontier_closed = []


def clbk_odom(msg):
    # sets global values of yaw and position each time odometry message is received
    global Position, find_new_frontiers

    if find_new_frontiers == True:
        # get position of robot from odometry message
        Position = msg.pose.pose.position


def clbk_map(msg):
    global map_rows, map_columns, map_res, map_origin, gmap, find_new_frontiers

    if find_new_frontiers == True:
        # get size of map occupancy grid in rows and columns
        map_columns = msg.info.width
        map_rows = msg.info.height

        # get map resolution
        map_res = msg.info.resolution

        # get coordinates of gmap cell[0,0]
        map_origin = msg.info.origin.position

        # get occupancy grid map
        gmap = msg.data

        gmap = np.array(gmap)

        gmap = gmap.reshape(map_rows, map_columns)

        find_new_frontiers = False

def is_valid_cell(cell):
    global map_rows, map_columns

    #returns whether or not cell indices are valid

    r = cell[0]
    c = cell[1]

    if r < 0 or c < 0:
        return False
    elif r > map_rows or c > map_columns:
        return False
    else:
        return True

def is_frontier_point(cell):
    global gmap
    #returns whether cell is a frontier point or not

    # default is false
    is_frontier_point = False

    #get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0]
    dcol = [0, 1, 0, -1]

    # cycle through adjacent cells, checking for if boundary between open and unknown is crossed
    for i in range(4):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[r][c] == -1 and gmap[adj_r][adj_c] == 0:
            #if cell is unknown and an adjacent cell is open space then it is a frontier point
            is_frontier_point = True

    #return whether cell is a frontier point
    return is_frontier_point

def is_map_open(cell):
    global map_open

    #returns whether cell is map-open or not

    if map_open[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_map_closed(cell):
    global map_closed

    # returns whether cell is map-closed or not

    if map_closed[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_frontier_open(cell):
    global frontier_open

    # returns whether cell is frontier-open or not

    if frontier_open[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def is_frontier_closed(cell):
    global frontier_closed

    # returns whether cell is frontier-closed or not

    if frontier_closed[cell[0]][cell[1]] == True:
        return True
    else:
        return False

def mark(cell,marker):
    global map_open, map_closed, frontier_open, frontier_closed

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    #mark cell in appropriate array according to marker
    if marker == "map_open":
        map_open[r][c] = True
    elif marker == "map_closed":
        map_closed[r][c] = True
    elif marker == "frontier_open":
        frontier_open[r][c] = True
    elif marker == "frontier_closed":
        frontier_closed[r][c] = True
    else:
        print("Error in mark function marker input!")


def queue_f_adjacent(cell, queue):
    #adds adjacent points to frontier queue if they're not already in it or otherwise explored

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0]
    dcol = [0, 1, 0, -1]

    # cycle through adjacent cells, checking for frontier points
    for i in range(4):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        w = [adj_r,adj_c]

        if is_valid_cell(w) == False:
            continue

        if is_frontier_open(w) == False and is_frontier_closed(w) == False and is_map_closed(w) == False:
            queue.put(w)
            mark(w,"frontier_open")

def queue_m_adjacent(cell, queue):
    #adds adjacent points to frontier queue if they're not already in it or otherwise explored

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0]
    dcol = [0, 1, 0, -1]

    # cycle through adjacent cells, checking for frontier points
    for i in range(4):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        v = [adj_r,adj_c]

        if is_valid_cell(v) == False:
            continue

        if is_map_open(v) == False and is_map_closed(v) == False and has_open_space_neighbour(v) == True:
            queue.put(v)
            mark(v,"map_open")

def has_open_space_neighbour(cell):
    global gmap
    #returns whether cell has open space neighbours or not

    #default is false
    has_open_neighbour = False

    # get row and column indices of cell
    r = cell[0]
    c = cell[1]

    # direction vectors to enable exploration of adjacent cells
    drow = [-1, 0, 1, 0]
    dcol = [0, 1, 0, -1]

    # cycle through adjacent cells, checking for open cells
    for i in range(4):
        adj_r = r + drow[i]
        adj_c = c + dcol[i]

        adj_cell = [adj_r, adj_c]

        if is_valid_cell(adj_cell) == False:
            continue

        if gmap[adj_r][adj_c] == 0:
            has_open_neighbour = True

    return has_open_neighbour

def find_robot_cell():
    global Position, map_origin, map_res

    #finds index of gmap cell in centre of robot

    start_r = int(round((Position.y - map_origin.y) / map_res - 1))
    start_c = int(round((Position.x - map_origin.x)/map_res - 1))

    return [start_r, start_c]

def indices_to_coordinates(cell_list):
    global map_res, map_origin
    #converts cell indices to x,y coordinates for a list of cells
    #returns list of coordinates

    coordinate_list = [] #initialise coordinate list

    for cell in cell_list:
        r = cell[0]
        c = cell[1]
        y = (r + 1)*map_res + map_origin.y
        x = (c + 1)*map_res + map_origin.x
        coordinate_list.append([x,y])

    return coordinate_list

def frontier_midpoint(coordinate_list):
    #returns midpoint from list of coordinates

    #get list of x and y coordinates respectively
    x_list = [0]*len(coordinate_list)
    y_list = [0]*len(coordinate_list)
    for i in range(len(coordinate_list)):
        point = coordinate_list[i]
        x_list[i] = point[0]
        y_list[i] = point[1]

    #find mid x coordinate and mid y coordinate
    mid_x = (max(x_list) + min(x_list)) / 2
    mid_y = (max(y_list) + min(y_list)) / 2

    return [mid_x, mid_y]

def order_by_closest(coordinate_list):
    global Position
    #order list of coordinates by ascending euclidean distance from robot position

    #find euclidean distance from each point to robot
    dists = [0]*len(coordinate_list)
    for i in range(len(coordinate_list)):
        point = coordinate_list[i]
        x = point[0]
        y = point[1]
        dists[i] = ((x - Position.x)**2 + (y - Position.y)**2)**0.5

    #sort coordinate list in order corresponding to ascending order of dists
    index_order = np.argsort(dists) #gets list of indices in sorted (ascending) order (from dists)
    #print(index_order)

    for i in range(len(coordinate_list)):
        coordinate_list[i] = coordinate_list[index_order[i]] #reorders coordinate list accordingly

    return coordinate_list

def find_frontiers():
    global map_rows, map_columns, map_open, map_closed, frontier_open, frontier_closed
    #returns list containing each frontier (by cell indices)

    # initialise lists to track occupancy grid cell states
    map_open = [[False for i in range(map_columns)] for i in range(map_rows)]
    map_closed = [[False for i in range(map_columns)] for i in range(map_rows)]
    frontier_open = [[False for i in range(map_columns)] for i in range(map_rows)]
    frontier_closed = [[False for i in range(map_columns)] for i in range(map_rows)]

    # initialise list to store found frontiers in
    frontiers = []

    # define markers
    mo = "map_open"
    mc = "map_closed"
    fo = "frontier_open"
    fc = "frontier_closed"

    # initiate WFD process
    # find index of cell at centre of robot to begin search
    start_p = find_robot_cell()
    queue_m = Queue()  # create explore queue
    queue_m.put(start_p)  # enqueue start point to explore queue
    mark(start_p, mo)  # mark start point as map-open

    # deal with each point in explore queue
    while queue_m.empty() == False:
        p = queue_m.get()  # get next point in explore queue

        # skip to next point in explore queue if this point has already been explored
        if is_map_closed(p) == True:
            continue

        # otherwise
        # see if point is a frontier point
        if is_frontier_point(p) == True:
            # if point is a frontier point then:
            queue_f = Queue()  # create a frontier point queue
            new_frontier = []  # create new frontier point list
            queue_f.put(p)  # add point to frontier point list
            mark(p, fo)  # mark point as frontier-open

            # frontier extraction now begins

            # deal with each point in frontier queue
            while queue_f.empty() == False:
                q = queue_f.get()  # get next point in frontier queue

                # skip to next point in frontier queue if point cannot be added to new frontier list because it's already explored
                if is_map_closed(q) == True or is_frontier_closed(q) == True:
                    continue

                # otherwise
                # see if point is a frontier point
                if is_frontier_point(q) == True:
                    # if point is a frontier point then:
                    new_frontier.append(q)  # add point to frontier list

                    queue_f_adjacent(q,
                                     queue_f)  # explore points adjacent to q and add unexplored ones to frontier queue

                mark(q, fc)  # mark point as frontier-closed

            frontiers.append(new_frontier)  # save frontier point list to list of all frontiers

            # frontier extraction ends

        # exploration continues by adding unexplored cells to explore queue
        queue_m_adjacent(p, queue_m)  # explore points adjacent to p and add valid unexplored ones to explore queue
        mark(p, mc)  # mark p as map-closed

    # remove empty elements from list of frontiers
    frontiers = [ele for ele in frontiers if ele != []]

    return frontiers

def find_closest_frontier_midpoints(frontiers):
    #returns list containing frontier midpoints in x,y coordinates ordered by euclidean distance to robot position

    frontiers_midpoints = []  # initialise list to store frontier midpoints
    frontier_coordinates = []  # initialise list to store coordinates of all frontier points
    for frontier in frontiers:
        # for each frontier in frontiers:

        # turn list of cell indices into list of x,y coordinate pairs
        coordinates = indices_to_coordinates(frontier)

        # add list of x,y coordinates to list containing x,y coordinates of all points in each frontier (rows = frontiers, columns = x,y coordinates)
        frontier_coordinates.append(coordinates)

        # turn list of x,y coordinate pairs into midpoint of frontier
        midpoint = frontier_midpoint(coordinates)

        # add frontier midpoint to list of frontier midpoints
        frontiers_midpoints.append(midpoint)

    # order list of frontier midpoints by euclidean distance to robot position
    closest_frontier_midpoints = order_by_closest(frontiers_midpoints)

    return closest_frontier_midpoints

def main():
    global map_columns, map_rows

    rospy.init_node('exploration')

    #subscribe to receive position of robot, and occupancy grid map data
    sub_odom = rospy.Subscriber('/odom', Odometry, clbk_odom)
    sub_map = rospy.Subscriber('/map', OccupancyGrid, clbk_map)

    rate = rospy.Rate(0.4)
    while not rospy.is_shutdown():
        rate.sleep()

################################################### FIND FRONTIERS #####################################################

        frontiers = find_frontiers()

########################################################################################################################

########################################### ORDER FRONTIERS BY CLOSEST #################################################

        closest_frontier_midpoints = find_closest_frontier_midpoints(frontiers)

########################################################################################################################

######################################## PUBLISH POINTS TO PATH PLANNER ################################################

########################################################################################################################

################################################ PLOTTING TO TEST ######################################################

        ##plot received gmap as a colour map
        #plt.figure(1)
        ##make colour map of fixed colours: dark grey, light grey, black
        #colour_map = mpl.colors.ListedColormap(['#808080','#C5C9C7','black'])
        ##specify boundary values for each colour
        #bounds = [-1.5,-0.5,0.5,101]
        ##set how to scale values to be coloured
        #norm = mpl.colors.BoundaryNorm(bounds, colour_map.N)
        #
        ##create plot
        #plt.imshow(gmap, interpolation='nearest', cmap=colour_map, norm=norm)
        #plt.gca().invert_yaxis()
        #plt.suptitle("Gmap (as a colour map)")
        #
        #
        #
        ##create merged list of all frontier points for plotting (can no longer tell where each frontier starts/ends
        ##list created is: rows = 1, columns = number of frontier points/x,y coordinate pairs
        #all_frontier_points = []
        #for frontier in frontiers:
        #    all_frontier_points = all_frontier_points + frontier
        #
        ##convert list to numpy array for reordering
        #all_frontier_points = np.array(all_frontier_points)
        ##get total number of frontier points/x,y coordinate pairs
        #total_frontier_points = int(sum(len(row) for row in frontiers))
        ##reorder array/list so its: rows = number of frontier points/x,y coordinate pairs, columns = 2 (one for x, other for y)
        #all_frontier_points = all_frontier_points.reshape(total_frontier_points, 2)
        #
        ##plot all frontier points on blank graph
        #r_points = [0]*total_frontier_points
        #c_points = [0]*total_frontier_points
        #for i in range(total_frontier_points):
        #    r_points[i] = all_frontier_points[i][0]
        #    c_points[i] = all_frontier_points[i][1]
        #
        #plt.figure(2)
        #plt.plot(c_points, r_points, 'o', color='r')
        #plt.suptitle("All frontier points in Gmap")
        #plt.xlim([0, map_columns])
        #plt.ylim([0, map_rows])
        #plt.show()

        # plot received gmap as a colour map
        plt.figure(1)

        # create merged list of all frontier points for plotting (can no longer tell where each frontier starts/ends
        # list created is: rows = 1, columns = number of frontier points/x,y coordinate pairs
        all_frontier_points = []
        for frontier in frontiers:
            all_frontier_points = all_frontier_points + frontier

        # convert list to numpy array for reordering
        all_frontier_points = np.array(all_frontier_points)
        # get total number of frontier points/x,y coordinate pairs
        total_frontier_points = int(sum(len(row) for row in frontiers))
        # reorder array/list so its: rows = number of frontier points/x,y coordinate pairs, columns = 2 (one for x, other for y)
        all_frontier_points = all_frontier_points.reshape(total_frontier_points, 2)

        # create gmap with frontier points highlighted
        gmap_highlighted = gmap
        for point in all_frontier_points:
            r = point[0]
            c = point[1]
            gmap_highlighted[r][c] = 150

        # make colour map of fixed colours: dark grey, light grey, black, red
        colour_map = mpl.colors.ListedColormap(['#808080', '#C5C9C7', 'black', 'red'])
        # specify boundary values for each colour
        bounds = [-1.5, -0.5, 0.5, 101, 200]
        # set how to scale values to be coloured
        norm = mpl.colors.BoundaryNorm(bounds, colour_map.N)

        # create plot
        plt.imshow(gmap_highlighted, interpolation='nearest', cmap=colour_map, norm=norm)
        plt.gca().invert_yaxis()
        plt.suptitle("Gmap (frontier points highlighted)")

        plt.show()
########################################################################################################################

if __name__ == '__main__':
    main()