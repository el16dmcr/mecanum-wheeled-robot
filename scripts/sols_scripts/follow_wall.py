#!/usr/bin/env python

import rospy #rospy library
from create_obstacle_map_from_sensors import flattenLidar2DMap #class to process lidar point clouds
from sensor_msgs.msg import PointCloud2 #message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2 #class to read data from /UGV/laser/scan topic messages
from geometry_msgs.msg import Point, Twist
#from nav_msgs.msg import Odometry
#from tf import transformations
#import math

#initialise publisher
pub = None

#initialise state stuff
State = 0
State_dict ={
    0:'Find wall',
    1:'Turn left',
    2:'Follow wall'
}

def clbk_laser(msg):
    #initiaise class
    numberAngleSegments = 360
    lidarOffset = [0, 0.136]
    flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

    #read in mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc) #so-called 2D map

    # 360/8 = 45
    #define regions of robot using 2D map via a dictionary
    regions = {
        'front': min(dists[158:202]),
        'fright': min(dists[203:247]),
        'right': min(dists[248:292]),
        'rright': min(dists[293:337]),
        'rear': min(min(dists[338:360]), min(dists[0:22])),
        'rleft': min(dists[23:67]),
        'left': min(dists[68:112]),
        'fleft': min(dists[113:157])
    }

    #use state of regions to move the robot accordingly (state machine)
    take_action(regions)

def take_action(regions):
    d = 1 # safe distance

    #decide how robot should act depending on state of environment
    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        change_state(0)
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        change_state(2)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        change_state(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        change_state(1)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        change_state(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        change_state(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        change_state(0)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        change_state(1)
    else:
        state_description = 'Unknown Case'
        rospy.loginfo(regions)

def change_state(state):
    global State, State_dict
    State = state
    print('State [%s] - %s') % (state, State_dict[state])



def find_wall():
    msg = Twist()
    msg.linear.x = 0.4
    msg.angular.z = -0.3
    return msg

def turn_left():
    msg = Twist()
    msg.angular.z = 0.6
    return msg

def follow_wall():
    msg = Twist()
    msg.linear.x = 0.8
    return msg



def stopRobot():
        msg = Twist()
        msg.angular.x = 0
        msg.angular.y = 0
        msg.angular.z = 0
        msg.linear.x = 0
        msg.linear.y = 0
        msg.linear.z = 0
        pub.publish(msg)
        print('Robot Stopped')



def main():
    global pub

    rospy.init_node('follow_wall')

    #define subscriber to receive point cloud data from lidar
    #when new messages are received, clbk_laser is invoked with the message as the first argument
    sub = rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk_laser)

    #define publisher to move robot
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    rospy.on_shutdown(stopRobot)

    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        msg = Twist()
        if State == 0:
            msg = find_wall()
        elif State == 1:
            msg = turn_left()
        elif State == 2:
            msg = follow_wall()
        else:
            rospy.logerr('Unknown State!')

        pub.publish(msg)


        rate.sleep()

if __name__ == '__main__':
    main()
