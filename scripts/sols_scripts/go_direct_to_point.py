#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from tf import transformations
import math

#robot state variables
Position = Point()
Yaw = 0

#machine state
State = 0

#goal point
desired_position = Point()
desired_position.x = 2
desired_position.y = 2
desired_position.z = 0

#precisions
yaw_precision = 2*math.pi/180
dist_precision = 0.3

#publishers
pub = None

#callback to determine position and yaw from odometry messages
def clbk_odom(msg):
    global Position, Yaw

    #get position from odometry message
    Position = msg.pose.pose.position

    #determine yaw from odometry message
    quaternion = (msg.pose.pose.orientation.x,
                  msg.pose.pose.orientation.y,
                  msg.pose.pose.orientation.z,
                  msg.pose.pose.orientation.w)
    euler = transformations.euler_from_quaternion(quaternion)
    Yaw = euler[2]

def fix_yaw(des_pos):
    global Yaw, Position, pub, State, yaw_precision

    print('Fixing heading')

    #find desired heading toward goal
    desired_yaw = math.atan2(des_pos.y - Position.y, des_pos.x - Position.x)
    #find error in heading
    err_yaw = desired_yaw - Yaw

    #publish angular velocity to correct heading error
    twist_msg = Twist()
    if math.fabs(err_yaw) > yaw_precision:
       twist_msg.angular.z = 0.7 if err_yaw > 0 else -0.7

    pub.publish(twist_msg)

    #chage state if yaw is okay
    if math.fabs(err_yaw) <= yaw_precision:
        change_state(1)

    print('Yaw: ' + str(Yaw))

def change_state(state):
    global State
    State = state

def go_straight(des_pos):
    global Yaw, Position, pub, State, dist_precision, yaw_precision

    print('Proceeding to goal')

    #find position error (distance to goal)
    pos_error = ((des_pos.y - Position.y)**2 + (des_pos.x - Position.x)**2)**0.5

    twist_msg = Twist()
    if pos_error > dist_precision:
        twist_msg.linear.x = 0.6

    pub.publish(twist_msg)

    #change state to stop if needed
    if pos_error <= dist_precision:
        change_state(2)

    # find desired heading toward goal
    desired_yaw = math.atan2(des_pos.y - Position.y, des_pos.x - Position.x)
    # find error in heading
    err_yaw = desired_yaw - Yaw

    #change state to fix heading if needed
    if math.fabs(err_yaw) > yaw_precision:
        change_state(0)

def stop():
    global pub
    twist_msg = Twist()
    twist_msg.linear.x = 0
    twist_msg.linear.y = 0
    twist_msg.linear.z = 0
    twist_msg.angular.x = 0
    twist_msg.angular.y = 0
    twist_msg.angular.z = 0
    pub.publish(twist_msg)

    print('Stopping')

def main():
    global pub, desired_position
    rospy.init_node('go_direct_to_point')
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    sub_odom = rospy.Subscriber('/odom', Odometry, clbk_odom)

    rospy.on_shutdown(stop)

    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        if State == 0:
            fix_yaw(desired_position)
        elif State == 1:
            go_straight(desired_position)
        elif State == 2:
            print('Goal reached!')
            stop()
        else:
            print('Unknown State!')
        rate.sleep()

if __name__ == '__main__':
    main()
