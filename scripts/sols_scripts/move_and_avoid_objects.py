#!/usr/bin/env python

import rospy #rospy library
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist #message type to send to cmd_vel topic
from create_obstacle_map_from_sensors import flattenLidar2DMap #class to process lidar point clouds
from sensor_msgs.msg import PointCloud2 #message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2 #class to read data from /UGV/laser/scan topic messages
import matplotlib.pyplot as plt

pub = None

# initiaise class
numberAngleSegments = 360
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

def clbk_laser(msg):


    #read in mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc) #so-called 2D map

    # 360/8 = 45
    #define regions of robot using 2D map via a dictionary
    regions = {
        'front': min(dists[158:202]),
        'fright': min(dists[203:247]),
        'right': min(dists[248:292]),
        'rright': min(dists[293:337]),
        'rear': min(min(dists[338:360]), min(dists[0:22])),
        'rleft': min(dists[23:67]),
        'left': min(dists[68:112]),
        'fleft': min(dists[113:157])
    }

    #use state of regions to move the robot accordingly (state machine)
    take_action(regions)


def take_action(regions):
    #initialise message to send
    msg = Twist()
    linear_x = 0
    angular_z = 0
    d = 0.5  # safe distance

    #decide how robot should act depending on state of environment
    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        linear_x = 0.8
        angular_z = 0
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        linear_x = 0
        angular_z = 0.5
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        linear_x = 0
        angular_z = -0.5
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        linear_x = 0
        angular_z = -0.5
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        linear_x = 0
        angular_z = 0.5
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        linear_x = 0
        angular_z = -0.5
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        linear_x = 0.2
        angular_z = 0
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        linear_x = 0
        angular_z = -0.5
    else:
        state_description = 'Unknown Case'
        rospy.loginfo(regions)

    #show state of environment and publish appropriate message to cmd_vel topic to which robot is subscribed
    rospy.loginfo(state_description)
    msg.linear.x = linear_x
    msg.angular.z = angular_z
    pub.publish(msg)

def stopRobot():
        msg = Twist()
        msg.angular.x = 0
        msg.angular.y = 0
        msg.angular.z = 0
        msg.linear.x = 0
        msg.linear.y = 0
        msg.linear.z = 0
        pub.publish(msg)
        print('Robot Stopped')

def plotData():
    global flattenNearestObstacles
    print("plot function called")
    plt.figure(1)
    plt.cla()
    plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5',
                    linewidth=0.1)
    plt.xlabel('Angle around robot / degrees', fontsize=14)
    plt.ylabel('Distance to nearest object / m', fontsize=14)
    plt.title('Flattened graph of nearest obstacles', fontsize=18)
    fig = plt.gcf()
    fig.canvas.draw()


def main():
    global pub
    global flattenNearestObstacles

    rospy.init_node('move_forward_and_avoid')

    #define publisher to move robot
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    rospy.on_shutdown(stopRobot)

    #rate = rospy.Rate(20)
    #while not rospy.is_shutdown():

    #plotData()
    #define subscriber to receive point cloud data from lidar
    #when new messages are received, clbk_laser is invoked with the message as the first argument
    sub = rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk_laser)


    #rate.sleep()

    rospy.spin()


if __name__ == '__main__':
    main()
