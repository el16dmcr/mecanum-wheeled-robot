#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
import time

class MyRobot():

    def __init__(self, name, speed):
        self.robot_name = name
        self.max_speed = speed
        self.current_speed = 0.0
        self.wheels = rospy.Publisher("/cmd_vel", Twist, queue_size=0)
        time.sleep(1)
        print("Robot initialised")

    def set_speed(self, speed):
        print("Sending speed: "+ str(speed))
        #self.current_speed = speed
        speed_cmd = Twist()
        speed_cmd.linear.x = speed
        self.wheels.publish(speed_cmd)


if __name__ == "__main__":

    rospy.init_node("my_node")

    robot = MyRobot("Geezer", 0.5)
    robot.set_speed(0.15)
    time.sleep(10)
    robot.set_speed(0)

    rospy.spin()
