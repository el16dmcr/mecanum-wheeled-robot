#!/usr/bin/env python

import rospy #rospy library
from local_path_planning.create_obstacle_map_from_sensors import flattenLidar2DMap #class to process lidar point clouds
from sensor_msgs.msg import PointCloud2 #message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2 #class to read data from /UGV/laser/scan topic messages
import matplotlib.pyplot as plt

pub = None

# initialise class
numberAngleSegments = 360
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

def clbk(msg):
    global flattenNearestObstacles

    #read in mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc) #so-called 2D map

    # 360/8 = 45
    #define regions of robot using 2D map via a dictionary
    #regions1 = [
    #    min(dists[68:112]),          #left
    #    min(dists[113:157]),
    #    min(dists[158:202]),         #front
    #    min(dists[203:247]),
    #    min(dists[248:292]),         #right
    #    min(dists[293:337]),
    #    min(min(dists[338:360]), min(dists[0:22])),  #rear
    #    min(dists[23:67]),           #rear left
    #    ]

    regions = {
        'left': min(dists[68:112]),  # left
        'fleft': min(dists[113:157]),
        'front': min(dists[158:202]),  # front
        'fright': min(dists[203:247]),
        'right': min(dists[248:292]),  # right
        'rright': min(dists[293:337]),
        'rear': min(min(dists[338:360]), min(dists[0:22])),  # rear
        'rleft': min(dists[23:67]),  # rear left
        }
         #min(dists[68:112]),  # left
         #min(dists[113:157]),  # front left
         #min(dists[158:202]), #front
         #min(dists[203:247]), #front right
         #min(dists[248:292]), #right
         #min(dists[293:337]), #rear right
         #min(min(dists[338:360]), min(dists[0:22])), #rear
         #min(dists[23:67]), #rear left

    #rospy.loginfo(regions1[1:4])
    #print(dists)

    #use state of regions to move the robot accordingly (state machine)
    take_action(regions)
    #print(dists)

def take_action(regions):
    d = 1.5  # safe distance

    #decide how robot should act depending on state of environment
    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        #change_state_wf(0)
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        #change_state_wf(2)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        #change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        #change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        #change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        #change_state_wf(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        #change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        #change_state_wf(1)
    else:
        state_description = 'Unknown Case'
        rospy.loginfo(regions)

    print(state_description)
    print(regions['fleft'], regions['front'], regions['fright'])
    
def plotData():
    global flattenNearestObstacles
    print("plot function called")
    plt.figure(1)
    plt.cla()
    plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5',
                    linewidth=0.1)
    plt.xlabel('Angle around robot / degrees', fontsize=14)
    plt.ylabel('Distance to nearest object / m', fontsize=14)
    plt.title('Flattened graph of nearest obstacles', fontsize=18)
    fig = plt.gcf()
    fig.canvas.draw()


def main():
    global pub
    #global flattenNearestObstacles

    rospy.init_node('read_lidar_flat_distances')

    # define subscriber to receive point cloud data from lidar
    # when new messages are received, clbk is invoked with the message as the first argument
    sub = rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk)

    rate = rospy.Rate(5)
    while not rospy.is_shutdown():
        rate.sleep()






if __name__ == '__main__':
    main()
