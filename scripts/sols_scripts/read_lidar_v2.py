#!/usr/bin/env python

import rospy  # rospy library
from local_path_planning.create_obstacle_map_from_sensors import \
    flattenLidar2DMap  # class to process lidar point clouds
from sensor_msgs.msg import PointCloud2  # message type to receive from /UGV/laser/scan topic
from sensor_msgs import point_cloud2  # class to read data from /UGV/laser/scan topic messages
import matplotlib.pyplot as plt

pub = None

# initialise class
numberAngleSegments = 100
lidarOffset = [0, 0.136]
flattenNearestObstacles = flattenLidar2DMap(numberAngleSegments, 0.246, 0.36185, lidarOffset)

# initialise safe distance
d = 1

# initialise dictionary to contain min distances to objects around robot
regions = {
        'front': d,
        'fright': d,
        'right': d,
        'left': d,
        'fleft': d,
    }



def clbk(msg):
    global flattenNearestObstacles, regions

    # read in mesaage to create lidar point cloud
    lidar_pc = point_cloud2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)

    # flatten point cloud to create 2D obstacle map (distance to closest obstacle at each angle segment 0-360 degrees according to defined coordinate system)
    dists = flattenNearestObstacles.return2DObstacleMap(lidar_pc)  # so-called 2D map

    # 360/8 = 45
    # define regions of robot using 2D map via a dictionary
    regions = {
        'front': min(dists[45:54]),
        'fright': min(dists[55:64]),
        'right': min(dists[65:74]),
        'left': min(dists[25:34]),
        'fleft': min(dists[35:44]),
    }


def take_action():
    global regions, d

    # decide how robot should act depending on state of environment
    state_description = ''
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 1 - No Obstacle Ahead'
        # change_state_wf(0)
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 2 - Obstacle Front Right'
        # change_state_wf(2)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 3 - Obstacle Front Left'
        # change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'Case 4 - Obstacle Front'
        # change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'Case 5 - Obstacle Front and Front Right'
        # change_state_wf(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'Case 6 - Obstacle Front and Front Left'
        # change_state_wf(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 7 - Obstacle Front Right and Front Left'
        # change_state_wf(0)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'Case 8 - Obstacle Front, Front Right and Front Left'
        # change_state_wf(1)
    else:
        state_description = 'Unknown Case'
        rospy.loginfo(regions)

    print(state_description)
    print(regions['fleft'], regions['front'], regions['fright'])


def plotData():
    global flattenNearestObstacles
    print("plot function called")
    plt.figure(1)
    plt.cla()
    plt.bar(flattenNearestObstacles.xAxis, flattenNearestObstacles.nearestObstaclesFlattened, color='#89c3e5',
            linewidth=0.1)
    plt.xlabel('Angle around robot / degrees', fontsize=14)
    plt.ylabel('Distance to nearest object / m', fontsize=14)
    plt.title('Flattened graph of nearest obstacles', fontsize=18)
    fig = plt.gcf()
    fig.canvas.draw()


def main():

    # global flattenNearestObstacles

    rospy.init_node('read_lidar_flat_distances')

    # define subscriber to receive point cloud data from lidar
    # when new messages are received, clbk is invoked with the message as the first argument
    sub = rospy.Subscriber('/UGV/laser/scan', PointCloud2, clbk)

    rate = rospy.Rate(5)
    while not rospy.is_shutdown():

        take_action()

        rate.sleep()



if __name__ == '__main__':
    main()
