from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup
d = generate_distutils_setup(
    packages=['local_path_planning','SLAM','genetic_training_2D_Sim','nodes'],
    package_dir={'': 'scripts'}
)
setup(**d)
